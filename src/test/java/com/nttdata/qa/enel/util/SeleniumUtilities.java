package com.nttdata.qa.enel.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
//package utilities;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumUtilities {

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String NUMERIC_STRING = "0123456789";

    public Wait<WebDriver> wait;
    public Wait<WebElement> waitElement;
    public WebDriver driver;
    JavascriptExecutor jse;
    //LOGGER //LOGGER;

    public Consumer<WebElement> click = e -> {
        e.click();
    };

    public Consumer<WebElement> sendEnter = e -> {
        e.sendKeys(Keys.ENTER);
    };

    /**
     * Scrolla l'elemento in visibilità
     *
     * @param e --> elemento da portarei n visibilità
     * @param f --> flag, se false l'elemento viene protato in visibilità quanto più in basso possibile nella pagina,
     * se true quanto più in alto possibile
     */
    public BiConsumer<WebElement, Boolean> scrollToVisibility = (e, f) -> {
        jse.executeScript("arguments[0].scrollIntoView(" + f + ");", e);
    };

    public Consumer<WebElement> clickwithjse = (e) -> {
        jse.executeScript("arguments[0].click();", e);
    };

    public Consumer<WebElement> setBorder = (e) -> {
        jse.executeScript("arguments[0].style.border = \"thick solid #0000FF\";", e);
    };

    public Consumer<WebElement> removeBorder = (e) -> {
        jse.executeScript("arguments[0].style.border = \"\";", e);
    };

//	public BiConsumer<WebElement,String> evaluate = (e,f) -> {
//		jse.executeScript("document.evaluate( " + f + " ,document, null, XPathResult.STRING_TYPE, null ).stringValue;", e) ;
//	};

    public Consumer<WebElement> scrollAndClick = e -> {

//		scrollToElement(e);

        try {
            Thread.currentThread().sleep(1050);
            scrollToElement(e);
            Thread.currentThread().sleep(460);
            e.click();
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
//		e.click();
    };

    public Consumer<WebElement> scrollElement = e -> {

//		scrollToElement(e);

        try {
            Thread.currentThread().sleep(1050);
            scrollToElement(e);
            Thread.currentThread().sleep(460);

        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    };

    public Consumer<WebElement> clear = (WebElement e) -> {
        e.clear();
    };

    public Function<WebElement, String> getText = e -> {
        return e.getText();
    };


    public BiConsumer<WebElement, String> sendKeys = (e, s) -> {
        e.clear();
        e.sendKeys(s);
    };

    public BiConsumer<WebElement, String> sendKeysCharByChar = (e, s) -> {
        scrollToElement(e);
        e.clear();

        for (int idx = 0; idx < s.length(); idx++) {
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e1) {
                // TODO Auto-generated catch block
                //e1.printStackTrace();
            }
            e.sendKeys(s.substring(idx, idx + 1));
        }
    };

    public void pressJavascript(By oggetto) throws Exception {
        WebElement element = waitAndGetElement(oggetto);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public BiConsumer<WebElement, String> sendKeysAndEnter = (e, s) -> {
        e.clear();
        e.sendKeys(s);
        e.sendKeys(Keys.ENTER);
    };

    public BiConsumer<WebElement, String> scrollAndSendKeys = (e, s) -> {
        scrollToElement(e);
        e.clear();
        e.sendKeys(s);
    };

    public BiConsumer<WebElement, String> scrollAndSendKeysAndTab = (e, s) -> {
        scrollToElement(e);
        e.clear();
        e.sendKeys(s);
        e.sendKeys(Keys.TAB);
    };


    public BiConsumer<WebElement, String> select = (e, s) -> {
        Select select = new Select(e);
        select.selectByVisibleText(s);
    };

    public BiConsumer<WebElement, String> selectFirstElemet = (e, s) -> {
        Select select = new Select(e);
        select.selectByIndex(1);
    };

    public BiConsumer<WebElement, String> selectADifferentOption = (e, v) -> {
        Select select = new Select(e);
        List<WebElement> options = select.getOptions();
        for (WebElement s : options) {
            if (!s.getText().trim().equalsIgnoreCase(v)) {
                select.selectByVisibleText(s.getText());
                break;
            }
        }
    };

    public BiConsumer<WebElement, String> scrollAndSelect = (e, s) -> {
        Select select = new Select(e);
        scrollToElement(e);
        select.selectByVisibleText(s);
    };

    public BiConsumer<WebElement, String> scrollAndSelectByValue = (e, s) -> {
        Select select = new Select(e);
        scrollToElement(e);
        select.selectByValue(s);
        ;
    };

    public Function<WebElement, List<WebElement>> selectOptions = e -> {
        Select select = new Select(e);
        return select.getOptions();
    };

    // Costruisco la classe passando in input il Webdriver
    public SeleniumUtilities(WebDriver driver) {
        this.driver = driver;
        this.jse = (JavascriptExecutor) driver;
        //LOGGER = //LOGGER.getLogger("");
    }


    /**
     * Metodo che verifica l'esistenza di un oggetto in pagina, restituisce un booleano
     *
     * @param el
     * @param sec
     * @return true --> oggetto presente in pagina
     * @throws Exception
     */
    public boolean exists(String frame, By el, int sec) throws Exception {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement frameToSw = this.driver.findElement(
                By.xpath("//iframe[@name='" + frame + "'] | //frame[@name='" + frame + "']")
        );
        this.driver.switchTo().frame(frameToSw);
        boolean flag = exists(el, sec);
        driver.switchTo().defaultContent();
        return flag;
    }

    /**
     * Metodo che verifica l'esistenza di un oggetto in pagina, restituisce un booleano
     *
     * @param el
     * @param sec
     * @return true --> oggetto presente in pagina
     * @throws Exception
     */
    public boolean exists(By el, int sec) throws Exception{
        driver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
        FluentWait<WebDriver> wait = new WebDriverWait(this.driver, sec);
//      if(wait.until(ExpectedConditions.presenceOfElementLocated(el)) == null){throw new Exception("Elemento non trovato with the xpath :" + xpathToString(el));};
        try {
			wait.until(ExpectedConditions.presenceOfElementLocated(el));
		} catch (Exception e) {
			return false;
		}
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        return true;
    }

    public WebElement waitAndGetElement(By by, boolean skipClickable) throws Exception {

        return waitAndGetElement(by, Costanti.TIMEOUT, Costanti.POLLING, skipClickable);
    }

    public WebElement waitAndGetElement(String frame, By by, boolean skipClickable) throws Exception {
        //		driver.switchTo().frame(frame);
        WebElement frameToSw = this.driver.findElement(
                By.xpath("//iframe[@name='" + frame + "'] | //frame[@name='" + frame + "']")
        );
        this.driver.switchTo().frame(frameToSw);
        WebElement el = waitAndGetElement(by, Costanti.TIMEOUT, Costanti.POLLING, skipClickable);
        driver.switchTo().defaultContent();
        return el;
    }


    public WebElement waitAndGetElement(By by, int withTimeout, int pollingEvery) throws Exception {

        return waitAndGetElement(by, withTimeout, pollingEvery, false);
    }

    // Metodo che restituisce un oggetto tramite il BY, dopo aver aspettato la sua
    // esistenza in base a Timeout e Polling di default.
    public WebElement waitAndGetElement(By by) throws Exception {

        return waitAndGetElement(by, Costanti.TIMEOUT, Costanti.POLLING, false);

    }

    // Metodo che restituisce oggetti tramite il BY, dopo aver aspettato la loro
    // esistenza in base a Timeout e Polling di default.
    public List<WebElement> waitAndGetElements(By by) throws Exception {

        return waitAndGetElements(by, Costanti.TIMEOUT, Costanti.POLLING);
    }

    // Metodo che restituisce un elemento tramite il BY dopo aver aspettato la sua
    // esistenza in base ad un Timeout e Polling specifici.

    @SuppressWarnings("deprecation")
    public WebElement waitAndGetElement(By by, int withTimeout, int pollingEvery, boolean skipClickable) throws Exception {
        //System.out.println("start waiting for " + by);

        this.wait = new FluentWait<WebDriver>(this.driver)
                .withTimeout(withTimeout, TimeUnit.SECONDS)
                .pollingEvery(pollingEvery, TimeUnit.SECONDS)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class);

        WebElement x = this.wait.until((driver) -> {
            return driver.findElement(by);
        });
        if (!skipClickable)
            this.wait.until(ExpectedConditions.elementToBeClickable(x));

        //System.out.println("stop waiting for " + by);
        return x;
    }

    // Metodo che restituisce una lista di elementi tramite il BY dopo aver
    // aspettato la loro
    // esistenza in base ad un Timeout e Polling specifici.

    @SuppressWarnings("deprecation")
    public List<WebElement> waitAndGetElements(By by, int withTimeout, int pollingEvery) throws Exception {
        this.wait = new FluentWait<WebDriver>(this.driver).withTimeout(withTimeout, TimeUnit.SECONDS)
                .pollingEvery(pollingEvery, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
                .ignoring(ElementNotVisibleException.class).ignoring(NoSuchElementException.class);

        List<WebElement> x = this.wait.until((driver) -> {
            return driver.findElements(by);
        });

        return x;
    }

    // Metodo che restituisce un elemento a partire da un altro WebElement tramite
    // il BY dopo aver aspettato la sua esistenza in base ad un Timeout e Polling di
    // default.
    public WebElement waitAndGetElement(WebElement element, By by) throws Exception {

        return waitAndGetElement(element, by, Costanti.TIMEOUT, Costanti.POLLING);

    }

    public WebElement waitAndGetElement(WebElement element, By by, boolean skip) throws Exception {

        return waitAndGetElement(element, by, Costanti.TIMEOUT, Costanti.POLLING, skip);

    }

    // Metodo che restituisce lista di elementi a partire da un altro WebElement
    // tramite
    // il BY dopo aver aspettato l'esistenza ad un Timeout e Polling di
    // default.
    public List<WebElement> waitAndGetElements(WebElement element, By by) throws Exception {
        return waitAndGetElements(element, by, Costanti.TIMEOUT, Costanti.POLLING);
    }

    /*
     * Metodo che restituisce un elemento a partire da un altro WebElement tramite
     * il BY dopo aver aspettato la sua esistenza in base ad un Timeout e Polling
     * specifici.
     */
    @SuppressWarnings("deprecation")
    public WebElement waitAndGetElement(WebElement element, By by, int withTimeout, int pollingEvery, boolean skipCheckClickable) throws Exception {
        this.waitElement = new FluentWait<WebElement>(element).withTimeout(withTimeout, TimeUnit.SECONDS)
                .pollingEvery(pollingEvery, TimeUnit.SECONDS)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class);

        this.wait = new FluentWait<WebDriver>(this.driver)
                .withTimeout(withTimeout, TimeUnit.SECONDS)
                .pollingEvery(pollingEvery, TimeUnit.SECONDS)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class);

        //System.out.println("start waiting for " + by);
        WebElement x = this.waitElement.until((ele) -> {
            return ele.findElement(by);
        });

        if (!skipCheckClickable)
            this.wait.until(ExpectedConditions.elementToBeClickable(x));

        //System.out.println("stop waiting for " + by);
        return x;
    }

    public WebElement waitAndGetElement(WebElement element, By by, int withTimeout, int pollingEvery) throws Exception {
        return waitAndGetElement(element, by, withTimeout, pollingEvery, false);
    }

    /*
     * Metodo che restituisce elementi partire da un altro WebElement tramite il BY
     * dopo aver aspettato l'esistenza in base ad un Timeout e Polling specifici.
     */
    @SuppressWarnings("deprecation")
    public List<WebElement> waitAndGetElements(WebElement element, By by, int withTimeout, int pollingEvery)
            throws Exception {
        //System.out.println("start waiting for " + by);
        this.waitElement = new FluentWait<WebElement>(element).withTimeout(withTimeout, TimeUnit.SECONDS)
                .pollingEvery(pollingEvery, TimeUnit.SECONDS).ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class);

        //System.out.println("start waiting for " + by);
        List<WebElement> x = this.waitElement.until((ele) -> {
                    return ele.findElements(by);

                }
        );
        //System.out.println("stop waiting for " + by);
        return x;
    }

    public boolean FrameSwitcher(By by) throws Exception {
        return FrameSwitcher(by, Costanti.TIMEOUT, Costanti.POLLING);
    }

    /*
     * Metodo che restituisce elementi partire da un altro WebElement tramite il BY
     * dopo aver aspettato l'esistenza in base ad un Timeout e Polling specifici.
     */
    @SuppressWarnings("deprecation")
    public List<WebElement> waitAndGetElements(WebElement element, By by, int withTimeout, int pollingEvery, boolean skipClickable)
            throws Exception {
        //System.out.println("start waiting for " + by);
        this.waitElement = new FluentWait<WebElement>(element).withTimeout(withTimeout, TimeUnit.SECONDS)
                .pollingEvery(pollingEvery, TimeUnit.SECONDS).ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class);

        //System.out.println("start waiting for " + by);
        List<WebElement> x = this.waitElement.until((ele) -> {
                    return ele.findElements(by);

                }
        );
        //System.out.println("stop waiting for " + by);
        return x;
    }

    public boolean FrameSwitcher(By by, int withTimeout, int pollingEvery) throws Exception {
        try {
            WebElement x = waitAndGetElement(by, withTimeout, pollingEvery);
            this.driver.switchTo().frame(x);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String frameSearcher(By frameBy, By subElement, Consumer<WebElement> fun) throws Exception {
        // boolean guard = false;
        String frameName = "";
        // WebElement clientFrame;
        List<String> possibleFrames;
        boolean guard = false;
        int i = 0;
        possibleFrames = refreshFrame2(frameBy);

        // for (int i = 0; i < possibleFrames.size() && !guard; i++)
        for (String clientFrame : possibleFrames) {
            i++;
            // clientFrame = possibleFrames.get(i);

            try {
                //System.out.println("index = " + i + " of " + possibleFrames.size() + " frames, id :" + clientFrame);
                frameName = clientFrame;
                /**
                 * if (!frameName.contains("standby")) {
                 */
                WebElement frameToSw = this.driver.findElement(
                        By.xpath("//iframe[@name='" + clientFrame + "'] | //frame[@name='" + clientFrame + "']")
                );
                this.driver.switchTo().frame(frameToSw);
                //				this.driver.switchTo().frame(clientFrame);

                WebElement confirm = this.driver.findElement(subElement);
                //System.out.println("Provo ad interagire con l'elemento");
                //System.out.println(confirm.isDisplayed());
                if (confirm.isDisplayed()) {
                    fun.accept(confirm);
                    // confirm.click();
                    guard = true;
                    break;
                }

            } catch (Exception e) {
                // se l'elemento non � stato trovato posso proseguire la ricerca
                //System.out.println(e.getClass());
                //System.out.println("Element not found in this frame, checking the next");
            } finally {
                this.driver.switchTo().defaultContent();
            }
        }

        if (!guard)
            throw new NoSuchElementException("Impossibile identificare l'elemento con selettore : "
                    + subElement
                    + "nei frame identificati da" + frameBy);
        return frameName;
    }

    private List<WebElement> refreshFrame(By frameBy) throws Exception {
        // this.driver.switchTo().defaultContent();
        WebDriverWait wait = new WebDriverWait(this.driver, 50);
        return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(frameBy))
                .stream().filter(f -> {
                    return !(f.getAttribute("name").contains("standby"));
                }).collect(Collectors.toList());
    }

    private List<String> refreshFrame2(By frameBy) throws Exception {
        // this.driver.switchTo().defaultContent();
        WebDriverWait wait = new WebDriverWait(this.driver, 50);
        return wait.until(
                ExpectedConditions.presenceOfAllElementsLocatedBy(frameBy))
                .stream().map(f -> {
                    return f.getAttribute("name");
                }).filter(f -> {
                    return !(f.contains("standby"));
                }).collect(Collectors.toList());
    }

    public WebElement commonFrameManagement(String frameName, By by) throws Exception {
        WebDriverWait wait = new WebDriverWait(this.driver, 120);
        //LOGGER.log(Level.INFO ,"try to switch to " + frameName);//("try to switch to " + frameName);
        WebElement frameToSw = this.driver.findElement(
                By.xpath("//iframe[@name='" + frameName + "'] | //frame[@name='" + frameName + "']")
        );
        this.driver.switchTo().frame(frameToSw);
        return wait.until(ExpectedConditions.elementToBeClickable(by));
    }
    public WebElement commonFrameManagement( By by) throws Exception {
        WebDriverWait wait = new WebDriverWait(this.driver, 120);
//        //LOGGER.log(Level.INFO ,"try to switch to " + frameName);//("try to switch to " + frameName);
//        WebElement frameToSw = this.driver.findElement(
//                By.xpath("//iframe[@name='" + frameName + "'] | //frame[@name='" + frameName + "']")
//        );
//        this.driver.switchTo().frame(frameToSw);
        //RS 
        SeleniumUtilities util = new SeleniumUtilities(driver);
        
        util.getFrameActive();
        return wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public void switchToFrame(String frameName) throws Exception {
        WebDriverWait wait = new WebDriverWait(this.driver, 120);
        //LOGGER.log(Level.INFO ,"try to switch to " + frameName);//("try to switch to " + frameName);
        WebElement frameToSw = this.driver.findElement(
                By.xpath("//iframe[@name='" + frameName + "'] | //frame[@name='" + frameName + "']")
        );
        this.driver.switchTo().frame(frameToSw);
    }


    public String frameManager(String frameName, By by, Function<WebElement, String> fun) throws Exception {
        try {
            WebElement x = commonFrameManagement(frameName, by);
            return fun.apply(x);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Impossibile interagire con l'oggetto identificato dal seguente by :" + by.toString() + ". Exception class: " + e.getClass());

        } finally {
            this.driver.switchTo().defaultContent();
        }

    }


    public void frameManager(String frameName, By by, Consumer<WebElement> fun) throws Exception {
        try {
            WebElement x = commonFrameManagement(frameName, by);
            fun.accept(x);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Impossibile interagire con l'oggetto identificato dal seguente by :"
                    + by.toString() +
                    ". Exception class: "
                    + e.getClass());
        } finally {
            this.driver.switchTo().defaultContent();
        }

    }


    public void frameManager(String frameName, By by, BiConsumer<WebElement, Boolean> fun, Boolean param)
            throws Exception {
        try {
            WebElement x = commonFrameManagement(frameName, by);
            fun.accept(x, param);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            this.driver.switchTo().defaultContent();
        }
    }


    public void frameManager(String frameName, By by, BiConsumer<WebElement, String> fun, String param)
            throws Exception {
        try {
            WebElement x = commonFrameManagement(frameName, by);
            fun.accept(x, param);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            this.driver.switchTo().defaultContent();
        }
    }


    // Ritorna il numero di righe della tabella
    public int getTableRowCount(WebElement table) throws Exception {
        return waitAndGetElements(table, By.xpath("./tbody/tr")).size();
    }

    // Ritorna il numero di colonne della tabella. N.B: ritorna solo il numero di
    // righe significative,
    // non quelle con presenza checkbox
    public int getTableColumnCount(WebElement table) throws Exception {
        return waitAndGetElements(table, By.xpath("./thead/tr/th/div/span")).size();

    }

    // Ritorna l'indice della colonna della tabella a partire dal suo nome
    public int getTableColumnIndexByName(WebElement table, String colName) throws Exception {
        int colIndex = 0;
        boolean guard = false;
        List<WebElement> colNamesSpan = waitAndGetElements(table, By.xpath("./thead/tr/th/div/span"));
        for (WebElement el : colNamesSpan) {
            colIndex = colIndex + 1;

            if (el.getText().equals(colName)) {
                guard = true;
                break;
            }
        }

        if (!guard)
            throw new Exception("Impossibile trovare la colonna " + colName + " nella tabella");

        return colIndex;
    }

    //la tabella non contiene span
    public int getTableColumnIndexByNameWithOutSpan(WebElement table, String colName) throws Exception {
        int colIndex = 0;
        boolean guard = false;
        List<WebElement> colNamesSpan = waitAndGetElements(table, By.xpath("./thead/tr/th/div"));
        for (WebElement el : colNamesSpan) {
            colIndex = colIndex + 1;

            if (el.getText().equals(colName)) {
                guard = true;
                break;
            }
        }

        if (!guard)
            throw new Exception("Impossibile trovare la colonna " + colName + " nella tabella");

        return colIndex;
    }

    //

    /**
     * Ritorna l'indice della colonna della tabella a partire dal suo nome
     *
     * @param table
     * @param colName
     * @param xpathHeader - Xpath per riconoscimento oggetti header
     * @return colIndex
     * @throws Exception
     */
    public int getTableColumnIndexByName(WebElement table, String colName, String xpathHeader) throws Exception {
        int colIndex = 0;
        boolean guard = false;
        List<WebElement> colNamesSpan = waitAndGetElements(table, By.xpath(xpathHeader));
        for (WebElement el : colNamesSpan) {

            colIndex = colIndex + 1;
            if (el.getText().equals(colName)) {
                guard = true;
                break;
            }
        }

        if (!guard)
            throw new Exception("Impossibile trovare la colonna " + colName + " nella tabella");

        return colIndex;
    }

    // Ritorna una mappa contenente le dimensioni della tabella
    public Map<String, Integer> getTableSize(WebElement table) throws Exception {
        Map<String, Integer> tableSize = new HashMap<>();
        tableSize.put("rows", getTableRowCount(table));
        tableSize.put("columns", getTableColumnCount(table));
        return tableSize;
    }


    // Ritorna tutta la lista di valori di una specifica riga della tabella
    public List<String> getTableRowData(WebElement table, int rowNumber) throws Exception {
        if (rowNumber == 0) {
            throw new Exception("Il numero di righe deve partire da 1");
        }
        int rowCount = getTableRowCount(table);
        if (rowCount < rowNumber)
            throw new Exception("Impossibile trovare la riga numero " + rowNumber + " in tabella");
        WebElement row = waitAndGetElement(table, By.xpath("./tbody//tr[" + rowNumber + "]"));
        List<WebElement> rowsContent = waitAndGetElements(row, By.xpath("./span[@class='uiOutputText']"));
        List<String> rData = new ArrayList<>();
        for (WebElement webElement : rowsContent) {
            rData.add(webElement.getText());
        }
        return rData;
    }

    // table[@id='x_Table']/tbody/tr[1]/td[not(self::td[@class='slds-cell-shrink'])][1]/span/div//span[@class='uiOutputText']
    public String getTableCellData(WebElement table, int rowNumber, String columnName) throws Exception {
        if (rowNumber == 0) {
            throw new Exception("Il numero di righe deve partire da 1");
        }
        int rowCount = getTableRowCount(table);
        if (rowCount < rowNumber)
            throw new Exception("Impossibile trovare la riga numero " + rowNumber + " in tabella");

        int colNumber = getTableColumnIndexByName(table, columnName);
        WebElement colContent = waitAndGetElement(table,
                By.xpath("./tbody/tr[" + rowNumber + "]/td[not(self::td[@class='slds-cell-shrink'])][" + colNumber
                        + "]/span/div/div/span[@data-aura-class='uiOutputText']"));
        return colContent.getText();
        // table[@id='x_Table']/tbody/tr[1]/td[not(self::td[@class='slds-cell-shrink'])][7]/span/div/div[@class='slds-truncate']/span
    }

    //la struttura della tabella non contiene i div
    public String getTableCellDataWithoutDiv(WebElement table, int rowNumber, String columnName, String tag) throws Exception {
        if (rowNumber == 0) {
            throw new Exception("Il numero di righe deve partire da 1");
        }
        int rowCount = getTableRowCount(table);
        if (rowCount < rowNumber)
            throw new Exception("Impossibile trovare la riga numero " + rowNumber + " in tabella");

        int colNumber = getTableColumnIndexByNameWithOutSpan(table, columnName);
        WebElement colContent = waitAndGetElement(table,
                By.xpath("./tbody/tr[" + rowNumber + "]/td[" + colNumber
                        + "]/" + tag));
        return colContent.getText();
        // table[@id='x_Table']/tbody/tr[1]/td[not(self::td[@class='slds-cell-shrink'])][7]/span/div/div[@class='slds-truncate']/span
    }

    public String getTableCellData2(WebElement table, int rowNumber, String columnName) throws Exception {
        if (rowNumber == 0) {
            throw new Exception("Il numero di righe deve partire da 1");
        }
        int rowCount = getTableRowCount(table);
        if (rowCount < rowNumber)
            throw new Exception("Impossibile trovare la riga numero " + rowNumber + " in tabella");

        int colNumber = getTableColumnIndexByName(table, columnName);
        WebElement colContent = waitAndGetElement(table,
                By.xpath("./tbody/tr[" + rowNumber + "]/td[not(self::td[@class='slds-cell-shrink'])][" + colNumber
                        + "]/span/div/div/span/span[@data-aura-class='uiOutputText']"));
        return colContent.getText();
        // table[@id='x_Table']/tbody/tr[1]/td[not(self::td[@class='slds-cell-shrink'])][7]/span/div/div[@class='slds-truncate']/span
    }

    public int getTableRowFromColumnData(WebElement table, String columnName, String columnValue) throws Exception {

        int rowCount = getTableRowCount(table);
        boolean guard = false;
        if (rowCount < 1)
            throw new Exception("Non risultano righe in tabella. Impossibile cercare un elemento");

        int colNumber = getTableColumnIndexByName(table, columnName);
        int rowNum = 1;
        while (rowNum <= rowCount) {
            WebElement colContent = waitAndGetElement(table,
                    By.xpath("./tbody/tr[" + rowNum + "]/td[not(self::td[@class='slds-cell-shrink'])][" + colNumber
                            + "]/span/div//span[@class='uiOutputText']"));

            if (colContent.getText().equals(columnValue)) {
                guard = true;
                break;
            }
            rowNum++;
        }
        if (!guard)
            throw new Exception(
                    "Impossibile trovare in tabella il valore " + columnValue + " nella colonna " + columnName);
        return rowNum;

    }

    /**
     * @param table
     * @param columnName
     * @param columnValue
     * @param xpathHeader  - xPath riconoscimento Elementi Header Es. //table[@id ='clientsTable2']
     * @param xpathRow     - xPath riconoscimento della Row Es. /tbody/tr[%d]
     *                     Il %d è un placeholder per il numero di riga
     * @param xpathElement - xPath elemento nella Row Es./td[%d]
     *                     %d è il numero di colonna da sostituire
     * @return
     * @throws Exception ritorna il numero di riga che contiene il valore (columnValue) della colonna (columnValue) passato
     */
    public int getTableRowFromColumnData(WebElement table, String columnName, String columnValue,
                                         String xpathHeader, String xpathRow, String xpathElement) throws Exception {
        return getTableRowFromColumnData(table, columnName, columnValue,
                xpathHeader, xpathRow, xpathElement, 0);
    }

    /**
     * @param table
     * @param columnName
     * @param columnValue
     * @param xpathHeader      - xPath riconoscimento Elementi Header Es. //table[@id ='clientsTable2']
     * @param xpathRow         - xPath riconoscimento della Row Es. /tbody/tr[%d]
     *                         Il %d è un placeholder per il numero di riga
     * @param xpathElement     - xPath elemento nella Row Es./td[%d]
     *                         %d è il numero di colonna da sostituire
     * @param columnStartIndex - numero di colonne da ignorare da sinistra ( ex checkbox)
     * @return
     * @throws Exception ritorna il numero di riga che contiene il valore (columnValue) della colonna (columnValue) passato
     */
    public int getTableRowFromColumnData(WebElement table, String columnName, String columnValue,
                                         String xpathHeader, String xpathRow, String xpathElement, int columnStartIndex) throws Exception {

        return getTableRowFromColumnData(table, columnName, columnValue,
                xpathHeader, xpathRow, xpathElement, columnStartIndex, true);
    }


    /**
     * @param table
     * @param columnName
     * @param columnValue
     * @param xpathHeader      - xPath riconoscimento Elementi Header Es. //table[@id ='clientsTable2']
     * @param xpathRow         - xPath riconoscimento della Row Es. /tbody/tr[%d]
     *                         Il %d è un placeholder per il numero di riga
     * @param xpathElement     - xPath elemento nella Row Es./td[%d]
     *                         %d è il numero di colonna da sostituire
     * @param columnStartIndex - numero di colonne da ignorare da sinistra ( ex checkbox)
     * @param exceptionReturn  - true -> lancia Exception in caso dato non trovato - False -> Non lancia Exception
     * @return
     * @throws Exception ritorna il numero di riga che contiene il valore (columnValue) della colonna (columnValue) passato
     */
    public int clickTableElementFromColumnData(WebElement table, String columnName, String columnValue,
                                               String xpathHeader, String xpathRow, String xpathElement, int columnStartIndex, boolean exceptionReturn) throws Exception {

        int rowCount = getTableRowCount(table);
        boolean guard = false;

        if (rowCount < 1)
            throw new Exception("Non risultano righe in tabella. Impossibile cercare un elemento");

        int colNumber = getTableColumnIndexByName(table, columnName, xpathHeader);
        int rowNum = 1;
        //		List<WebElement> rowElement = waitAndGetElements(table, By.xpath(xpathRow));
        while (rowNum <= rowCount) {
            //System.out.println(" serching row " + rowNum);
            WebElement colContent = waitAndGetElement(table,
                    By.xpath(String.format(xpathRow, rowNum)
                            + String.format(xpathElement, colNumber + columnStartIndex)
                    ), true
            );
            //System.out.println("Confronting " + colContent.getText() +" with "+ columnValue);

            if (colContent.getText().equalsIgnoreCase(columnValue)) {
                guard = true;
                colContent.click();
                break;
            }
            rowNum++;
        }
        if (!guard && exceptionReturn)
            throw new Exception(
                    "Impossibile trovare in tabella il valore " + columnValue + " nella colonna " + columnName);
        else if (!guard && !exceptionReturn)
            rowNum = 0;

        return rowNum;

    }


    /**
     * @param table
     * @param columnName
     * @param columnValue
     * @param xpathHeader      - xPath riconoscimento Elementi Header Es. //table[@id ='clientsTable2']
     * @param xpathRow         - xPath riconoscimento della Row Es. /tbody/tr[%d]
     *                         Il %d è un placeholder per il numero di riga
     * @param xpathElement     - xPath elemento nella Row Es./td[%d]
     *                         %d è il numero di colonna da sostituire
     * @param columnStartIndex - numero di colonne da ignorare da sinistra ( ex checkbox)
     * @param exceptionReturn  - true -> lancia Exception in caso dato non trovato - False -> Non lancia Exception
     * @return
     * @throws Exception ritorna il numero di riga che contiene il valore (columnValue) della colonna (columnValue) passato
     */
    public int getTableRowFromColumnData(WebElement table, String columnName, String columnValue,
                                         String xpathHeader, String xpathRow, String xpathElement, int columnStartIndex, boolean exceptionReturn) throws Exception {

        int rowCount = getTableRowCount(table);
        boolean guard = false;

        if (rowCount < 1)
            throw new Exception("Non risultano righe in tabella. Impossibile cercare un elemento");

        int colNumber = getTableColumnIndexByName(table, columnName, xpathHeader);
        //		System.out.println(colNumber);
        int rowNum = 1;
        //		List<WebElement> rowElement = waitAndGetElements(table, By.xpath(xpathRow));
        while (rowNum <= rowCount) {

            //			System.out.println(String.format(xpathRow,rowNum)
            //					+ String.format(xpathElement,colNumber + columnStartIndex));

            //System.out.println(" serching row " + rowNum);
            WebElement colContent = waitAndGetElement(table,
                    By.xpath(String.format(xpathRow, rowNum)
                            + String.format(xpathElement, colNumber + columnStartIndex)
                    ), true
            );
            //System.out.println("Confronting " + colContent.getText() +" with "+ columnValue);

            if (colContent.getText().equalsIgnoreCase(columnValue)) {
                guard = true;
                break;
            }
            rowNum++;
        }
        if (!guard && exceptionReturn)
            throw new Exception(
                    "Impossibile trovare in tabella il valore " + columnValue + " nella colonna " + columnName);
        else if (!guard && !exceptionReturn)
            rowNum = 0;

        return rowNum;

    }

    public String getTableCellText(WebElement table, int rowNum, String columnName,
                                   String xpathHeader, String xpathRow, String xpathElement, int columnStartIndex) throws Exception {

        int colNumber = getTableColumnIndexByName(table, columnName, xpathHeader);
        WebElement colContent = waitAndGetElement(table,
                By.xpath(String.format(xpathRow, rowNum)
                        + String.format(xpathElement, colNumber + columnStartIndex)
                ), true
        );
        return colContent.getText();

    }

    /**
     * Metodo da utilizzare per Tabelle dati il cui Header e Righe dati sono formate da 2 distinti oggetti table html
     * ritorna il numero di riga che contiene il valore (columnValue) della colonna (columnValue) passato
     *
     * @param tableHeader  - Oggetto table html Header
     * @param tableRow     - Oggetto table html che identifica le Rows
     * @param columnName
     * @param columnValue
     * @param xpathHeader  - xPath riconoscimento Elementi Header Es. //table[@id ='clientsTable2']
     * @param xpathRow     - xPath riconoscimento della Row Es. /tbody/tr
     * @param xpathElement - xPath elemento nella Row Es./td
     * @return numero di riga
     * @throws Exception
     */
    public int getTableRowFromColumnData(WebElement tableHeader, WebElement tableRow,
                                         String columnName, String columnValue, String xpathHeader, String xpathRow, String xpathElement) throws Exception {

        int rowCount = getTableRowCount(tableRow);
        boolean guard = false;
        if (rowCount < 1)
            throw new Exception("Non risultano righe in tabella. Impossibile cercare un elemento");

        int colNumber = getTableColumnIndexByName(tableHeader, columnName, xpathHeader);
        int rowNum = 1;
        //		List<WebElement> rowElement = waitAndGetElements(table, By.xpath(xpathRow));
        while (rowNum <= rowCount) {
            WebElement colContent = waitAndGetElement(tableRow, By.xpath(xpathRow + "[" + rowNum + "]" + xpathElement + "[" + colNumber + "]"));

            if (colContent.getText().equals(columnValue)) {
                guard = true;
                break;
            }
            rowNum++;
        }
        if (!guard)
            throw new Exception(
                    "Impossibile trovare in tabella il valore " + columnValue + " nella colonna " + columnName);
        return rowNum;

    }


    public void checkElementDisabled(By elemento) throws Exception {
        WebElement el = waitAndGetElement(elemento, 20, 1, true);
        String a = el.getAttribute("disabled");
        if (a != null && a.length() > 1) {

            if (!a.contentEquals("true"))
                throw new Exception("L'elemento identificato dal seguente xpath " + elemento.toString() + " risulta abilitato contrariamente a quanto atteso.");

        } else
            throw new Exception("L'elemento identificato dal seguente xpath " + elemento.toString() + " risulta abilitato contrariamente a quanto atteso.");


    }

    public void checkElementValue(By elemento, String value) throws Exception {
        WebElement el = waitAndGetElement(elemento, 20, 1, true);
        String a = el.getAttribute("value");
        if (!a.contentEquals(value))
            throw new Exception("L'elemento identificato dal seguente xpath " + elemento.toString() + " non contiene il valore " + value + " come atteso");

    }

    public boolean checkElementText(By elemento, String text) throws Exception {
        WebElement el = waitAndGetElement(elemento, 20, 1, true);
        String a = el.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
        if (a.length() < 1) a = el.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");

        return a.contains(text);

    }

    public String getElementText(By elemento) throws Exception {
        WebElement el = waitAndGetElement(elemento, 20, 1, true);
        String a = el.getText().replace("\r", "").replace("\n", "").replace("\r\n", "");
        if (a.length() < 1) a = el.getAttribute("innerText").replace("\r", "").replace("\n", "").replace("\r\n", "");

        return a;

    }


    //24.09 nuovo xpath della check box
    public WebElement getTableRowCheckbox2(WebElement table, int row) throws Exception {
//		String xpath =  "./tbody/tr[" + row
//				+ "]/td[@class='slds-cell-shrink']//lightning-input[contains(@id,'theCheckbox')]/div/span/label[@class='slds-checkbox__label']";
        //System.out.println(xpath);

        String xpath = "//lightning-input[@id='theCheckbox_02i1n00000Py0F8AAJ']/div/span/label/span";
        return waitAndGetElement(table, By.xpath(xpath));

    }

    public WebElement getTableRowCheckbox3(WebElement table, int row) throws Exception {
        String xpath = "./tbody/tr[" + row
                //		+ "]/td[(self::td[@class='slds-cell-shrink'])]/span/div[@class='slds-checkbox']/label";
                + "]/td[(self::td[@class='slds-cell-shrink'])]//lightning-input[contains(@id,'theCheckbox')]/div/span/label/span";
        //lightning-input[@id='theCheckbox_02i1n00000Py0F8AAJ']/div/span/label/span
        //System.out.println(xpath);
        return waitAndGetElement(table, By.xpath(xpath));

    }

    public WebElement getTableRowCheckbox(WebElement table, int row) throws Exception {
        String xpath = "./tbody/tr[" + row
                + "]/td[(self::td[@class='slds-cell-shrink'])]/span/div[@class='slds-checkbox']/label";
        //lightning-input[@id='theCheckbox_02i1n00000Py0F8AAJ']/div/span/label/span
        //System.out.println(xpath);
        return waitAndGetElement(table, By.xpath(xpath));

    }

    public WebElement getTableRowRadioButton(WebElement table, int row) throws Exception {
        String xpath = "./tbody/tr[" + row
                + "]/td[(self::td[@class='slds-cell-shrink'])]/span//label[@class='slds-radio']";
        //System.out.println(xpath);
        return waitAndGetElement(table, By.xpath(xpath));

    }


    public void scrollDown() {
        jse.executeScript("scroll(0, 1000)");
    }

    public void scrollDownTouch() {
        jse.executeScript("scroll(0, 1500)");
    }
    
    public void scrollNaNdecchia() {
        jse.executeScript("scroll(0, 500)");
    }

    public void scrollNaNdecchiaNdecchia() {
        jse.executeScript("scroll(0, 600)");
    }

    public void scrollDownDocument() {
        jse.executeScript("window.scrollTo(0, 1000)");
    }

    public void scrollUpDocument() {
        jse.executeScript("window.scrollTo(0, 1000)");
    }

    public void scrollUp() {
        jse.executeScript("scroll(1000, 0)");
    }

    public void setStyle(WebElement element, String style) {
        String script = "arguments[0].setAttribute('style',"
                + "'display: none');";
        ((JavascriptExecutor) driver).executeScript(script, element);
    }

    //	public void scrollToElement(WebElement el) {
    //		jse.executeScript("arguments[0].scrollIntoView();", el);
    //	}
    public void scrollToElement(WebElement element) {

        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

        ((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);


    }

    /**
     * This method sets an attribute of the element identified by an xpath.
     *
     * @param xpath: the xpath of the element
     *               attributeName: 	the name of the attribute to be set
     *               attrivuteValie:	the new attribute value
     */
    public void setElementAttribute(String xpath, String attributeName, String attributeValue) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("(document.evaluate(\"" + xpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).setAttribute('" + attributeName + "','" + attributeValue + "')");
    }

    /**
     * This method sets the element (identified by an xpath) value.
     *
     * @param xpath: the xpath of the element
     *               attrivuteValie:	the new attribute value
     * @throws Exception
     */
    public void setElementValue(String xpath, String attributeValue) throws Exception {
        waitAndGetElement(By.xpath(xpath));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("(document.evaluate(\"" + xpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value = '" + attributeValue + "'");
    }
    
    public void jsScroll() throws Exception {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, 200);");
    }
    
    public void jsScrollToElement(WebElement we) throws Exception {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", we);
    }

    public void jsClickElement(By xpath) throws Exception {
        String itemXpath = xpath.toString().replace("By.xpath: ", "");
        //waitAndGetElement(xpath);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("(document.evaluate(\"" + itemXpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
    }

    public String jsGetInnerText(By xpath) throws Exception {
        String itemXpath = xpath.toString().replace("By.xpath: ", "");
        waitAndGetElement(xpath);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return (String) js.executeScript("var elm = (document.evaluate(\"" + itemXpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).innerHTML; return elm;");
    }

    public String jsGetInputValue(By xpath) throws Exception {
        String itemXpath = xpath.toString().replace("By.xpath: ", "");
        waitAndGetElement(xpath);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return (String) js.executeScript("var elm = (document.evaluate(\"" + itemXpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).value; return elm;");
    }

    public void scrollToTop() {

        jse.executeScript("window.scrollTo(0, 0)");
    }

    public void scrollToBottom() {

        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public boolean checkModalExist() throws Exception {
        boolean exist = false;
        int tentativi = 10;
        int seconds = 10;
        while (tentativi-- > 0) {
            try {
                WebElement modal = this.driver.findElement(By.xpath("//div[@id='theModalCmp_B']"));
                //System.out.println(modal.getTagName());
                String classAttribute = modal.getAttribute("class");
                if (classAttribute.equals("slds-modal slds-fade-in-open")) {
                    exist = true;
                    tentativi = 0;
                }
            } catch (Exception e) {
            } finally {
                TimeUnit.SECONDS.sleep(seconds);
            }


        }


        return exist;
    }

    public String getModalDescription() throws Exception {
        String content = null;
        try {
            WebElement modalContent = driver.findElement(By.xpath("//div[@id='theModalCmpDescr_A']/p"));
            content = modalContent.getText();

        } catch (Exception e) {
            throw new Exception("Impossibile recuperare il contenuto del modal a video.");
        }
        return content;
    }


    public String generateRandomPodNumberGAS() throws Exception {
        int n = (int) (1000000 + (new Random().nextFloat() * 9000000));
        return "052699" + Integer.toString(n) + "9";
    }

    public String generateRandomPodNumberGAS4() throws Exception {
        int n = (int) (1000000 + (new Random().nextFloat() * 9000000));
        return "008845" + Integer.toString(n) + "9";
    }

    public String generateRandomPodNumberGASPrimaAttivazioneEvo() throws Exception {
        int n = (int) (1000000 + (new Random().nextFloat() * 9000000));
        return "010799" + Integer.toString(n) + "9";
    }

    public String generateRandomPodNumberGAS2() throws Exception {
        int n = (int) (1000000 + (new Random().nextFloat() * 9000000));
        return "030899" + Integer.toString(n) + "9";
    }

    public String generateRandomPodNumberGAS3() throws Exception {
        int n1 = (int) (1000000000 + (new Random().nextFloat() * 900000000));
        return "5453" + Integer.toString(n1);
    }

    public String generateRandomPodNumberEnergia() throws Exception {
        int n = (int) (1000000 + (new Random().nextFloat() * 9000000));
        return "IT002E" + Integer.toString(n) + "A";
    }

    public String generateRandomPodNumberEnergia2() throws Exception {
        int n = (int) (10000000 + (new Random().nextFloat() * 90000000));
        return "IT001E" + Integer.toString(n);
    }

    public void acceptModalDialog() throws Exception {
        try {
            driver.findElement(By.xpath("//button[@id='theModalCmpBtn_A' and text()='OK']")).click();
            TimeUnit.SECONDS.sleep(5);
        } catch (Exception e) {
            throw new Exception("Impossibile cliccare sul pulsante OK della modal dialog a video");
        }
    }

    public void waitSpinnerSupplyDataGrid() throws Exception {
        boolean xa = true;
        int tentativi = 10;
        int seconds = 5;
        TimeUnit.SECONDS.sleep(seconds);
        while (xa && --tentativi > 0) {
            try {
//				WebElement x = driver.findElement(By.xpath(
//						//"//lightning-spinner[@id='spinner' and  contains(@data-aura-class,'SupplyDataGrid') and contains(@class,'slds-hide')]"));
//						"//lightning-spinner[@id='spinner']"));
                if (!driver.findElement(By.xpath("//lightning-spinner[@id='spinner']")).isDisplayed()) {
                    xa = false;
                }

            } catch (Exception e) {

            }
            TimeUnit.SECONDS.sleep(seconds);
        }
    }

    public boolean verifyExistenceInFrame(String frame, By element, int timeout) throws Exception {
        try {
            //			driver.switchTo().frame(frame);
            WebElement frameToSw = this.driver.findElement(
                    By.xpath("//iframe[@name='" + frame + "'] | //frame[@name='" + frame + "']")
            );
            this.driver.switchTo().frame(frameToSw);
            new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(element));

        } catch (Exception e) {
            return false;
        } finally {
            driver.switchTo().defaultContent();
        }
        return true;
    }

    public boolean verifyExistence(By element, int timeout) throws Exception {
        try {
            new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(element));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    public boolean verifyExistenceNew(By element, int timeout) throws Exception {     
            return new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(element)) != null ? true : false;
    }
    
    public WebElement verifyExistenceWithElementReturn(By element, int timeout) throws Exception {
        try {
            return new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(element));
        } catch (Exception e) {
            return null;
        }
    }

    public boolean verifyAttribute(By element, int timeout, String attribute, String value) throws Exception {
        try {
            new WebDriverWait(driver, timeout).until(ExpectedConditions.attributeContains(element, attribute, value));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean verifyFrameAvailableAndSwitchToIt(By element, int timeout) throws Exception {
        try {
            new WebDriverWait(driver, timeout).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
        return true;
    }

    public boolean verifyVisibility(By element, int timeout) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            WebElement theElement = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
            scrollToElement(theElement);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyClickability(By element, int timeout) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            WebElement theElement = wait.until(ExpectedConditions.elementToBeClickable(element));
            scrollToElement(theElement);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
     * It checks the visibility of an element for certain number of attempts.
     *
     * Parameters:
     *		element: 	The element under check
     *		timeout: 	Timeout expressed in seconds
     *		attempt: 	If set to true, it will check until the element is not visible anymore; else, it will check the visibility of the element just once.
     *
     *Notes:
     *		It's very useful to wait until the loader is not visible anymore on Enel Web Portal www-colla.enel.it
     */
    public boolean verifyVisibility(By element, int timeout, boolean attempt) throws Exception {
        try {
            boolean cycle = true;
            while (cycle) {
                WebDriverWait wait = new WebDriverWait(driver, timeout);
                WebElement theElement = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
                scrollToElement(theElement);
                timeout--;
                if (!attempt)
                    cycle = false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void verifyEelementAttrInFrame(String frame, By element, String attr, String value, int timeout) throws Exception {
        //		driver.switchTo().frame(frame);
        WebElement frameToSw = this.driver.findElement(
                By.xpath("//iframe[@name='" + frame + "'] | //frame[@name='" + frame + "']")
        );
        this.driver.switchTo().frame(frameToSw);
        new WebDriverWait(driver, timeout).until(
                ExpectedConditions.attributeToBe(element, attr, value));
        driver.switchTo().defaultContent();
    }


    public void waitFrameNotExistAnymore(String frame) throws Exception {
        WebDriverWait wait = new WebDriverWait(this.driver, 100);
        String framePath = "//iframe[@name='" + frame + "']";
        //LOGGER.getLogger("").log(Level.INFO, "Attesa scomparsa del frame "+frame);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(framePath)));
        //LOGGER.getLogger("").log(Level.INFO, "Frame "+frame+"non piu presente. Switch altro frame");
    }


    public String getFrameByIndex(By frame, int idx) throws Exception {
        WebDriverWait wait = new WebDriverWait(this.driver, 100);
        By frameBy = frame;
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(
                frameBy,
                idx));

        List<String> l = driver.findElements(frameBy)
                .stream()
                .map(x -> {
                    return x.getAttribute("name");
                })
                .collect(Collectors.toList());

        //System.out.println("frame collection returned : " + l.toString());
        //System.out.println("frame returned : " + l.get(idx));
        //LOGGER.getLogger("").log(Level.INFO, l.get(idx));
        return l.get(idx);
    }


    public String getFrameByIndex(int idx) throws Exception {
    	//La modifica di classi e metodi così impattanti deve essere SEMPRE condivisa. Grazie 
        return getFrameByIndex(By.xpath("//iframe[@title ='accessibility title']"),
                idx);

    }

    public void waitUntilIsNotDisplayed(By by) throws Exception {
        int secondi = 5;
        int tentativi = 30;
        boolean isDisplayed = driver.findElement(by).isDisplayed();
        //System.out.println("Elemento ancora presente a video?:"+isDisplayed+ "Iniziano Tentativi");
        while (driver.findElement(by).isDisplayed() && tentativi >= 0) {
            //System.out.println("Elemento ancora presente a video?:"+isDisplayed+".Tentativo rimententi:"+tentativi);
            //LOGGER.log(Level.INFO ,"Elemento ancora presente a video?:"+isDisplayed+".Tentativo rimententi:"+tentativi);
            tentativi--;
            TimeUnit.SECONDS.sleep(secondi);
            isDisplayed = driver.findElement(by).isDisplayed();
        }
        if (tentativi == 0) {
            throw new Exception("Numero di tentativi esautiti,l'elemento è presente ancora a video");
        }
    }

    public void waitUntilIsDisplayed(By by) throws Exception {
        int secondi = 5;
        int tentativi = 30;
        boolean isDisplayed = false;

        while (!isDisplayed && tentativi >= 0) {
            //System.out.println("Visibilità elemento:"+isDisplayed+".Tentativo rimententi:"+tentativi);
            tentativi--;
            TimeUnit.SECONDS.sleep(secondi);
            try {
                isDisplayed = driver.findElement(by).isDisplayed();
                //se trova l'elemento deve uscire!!!!
                break;
                //LOGGER.log(Level.INFO ,"Visibilità elemento:"+isDisplayed+".Tentativo rimententi:"+tentativi);
            } catch (Exception e) {
                isDisplayed = false;
                //System.out.println("Catch, visibilità"+isDisplayed);
            }
        }
        if (!isDisplayed) {
            //System.out.println("Elemento not found by :" + by);
            throw new Exception("Numero di tentativi esautiti,l'elemento non è presente ancora a video");
        }
    }

    public void keepClickingElementUntilSuccess(WebElement element, int attemptsCounter) throws Exception {
        if (attemptsCounter > 0) {
            try {
                element.click();
            } catch (Exception e) {
                attemptsCounter--;
                if (attemptsCounter > 0) {
                    keepClickingElementUntilSuccess(element, attemptsCounter);
                } else {
                    throw new Exception("Could not click element after max number of attempts");
                }
            }
        } else {
            throw new Exception("The number of clicking attempts should be greater than zero");
        }
    }

    public static String randomAlphaNumeric(int count) throws Exception {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static String randomNumeric(int count) throws Exception {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * NUMERIC_STRING.length());
            builder.append(NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    /**
     * Il metodo cerca un elemento, identificato dal secondo parametro, in tutti i frame identificati dal primo parametro
     *
     * @param frameBy  espressione by che identificai frame da provare
     * @param elemento da cercare nei frames
     * @return nome del frame in cui è stato identificato il primo sub element interagibile
     * @throws NoSuchElementException se non viene trovato alcun elemento
     */
    public String sfaccimmator(By frameBy, By subElement) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, 10, 0);
        List<WebElement> frames = driver.findElements(frameBy);

        for (WebElement frame : frames) {
            try {
                //				driver.switchTo().frame(frame);
                WebElement frameToSw = this.driver.findElement(
                        By.xpath("//iframe[@name='" + frame + "'] | //frame[@name='" + frame + "']")
                );
                this.driver.switchTo().frame(frameToSw);
                wait.until(ExpectedConditions.elementToBeClickable(subElement));
                return frame.getAttribute("name");
            } catch (Exception e) {
                //System.out.println("sfaccimator exception : " + e.getClass());
                e.printStackTrace();
                continue;
            } finally {
                driver.switchTo().defaultContent();
            }
        }

        throw new NoSuchElementException("Impossibile identificare l'elemento con selettore : "
                + subElement
                + "nei frame identificati da" + frameBy);

    }

    /**
     * Gestione degli oggetti senza Frame
     *
     * @param by    - riconoscimento dell'oggetto
     * @param fun   - funtion da richiamare (Es. util.select)
     * @param param
     */
    public void objectManager(By by, BiConsumer<WebElement, String> fun, String param) {
        try {
            WebDriverWait wait = new WebDriverWait(this.driver, 120);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(by));
            fun.accept(x, param);
        } catch (Exception e) {
            //System.out.println("Unexpected exception!!! " + e.getClass());
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Gestione degli oggetti Select non standard su salesforce
     *
     * @param label - Label che identifica la select
     * @param value - Valore della lista da selezionare
     */

    public void selectLighningValue(String label, String value) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='" + label + "']/..//input")));
            Consumer<WebElement> fun = this.scrollAndClick;
            fun.accept(x);
            Thread.currentThread().sleep(1000);
            WebElement y = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='" + label + "']/..//span[text()='" + value + "']")));

            fun.accept(y);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con la lista di valori identificata dalla label :" + label + " o selezionare il valore " + value + " Exception class: " + e.getClass());

        }

    }

    /**
     * Gestione Alert con oggetti Select non standard su salesforce
     *
     * @param label1 - Label che identifica la select su cui scatenare l'errore
     * @param label2 - Label che identifica la select su cui passare il focus
     */

    public void selectLighningValueAlert(String label1, String label2) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='" + label1 + "']/..//input")));
            Consumer<WebElement> fun = this.scrollAndClick;
            fun.accept(x);

            Thread.currentThread().sleep(1000);
            WebElement y = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='" + label2 + "']/..//input")));
            fun.accept(y);

        } catch (Exception e) {
//			e.printStackTrace();
//			//System.out.println("Unexpected exception!!! " + e.getClass());
//			throw new Exception("Impossibile interagire con la lista di valori identificata dalla label :"+label+" o selezionare il valore "+value+" Exception class: "+e.getClass());

        }

    }


    public void selectLighningValueInSrollView(String label, String value) throws Exception {
        try {

            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='" + label + "']/..//input")));
            Consumer<WebElement> fun = this.scrollAndClick;
            fun.accept(x);
            Thread.currentThread().sleep(1000);

            WebElement DIVelement = driver.findElement(By.xpath("//label[text()='" + label + "']/..//span[text()='" + value + "']"));
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("arguments[0].scrollIntoView(true)", DIVelement);
            fun.accept(DIVelement);

        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile aprile la lista o la label :" + label + " o selezionare il valore " + value + " Exception class: " + e.getClass());

        }

    }


    /**
     * Gestione degli oggetti Select non standard su salesforce
     *
     * @param section - Il titolo della macrosezione
     * @param label   - Label che identifica la select
     * @param value   - Valore della lista da selezionare
     */

    public void selectLighningValue(String section, String label, String value) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'" + section + "')]/following::label[text()='" + label + "']/..//input")));
            Consumer<WebElement> fun = this.scrollAndClick;
            fun.accept(x);
            Thread.currentThread().sleep(1000);
            WebElement y = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'" + section + "')]/following::label[text()='" + label + "']/..//span[text()='" + value + "']")));
            fun.accept(y);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con la lista di valori identificata dalla label :" + label + " o selezionare il valore " + value + " Exception class: " + e.getClass());

        }

    }


    public void sendText(String labelField, String value) throws Exception {
        String by = "//label[contains(text(),'" + labelField + "')]/following::input";
        objectManager(By.xpath(by), scrollAndSendKeys, value);
    }

    /**
     * Gestione degli oggetti Select non standard su salesforce
     *
     * @param label - Label che identifica la select
     * @param value - Valore della lista da selezionare
     */

    public void selectListOptionValue(String label, String value) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='" + label + "']/..//input")));
            Consumer<WebElement> fun = this.scrollAndClick;
            fun.accept(x);
            BiConsumer<WebElement, String> fun2 = this.scrollAndSendKeys;
            //WebElement y = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='"+label+"']/..//option[@value='"+value+"']")));
            fun2.accept(x, value);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con la lista di valori identificata dalla label :" + label + " o selezionare il valore " + value + " Exception class: " + e.getClass());

        }

    }


    /**
     * Gestione degli oggetti senza Frame
     *
     * @param by    - riconoscimento dell'oggetto
     * @param fun   - funtion da richiamare (Es. util.select)
     * @param param
     */
    public void objectManager(By by, Consumer<WebElement> fun) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(by));
            fun.accept(x);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con l'oggetto identificato dal seguente by :" + by.toString() + ". Exception class: " + e.getClass());

        }
    }


    public List<WebElement> objectManager(By by, Function<WebElement, List<WebElement>> fun) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(by));
            return fun.apply(x);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con l'oggetto identificato dal seguente by :" + by.toString() + ". Exception class: " + e.getClass());

        }
    }


    /**
     * Calcola una nuova potenza negli scenari di cambio potenza
     *
     * @param by    - riconoscimento dell'oggetto
     * @param fun   - funtion da richiamare (Es. util.select)
     * @param param
     */
    public Float getNuovaPotenza(float potenza) throws Exception {
        return potenza == 4.5f ? 3f : 4.5f;
    }

    public Integer getNuovaTensione(float tensione) throws Exception {
        return tensione == 220 ? 380 : 220;
    }

    public static class AutomaticFrame implements AutoCloseable {
        WebDriver driver;

        public AutomaticFrame(WebDriver driver, String frame) {

            this.driver = driver;
            WebElement frameEl = driver.findElement(By.xpath("//iframe[@name='" + frame + "']"));
            driver.switchTo().frame(frameEl);

        }

        public AutomaticFrame(WebDriver driver, int frame) {
            WebElement frameEl = driver.findElement(By.xpath("//iframe[@title='accessibility title'][" +
                    frame + "]"));
        }

        @Override
        public void close() throws Exception {
            driver.switchTo().defaultContent();
        }

    }

    public void objectManager(By by, BiConsumer<WebElement, Boolean> fun, Boolean param) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            WebElement x = wait.until(ExpectedConditions.elementToBeClickable(by));
            fun.accept(x, param);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con l'oggetto identificato dal seguente by :" + by.toString() + ". Exception class: " + e.getClass());

        }
    }

    public void objectManager(WebElement el, BiConsumer<WebElement, Boolean> fun, Boolean param) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            fun.accept(el, param);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con l'oggetto identificato dal seguente by :" + el.getTagName() + ". Exception class: " + e.getClass());

        }
    }

    public void objectManager(WebElement el, Consumer<WebElement> fun) throws Exception {
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(this.driver, 150)
                    .ignoring(WebDriverException.class);
            fun.accept(el);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Unexpected exception!!! " + e.getClass());
            throw new Exception("Impossibile interagire con l'oggetto identificato dal seguente by :" + el.getTagName() + ". Exception class: " + e.getClass());

        }
    }

    /**
     * Metodo che preleva dal foglio di stile determinate proprietà come risultato della computazione degli eventi before/after css
     *
     * @param el          --> Web Element da cui partire per valutarne le proprietà css
     * @param beforeAfter --> deve indicare quale selettore css, se before o after
     * @param property    --> indica quale proprietà css prelevare, esempio background, display etc..
     * @return string --> Ritorna le proprietà css in formato stringa
     * @throws Exception
     */

    public String getComputedStyle(WebElement el, String beforeAfter, String property) throws Exception {

        return ((JavascriptExecutor) driver).executeScript("return window.getComputedStyle(arguments[0], ':"
                + beforeAfter + "').getPropertyValue('" + property + "');", el).toString();
    }

    /**
     * Metodo che verifica che nel selettore ::after del foglio di stile
     * sia presente come background un determinato url("http:......")
     *
     * @param by  --> identificativo oggetto
     * @param url --> url da ricercare
     * @return false --> url non presente
     * @throws Exception
     */


    public boolean checkAfterBackgroundUrl(By by, String url) throws Exception {
        WebElement el = waitAndGetElement(by);
        String back = getComputedStyle(el, "after", "background");
        if (back.contains(url)) return true;
        else return false;

    }

    public void checkPageLoaded() throws Exception {
        driver.manage().timeouts().pageLoadTimeout(45, TimeUnit.SECONDS);
    }

    /**
     * A very crude way to get the browser download folder path, mimicking what a real user would do.
     * This method opens Chrome settings page and copies the download folder path listed there.
     * The advantage is this should be a multiplatform solution. Also it is not necessary to hard-code
     * the path on a module or preference file
     *
     * @throws Exception
     */
    public String getChromeDownloadFolderPath() throws Exception {
        //Open Chrome download settings
        driver.get("chrome://settings/downloads");
        //Return download path
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Thread.sleep(2000);
        WebElement element = (WebElement) js.executeScript("return document.querySelector('body > settings-ui').shadowRoot.querySelector('#main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('#advancedPage > settings-section:nth-child(3) > settings-downloads-page').shadowRoot.querySelector('#defaultDownloadPath')");
        String thePath = element.getText();
        driver.navigate().back();

        return thePath;
    }

    /**
     * It copies the text contained by the web page element identified by 'webPageItem'
     *
     * @param webPageItem : By instance that identifies a webpage element
     * @throws Exception
     */
    public void copy(By webPageItem) throws Exception {
        WebElement we = this.waitAndGetElement(webPageItem);
        we.sendKeys(Keys.chord(Keys.CONTROL, "c"));
    }

    /**
     * It pastes a text copied onto clipboard to the web page element identified by 'webPageItem'
     *
     * @param webPageItem : By instance that identifies a webpage element
     * @throws Exception
     */
    public void paste(By by) throws Exception {
        WebElement we = this.waitAndGetElement(by);
        we.sendKeys(Keys.chord(Keys.CONTROL, "v"));
    }


    public WebElement frameForElement(By elementLocator) throws Exception {
        WebElement theFrame = null;
        List<WebElement> frames = driver.findElements(By.xpath("//iframe"));
        int numberOfFrames = frames.size();
        System.out.println("Number of frames: " + numberOfFrames);
        for (int i = 0; i < numberOfFrames; i++) {
            try {
                driver.switchTo().frame(frames.get(i));
                try {
                    new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(elementLocator));
                    theFrame = frames.get(i);
                    System.out.println("The frame for the requested element has index: " + i);
                    break;
                } catch (NoSuchElementException e) {
                    System.out.println("Frame at index" + i + " doesn't contain requested element");
                }
            } catch (StaleElementReferenceException e) {
                System.out.println("Stale frame at index: " + i);
            } finally {
                driver.switchTo().defaultContent();
            }
        }

        return theFrame;
    }


    public void salesForceClassicModePlease() throws Exception {
        By userBtn = By.xpath("//div[contains(@class,'branding-user-profile bgimg')]/span[@class='uiImage']");
        By classicModeBtn = By.xpath("//a[contains(@class,'switch-to-aloha')]");
        try {
            if (verifyVisibility(userBtn, 20)) {
                objectManager(userBtn, scrollAndClick);
                if (verifyVisibility(classicModeBtn, 20)) {
                    objectManager(classicModeBtn, scrollAndClick);
                }
            }
        } catch (Exception e) {
            System.out.println("Already in classic mode");
        }
    }


    public void salesForceLightningModePlease() throws Exception {
        By lightningModeBtn = By.className("switch-to-lightning");
        try {
            if (verifyVisibility(lightningModeBtn, 20)) {
                objectManager(lightningModeBtn, scrollAndClick);
            }
        } catch (Exception e) {
            System.out.println("Already in Lightning mode");
        }
    }


    public int countElement(By obj) {
        return driver.findElements(obj).size();
    }

    public boolean verifyText(By objWithText, String textToCheck) {
        if ((driver.findElement(objWithText).getText().replaceAll("\\s+", " ").trim().toLowerCase()).equals(textToCheck.toLowerCase())) {
            return true;
        }
        return false;
    }

    public boolean verifyText(By element, String value, int timeout) throws Exception {
        try {
            new WebDriverWait(driver, timeout).until(ExpectedConditions.textToBe(element, value));
        } catch (Exception e) {
            System.out.println(e.toString());
            return false;
        }
        return true;
    }

    public String[] getAttributesNameWithJse(String xpath) {
        int count = countAttributesWithJse(xpath);
        String[] attributesName = new String[count];
        for (int i = 1; i <= count; i++) {
            attributesName[i - 1] = getAttributeNameByIndexWithJse(xpath, i);
        }
        return attributesName;
    }

    public int countAttributesWithJse(String xpath) {
        return
                (int)
                        Integer.valueOf
                                (
                                        (String)
                                                jse.executeScript(
                                                        "return document.evaluate(\"count(##/@*)\", document, null, XPathResult.STRING_TYPE, null).stringValue"
                                                                .replaceFirst("##", xpath)
                                                )
                                );
    }

    public String getAttributeNameByIndexWithJse(String xpath, int index) {
        return
                (String)
                        jse.executeScript
                                (
                                        "return document.evaluate(\"name((##/@*)[§§])\", document, null, XPathResult.STRING_TYPE, null).stringValue"
                                                .replaceFirst("##", xpath)
                                                .replaceFirst("§§", String.valueOf(index)
                                                )
                                );
    }

    public String getValueWithJse(String xpath) {
        return
                (String)
                        jse.executeScript
                                (
                                        "return document.evaluate(\"##\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value"
                                                .replaceFirst("##", xpath)
                                );
    }

    public String xpathToString(By xpath) throws Exception {
        String sp = " ";
        String xpc = By.xpath("/*").toString().split(sp, 2)[0];
        String[] st = xpath.toString().split(sp, 2);
        if (st[0].equalsIgnoreCase(xpc)) {
            return st[1];
        } else {
            throw new Exception(xpath.toString() + " is not xpathExpression.");
        }
    }
    /**
     * restituisce il frame attivo che contiene il webElement passato in input
     */
    public WebElement getFrameActive(By locator) throws Exception {
		driver.switchTo().defaultContent();
		WebElement ret = null;
		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
		boolean flag = false;
		for (WebElement webElement : els) {
			
			
			if(webElement.findElement(By.xpath("..")).isDisplayed()) {
				driver.switchTo().frame(webElement);
				System.out.println("Switch to fareme");
				if(verifyExistence(locator, 10))
				{
					System.out.println("Elemento presente");
					ret = webElement;
					break;
				}
				ret = webElement;
				break;
			}	

				
				
				
			}	
			
		
		return ret;
    }
    
    public WebElement getFrameActive() {
		driver.switchTo().defaultContent();
		WebElement ret = null;
		List<WebElement> els = driver.findElements(By.xpath("//iframe[@title ='accessibility title']"));
		boolean flag = false;
		for (WebElement webElement : els) {
			
			
			if(webElement.findElement(By.xpath("..")).isDisplayed()) {
				driver.switchTo().frame(webElement);
				ret = webElement;
				break;
			}	
			
		}
		return ret;
    }
    
    public boolean isBusiness(String cf) {
    	Pattern pattern = Pattern.compile("^[0-9]{11}$");
    	Matcher match = pattern.matcher(cf);
    	  if(match.matches()) { return true;}
    	    else {return false;}
    }
    
}







