package com.nttdata.qa.enel.util;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ManageDriver {
	SeleniumUtilities util;
	Logger LOGGER = Logger.getLogger("");
	
	public static RemoteWebDriver startDriver() throws Exception {
		WindowsProcessKiller.killChrome();
		System.setProperty("webdriver.chrome.driver",
				"src/main/resources/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		Logger LOGGER = Logger.getLogger("");
		RemoteWebDriver driver;
		driver = new ChromeDriver(options);
		// Setta timeout di default per il caricamento della pagina
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(200, TimeUnit.SECONDS);
//		LOGGER.log(Level.INFO,((HttpCommandExecutor) driver.getCommandExecutor()).getAddressOfRemoteServer().toString());
//		LOGGER.log(Level.INFO,driver.getSessionId().toString());
		return driver;

		
	}

	public static void closeDriver(RemoteWebDriver driver) throws Exception {
		driver.close();
		driver=null;
	}

	// slds-modal slds-fade-in-open slds-spinner--large
}
