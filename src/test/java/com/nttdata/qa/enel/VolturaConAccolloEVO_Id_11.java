package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class VolturaConAccolloEVO_Id_11 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("CODICE_FISCALE", "52490720977");
        prop.setProperty("CODICE_FISCALE_REFERENTE", "BSNTTN88T10F839T");
        prop.setProperty("CODICE_FISCALE_CL_USCENTE", "RSSPLA61L28E522S");
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("TIPO_DELEGA", "Nessuna delega");
        prop.setProperty("POD", "IT002E0000383A");
        prop.setProperty("PROCESSO", "Avvio Voltura con accollo EVO");
        prop.setProperty("ATTIVABILITA", "Non Attivabile");
        prop.setProperty("RUN_LOCALLY", "Y");
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

        String args[] = {nomeScenario};
        prop.store(new FileOutputStream(nomeScenario), null);
        prop.load(new FileInputStream(nomeScenario));

        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID22.main(args);
        SelezioneCausaleVolturaCheckCampiBusiness.main(args);
    }

    @After
    public void fineTest() throws Exception {
		prop.load(new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
    }
}
