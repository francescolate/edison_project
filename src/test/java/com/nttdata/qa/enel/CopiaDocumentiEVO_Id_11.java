package com.nttdata.qa.enel;


import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class CopiaDocumentiEVO_Id_11 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void inizioTest() throws Exception {
        this.prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_bo);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_bo);
        prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
        prop.setProperty("CODICE_FISCALE", "01629361005");
        prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
        prop.setProperty("NOME_UTENTE", "712268222");
        prop.setProperty("TIPOLOGIA_DOC", "ESTRATTO_CONTO");
        prop.setProperty("TIPOLOGIA_DMS", "Tutte");
        prop.setProperty("NUMERO_PDR", "Doc-0332122018");
        prop.setProperty("CANALE_INVIO", "EMAIL");
        prop.setProperty("INDIRIZZO_EMAIL", "testing.crm.automation@gmail.com");
        prop.setProperty("RUN_LOCALLY", "Y");
    }

    @Test
    public void eseguiTest() throws Exception {

//        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load( new FileInputStream(nomeScenario));
/*
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
        VerificaSelezioneFornitureCopiaDocumenti.main(args);
        SelezionaSezioneTipologiaDocumenti.main(args);
        VerificaSezioneEstrattoContoCopiaDocumenti.main(args);
        AzioniDettagliSpedizioneCopiaDocumenti.main(args);
        SalvaNumeroDocumento.main(args);
        RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
        VerificaCasePlicoCopiaDocumenti.main(args);*/
        RecuperaLinkItemCaseCopiaDocumenti.main(args);
        VerificaModelloOggettoEmailGenericoCopiaDocumentiWorkbench.main(args);
        VerificaModelloCorpoEmailGenericoCopiaDocumentiWorkbench.main(args);
    }

    @After
    public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}
