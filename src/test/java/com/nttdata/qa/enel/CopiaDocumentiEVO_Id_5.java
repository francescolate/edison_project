package com.nttdata.qa.enel;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniDettagliSpedizioneCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID21;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaLinkItemCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.RecuperaStatoDocumentModelloPlicoCopiaDocumentiPostBatch;
import com.nttdata.qa.enel.testqantt.RecuperaStatusModelloPlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.SalvaNumeroDocumento;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaSezioneTipologiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaCasePlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaKitContrattualeCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaKitContrattualePadreCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaNumeroRichiestaCaseCopiaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaSelezioneFornitureCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoDocumentInviatoModelloPlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoInLavorazioneSottostatoInviatoCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoCaseCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaStatoSottostatoDocumentoWorkbench;


import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class CopiaDocumentiEVO_Id_5 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
		prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
		prop.setProperty("NOME_UTENTE", "723535469");
		prop.setProperty("TIPOLOGIA_DOC", "KIT_CONTRUATTUALE");
		prop.setProperty("ID_DOCUMENTO", "0332118324");
		prop.setProperty("ID_DOCUMENTO_2", "0332118327");
		prop.setProperty("CANALE_INVIO", "FAX");
		prop.setProperty("NUMERO_FAX", "0612345678");	
		prop.setProperty("STATO_DOCUMENTO", "SOTTOSTATO");
	    prop.setProperty("SOTTOSTATO_DOCUMENTO", "RICEVUTO");
		prop.setProperty("RUN_LOCALLY", "Y");
	};

	@Test
	public void eseguiTest() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load( new FileInputStream(nomeScenario));
	/*
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID21.main(args);
		VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
		VerificaSelezioneFornitureCopiaDocumenti.main(args);
		SelezionaSezioneTipologiaDocumenti.main(args);
		VerificaKitContrattualePadreCopiaDocumenti.main(args);
		VerificaKitContrattualeCopiaDocumenti.main(args);
		AzioniDettagliSpedizioneCopiaDocumenti.main(args);	
		SalvaNumeroDocumento.main(args);
		VerificaStatoSottostatoCaseCopiaDocumentiWorkbench.main(args);
		VerificaStatoInLavorazioneSottostatoInviatoCopiaDocumentiWorkbench.main(args);

		//verifica presenza reconds document associati al Case
		RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
		VerificaCasePlicoCopiaDocumenti.main(args);
		//verifica presenza item con campo Link valorizzato
		RecuperaLinkItemCaseCopiaDocumenti.main(args);		

		//da eseguire dopo batch
		//verifica avanzamento document con Modello = Plico in Stato=Inviato
		RecuperaStatoDocumentModelloPlicoCopiaDocumentiPostBatch.main(args);
		VerificaStatoDocumentInviatoModelloPlicoCopiaDocumenti.main(args);
		//verifica case: Stato - Chiuso, Sottostato - Ricevuto  
		VerificaStatoSottostatoCaseCopiaDocumentiWorkbench.main(args);
		VerificaStatoSottostatoDocumentoWorkbench.main(args);*/
	};

	@After
	public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};

}
