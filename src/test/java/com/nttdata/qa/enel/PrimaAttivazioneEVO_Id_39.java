package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.components.colla.SetUtenzaS2SAttivazioni;
import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;

import core.Decrypt;

/*



 
*/

/**
 * Processo Prima Attivazione EVO ELE con Fibra da S2S - E2E
	Profilo S2S
	Cliente Residenziale 
	Commodity Ele - Prodotto - Scegli Tu
	

 */
public class PrimaAttivazioneEVO_Id_39 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	
	@Before
	public void inizioTest() throws Exception {
	
		this.prop = new Properties();
		
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		
		//Property creazione cliente
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "AUTOMATION");
		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO", "F");
		prop.setProperty("DATA_NASCITA", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA", "Napoli");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");	
		prop.setProperty("CAP", "27036");
		
		
		
		//CODICE FISCALE
		prop.setProperty("CODICE_FISCALE", "TMTVVQ85C44F839V");
		
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("DIPENDENTE_ENEL","N");
		
		//configurazione prodotto
		prop.setProperty("PRODOTTO", "SCEGLI OGGI LUCE");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
		prop.setProperty("ELIMINA_VAS", "SI");
		
		
		//Configurazione Fibra
		prop.setProperty("OPZIONE_FIBRA","SI");
		prop.setProperty("INDIRIZZO_FIBRA_OK","SI");
		prop.setProperty("TELEFONO_FIBRA", "06221234567");
		prop.setProperty("REGIONE_FIBRA", "LAZIO");
		prop.setProperty("PROVINCIA_FIBRA", "ROMA");
		prop.setProperty("CITTA_FIBRA", "ROMA");
		prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
		prop.setProperty("CIVICO_FIBRA", "196");
		prop.setProperty("COMPILA_EMAIL_FIBRA", "Y");
		prop.setProperty("EMAIL_FIBRA", "cli.res111@gmailEnel.com");
		prop.setProperty("SCELTA_MELITA","NO");
		prop.setProperty("CONSENSO_DATI_FIBRA_OK","SI");

		//VERIFICHE FINALI ITEM FIBRA
		prop.setProperty("STATO_ATTESO_FIBRA","IN ATTESA");
		
		//Property prima  attivazione
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("TIPO_CLIENTE", "Casa");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
	


		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("CANALE_INVIO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "VOCAL ORDER");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		
		prop.setProperty("RESIDENTE", "NO");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("REGISTRAZIONE_VOCAL", "Y"); //serve ad evitare che nella gestione firma vocale venga cliccato il pulsante registrazione vocale
		prop.setProperty("SKIP_CONTATTI_CONSENSI", "Y");
		
		prop.setProperty("DOCUMENTI_DA_VALIDARE",
				"Documento di riconoscimento;ML_MdaSingleRes_Vocal_Istanza326_Nazionale");
		
		// Dati R2d ELE
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("STATO_R2D", "AW");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Prima Attivazione");
		prop.setProperty("TIPO_CONTATORE", "CE - Contatore Elettronico Non Orario");
		prop.setProperty("POTENZA_FRANCHIGIA", "220");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Ammissibilità A01");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito A01");
		
		
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY", "Y");
		
		
	}
	@Test
	public void eseguiTest() throws Exception {
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		//Creazione cliente
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CreazioneNuovoCliente.main(args);
		
		
		
		
		//Scenario PA
		RecuperaPodNonEsistente.main(args);
		SetUtenzaS2SAttivazioni.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID17.main(args);
		
		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEVO.main(args);
		CompilaDatiFornituraElePrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		CommodityEleResidenzialePrimaAttivazione.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostaleEVO.main(args);		
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoElettricoResidenziale.main(args);
		
		GestioneCVPPrimaAttivazione.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);
		
		
		CompilaIndirizzoFibra.main(args); //TODO: FR 04.10.2021 Aggiustare compilazione numero cellulare
		//MODALITA' FIRMA
		ModalitaFirmaPA.main(args); //SOSTITUISCE ModalitaFirmaPrimaAttivazione.main(args); 
		
		//TODO: non funziona il cilck su forza vocal order per la parte fibra
		RegistrazioneVocaleFibra_PA.main(args);//TODO: FR 04.10.2021 far funzionare registrazione vocale fibra //N.B. con la registrazione vocale non è necessario premere conferma
		RiepilogoOffertaDopoConferma.main(args);//SOSTITUISCE ConfermaPrimaAttivazione.main(args); 
		
		CaricaVerificaValidaDocumenti.main(args);//sostituisce CaricaEValidaDocumenti.main(args);

		
		
		
		//Verifica offerta in stato chiusa - nel caso sia in ricevuta rilanciare dopo qualche minuto
		RecuperaStatusOffer.main(args);
		VerificaOffertaInChiusa.main(args);
		VerificaStatoFibra.main(args); //TODO FR 23.08.2021 la fibra è rimasta in Attesa invece di andare in bozza 215661317 - 215661342
		
		

		

		
		//Verifiche POD ELE-->aspettare 3 min
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		VerificaStatusR2DPresaInCarico.main(args);
		SetOIRicercaDaOIOrdine.main(args);
		
		
		
		//Esiti R2D ELE
		LoginR2D_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSPortale_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);
		
		
		//Lanciare dopo 4 ore
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		
		//Verifica finale su chiusura Richiesta
		RecuperaStatusCase_and_Order.main(args);
		VerificheAttivazioneRichiesta.main(args);
		VerificaStatoFibra.main(args);
		
		
		
	}
	
	
	@After
    public void fineTest() throws Exception{
		
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
        
  }
	
}
