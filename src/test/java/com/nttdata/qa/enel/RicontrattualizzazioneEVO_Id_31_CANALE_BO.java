package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AnnullaOffertaGenerico;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornituraRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOffertaRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID22;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID23;
import com.nttdata.qa.enel.testqantt.IndirizziFatturazione;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaECanaleDInvioVoltura;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostale;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaItemAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCaseItem;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_RVC_31;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOrderHeader;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RicercaOffertaSubentro;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetPropertyNAMECFPOD;
import com.nttdata.qa.enel.testqantt.SetQueryRecuperaDatiRicontrattualizzazione119_1_bus;
import com.nttdata.qa.enel.testqantt.SetQueryRecuperaDatiRicontrattualizzazione123;
import com.nttdata.qa.enel.testqantt.VerificaAsset;
import com.nttdata.qa.enel.testqantt.VerificaCaseChiuso;
import com.nttdata.qa.enel.testqantt.VerificaItemAssetRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.VerificaOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaSottoStatoCaseEspletato;
import com.nttdata.qa.enel.testqantt.VerificaSottostatoCaseOrderInviato;
import com.nttdata.qa.enel.testqantt.VerificaStatoCaseInAttesa;
import com.nttdata.qa.enel.testqantt.VerificaStatoCaseItemEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatoInAttesa;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusPhaseStart;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DND;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREInAttesa;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBND;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class RicontrattualizzazioneEVO_Id_31_CANALE_BO {

	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK",Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_forzatura_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_forzatura_salesforce);
		prop.setProperty("PROCESSO", "Avvio Cambio Prodotto EVO");	
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA","Y");
		prop.setProperty("PRODOTTO", "SCEGLI OGGI WEB LUCE");
		//prop.setProperty("SCELTA_ABBONAMENTI", "GREEN");
		prop.setProperty("CAP", "00184");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
	    prop.setProperty("TAB_SGEGLI_TU", "SI");
	    prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
	    prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("RIGA_DA_ESTRARRE","13");
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));
/*
		SetQueryRecuperaDatiRicontrattualizzazione123.main(args);
		RecuperaDatiWorkbench.main(args);
		SetPropertyNAMECFPOD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID23.main(args);
		ConfermaCheckList.main(args);
		CheckSezioneSelezioneFornituraRicontrattualizzazione.main(args);
		ConfermaRiepilogoOffertaRicontrattualizzazione.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusEVO.main(args);
        ConfiguraProdottoElettricoResidenziale.main(args);
        ConfermaFatturazioneElettronicaSWAEVO.main(args);
        IndirizziFatturazione.main(args);
        ConsensiEContatti.main(args);
        ModalitaFirmaECanaleDInvioVoltura.main(args);
        ConfermaOffertaAllaccioAttivazione.main(args);//preso in presti da allaccio e attivazioni
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        RicercaOffertaSubentro.main(args);
        CaricaEValidaDocumenti.main(args);
        
        //attendere 5 minuti
        Thread.sleep(300000);

        RecuperaStatusOffer.main(args);
        VerificaOffertaInChiusa.main(args);
        RecuperaStatusCase_and_Offer.main(args);
        VerificaOffertaInChiusa.main(args);
        VerificaSottostatoCaseOrderInviato.main(args);

        //query 115
        RecuperaStatusCaseItem.main(args);
        VerificaStatoInAttesa.main(args);

        //query 105
        //RecuperaStatusOrderHeader.main(args);
        //Thread.currentThread().sleep(60000);

        //query 103
        RecuperaStatusCase_and_Order_RVC_31.main(args);
        VerificaStatusR2DND.main(args);*/
        //VerificaStatusUDBND.main(args);
        VerificaStatusPhaseStart.main(args);
        //VerificaStatoCaseInAttesa.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};
}
