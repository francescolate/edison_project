package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.components.colla.SetUtenzaS2SAttivazioni;
import com.nttdata.qa.enel.testqantt.AnnulloVoltura;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura2;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrelloBusiness;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.CigCupEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraVolturaBSN;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraVolturaSenzaAccolloRes;
import com.nttdata.qa.enel.testqantt.CompilaSezioneIndirizziVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfermaTP2;
import com.nttdata.qa.enel.testqantt.ContattiEConsensiVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.ContattiEConsensiVolturaSenzaAccolloRes;
import com.nttdata.qa.enel.testqantt.CreaOffertaVoltura3_Finalizzazione;
import com.nttdata.qa.enel.testqantt.GetNumeroCellulareOTP;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.MetodoDiPagamentoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.ModalitàFirmaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.NuovoMetodoDiPagamento;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Voltura;
import com.nttdata.qa.enel.testqantt.RecuperaNumeroContratto;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaOtp;
import com.nttdata.qa.enel.testqantt.RecuperaRichiestaTouchPoint;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.ReferenteSwaEVO;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaWebPrivato;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaWebPrivatoVoltura;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SalvaNumeroContratto;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Globale_Richiesta_InLavorazione;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID8;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SetUtenzaS2SResidenziale;
import com.nttdata.qa.enel.testqantt.SezioneCVPVoltura;
import com.nttdata.qa.enel.testqantt.SezioneFatturazioneElettronicaVolturaSenzaAccollo;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVO;
import com.nttdata.qa.enel.testqantt.UpdateNumeroCellulareOTP;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraTouchPoint;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificheOrdinePostQuery;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_CambioUso_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.APIService;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaSenzaAccolloEVO_Id_8 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CONSUMO","3");
		prop.setProperty("SCEGLI_METODO_PAGAMENTO", "Y");
		prop.setProperty("FLAG_RESIDENTE","SI");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("CAP", "00198");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");

		//prodotto 
		prop.setProperty("PRODOTTO", "SCEGLI OGGI WEB LUCE");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");

		prop.setProperty("MODALITA_FIRMA","DIGITAL");
		prop.setProperty("TIPOCLIENTE","RESIDENZIALE");
		prop.setProperty("EMAIL","testing.crm.automation@gmail.com");
		prop.setProperty("CELLULARE","3929926614");
		prop.setProperty("EFFETTUARE_CONFERMA","Y");

		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("DOCUMENTI_DA_VALIDARE", "Documento di riconoscimento");
		prop.setProperty("CANALE", "DIGITAL");

		prop.setProperty("STATO_GLOBALE_RICHIESTA", "BOZZA");
		prop.setProperty("STATO_RICHIESTA", "BOZZA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - NULL / DRAFT");

		//dati R2D
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
		prop.setProperty("TIPO_OPERAZIONE", "VOLTURA_SENZA_ACCOLLO");
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE","Areti");
		//prop.setProperty("STATO_R2D", "AW");
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Voltura senza accollo");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_ELE","Esito Ammissibilità - Ammissibilità Voltura VT1 AU");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esito Richiesta Voltura VT1 AU");
		prop.setProperty("CODICE_CAUSALE_5OK_ELE","1234");
		prop.setProperty("RIGA_DA_ESTRARRE","1");	


		prop.setProperty("QUERY", Costanti.recupera_CF_POD_per_Voltureid8);
		prop.setProperty("RUN_LOCALLY","Y");

		return prop;
	};

	@Test
	public void eseguiTest() throws Exception{

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		SetQueryRecupera_CF_POD_VSA_ID8.main(args);
		RecuperaDatiWorkbench.main(args);
		//		in = new FileInputStream(nomeScenario);
		//		prop.load(in);
		//		prop.setProperty("NOME", prop.getProperty("QUERY_ACCOUNT.NAME"));
		//		prop.setProperty("CODICE_FISCALE_CL_USCENTE", prop.getProperty("QUERY_ACCOUNT.NE__FISCAL_CODE__C"));
		//		prop.setProperty("POD", prop.getProperty("QUERY_ITA_IFM_POD_PDR__C"));
		//		prop.store(new FileOutputStream(nomeScenario), null);

		SetPropertyVSA_Data_Query.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args);
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID17.main(args);

		ConfermaCheckList.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);
		CheckSezioneSelezioneFornitura.main(args);

		RiepilogoOffertaVoltura.main(args);
		ConfermaRiepilogoOfferta.main(args);
		SelezioneUsoFornitura.main(args);
		// compiladatiForniRSD
		CompilaDatiFornituraVolturaSenzaAccolloRes.main(args);
		MetodoDiPagamentoOffertaVoltura.main(args);
		ConfermaScontiBonusEVO.main(args);


		CompilaSezioneIndirizziVolturaSenzaAccollo.main(args);
		AzioniSezioneCarrello.main(args);
		SezioneCVPVoltura.main(args);
		ContattiEConsensiVolturaSenzaAccolloRes.main(args);
		SezioneFatturazioneElettronicaVolturaSenzaAccollo.main(args);
		ModalitàFirmaVolturaSenzaAccollo.main(args);		

		CheckRiepilogoOfferta.main(args);

		VerificheRichiestaDaPod.main(args);
		SalvaNumeroContratto.main(args);

		RecuperaLinkTP2Voltura.main(args);
		SetUtenzaMailResidenziale.main(args);

		LoginPortalTP2.main(args); 

		//TODO
		RiepilogoOffertaWebPrivatoVoltura.main(args);
		// RiepilogoOffertaWebPrivato.main(args);

		//		//qualora fosse necessario recuperare l'otp
		//		GetNumeroCellulareOTP.main(args);        
		//		RecuperaOtp.main(args);		  	
		RecuperaRichiestaTouchPoint.main(args);
		VerificaChiusuraTouchPoint.main(args);

		SetPropertyVSA_Stato_Globale_Richiesta_InLavorazione.main(args);
		SetUtenzaS2SResidenziale.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args);

		//la richiesta resta nello stato "INVIATA" perchè sull'offerta c'è un ko sull'attività:KO OCR Validazione web parziale

		CaricaEValidaDocumentiVoltureSenzaAccollo.main(args);
		VerificaDocumento.main(args);



		VerificheRichiestaDaPod.main(args);
		//VerificheStatoRichiesta.main(args);
		RecuperaOrderIDDaPod.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		SalvaIdBPM.main(args);

		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);


		LoginR2D_ELE.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_ELE.main(args);

		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);



		//Da eseguire dopo qualche ora
		SetPropertyVSA_Stato_Ordine_Espletato.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args);
		VerificheRichiestaDaPod.main(args); 
		VerificheStatoRichiesta.main(args);	


	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
