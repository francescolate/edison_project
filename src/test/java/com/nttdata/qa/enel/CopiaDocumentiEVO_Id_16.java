package com.nttdata.qa.enel;


import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class CopiaDocumentiEVO_Id_16 {
    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void inizioTest() throws Exception {
        this.prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
        //prop.setProperty("CODICE_FISCALE", "CNCLCU44C14C990Q");
        prop.setProperty("CODICE_FISCALE", "PTRSVN61P60F839P");
        prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
        //prop.setProperty("NOME_UTENTE", "304613835");
        prop.setProperty("NOME_UTENTE", "610895035");
        //prop.setProperty("NUMERO_FATTURA", "0000003043028672");
        prop.setProperty("NUMERO_FATTURA", "0000003034652743");
        prop.setProperty("TIPOLOGIA_DOC", "FATTURE");
        prop.setProperty("TIPOLOGIA_DMS", "Tutte");
        prop.setProperty("ID_DOCUMENTO", "a3W1l000001CXNpEAO");
        prop.setProperty("CANALE_INVIO", "PEC");
        prop.setProperty("INDIRIZZO_EMAIL", "cliente1@pec.enel.it");
        prop.setProperty("MODALITA_DUPLICATO", "Invio dettaglio fattura");
        prop.setProperty("RUN_LOCALLY", "Y");
    }

    ;

    @Test
    public void eseguiTest() throws Exception {

//      prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
/*
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID21.main(args);
        VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
        VerificaSelezioneFornitureCopiaDocumenti.main(args);
        SelezionaSezioneTipologiaDocumenti.main(args);
        VerificaSelezioneFattureCopiaDocumenti.main(args);
        AzioniDettagliSpedizioneCopiaDocumenti.main(args);
        SalvaNumeroDocumento.main(args);
        RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
        VerificaCasePlicoCopiaDocumenti.main(args);
        RecuperaLinkItemCaseCopiaDocumenti.main(args);
        VerificaModelloOggettoPecGenericoCopiaDocumentiWorkbench.main(args);
        VerificaModelloCorpoPecGenericoCopiaDocumentiWorkbench.main(args);*/
    }


    @After
    public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);

    }

    ;

}
