package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AnnullaOfferta;
import com.nttdata.qa.enel.testqantt.AnnullaOffertaGenerico;
import com.nttdata.qa.enel.testqantt.AssociazioneProdottiServizi;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornituraRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.CigCugpRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVONonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID22;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID23;
import com.nttdata.qa.enel.testqantt.IndirizziFatturazione;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaECanaleDInvioVoltura;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaECanaleInvioRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostale;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaItemAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCaseItem;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOrderHeader;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetPropertyNAMECFPOD;
import com.nttdata.qa.enel.testqantt.SetQueryRecuperaDatiRicontrattualizzazione119_1_bus;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.VerificaAsset;
import com.nttdata.qa.enel.testqantt.VerificaCaseChiuso;
import com.nttdata.qa.enel.testqantt.VerificaItemAssetRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.VerificaOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaSottoStatoCaseEspletato;
import com.nttdata.qa.enel.testqantt.VerificaSottostatoCaseOrderInviato;
import com.nttdata.qa.enel.testqantt.VerificaStatoCaseItemEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatoInAttesa;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DND;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREInAttesa;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBND;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class RicontrattualizzazioneEVO_Id_34_SINGLE_ELE_S2S_Annullment_Offerta {

	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK",Costanti.salesforceLink);
		prop.setProperty("USERNAME", "s2s_user_cfi_l@enelfreemarket.com.uat" );
		prop.setProperty("PASSWORD", "UATSFDc_2024");
		prop.setProperty("PROCESSO", "Avvio Cambio Prodotto EVO");	
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA","Y");
		prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
		prop.setProperty("ASSOCIAZIONE_PRODOTTI", "Y");
		//prop.setProperty("SCELTA_ABBONAMENTI", "GREEN");
		prop.setProperty("CAP", "00184");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("CANALE_INVIO", "EMAIL");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
		prop.setProperty("MOTIVO_ANNULLAMENTO", "QC Rinuncia Cliente");
		prop.setProperty("RIGA_DA_ESTRARRE", "9");
		prop.setProperty("RUN_LOCALLY","Y");
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));
/*
		SetQueryRecuperaDatiRicontrattualizzazione119_1_bus.main(args);
		RecuperaDatiWorkbench.main(args);
		SetPropertyNAMECFPOD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID23.main(args);
		ConfermaCheckList.main(args);
		CheckSezioneSelezioneFornituraRicontrattualizzazione.main(args);
		ConfermaRiepilogoOfferta.main(args);
		PagamentoBollettinoPostale.main(args);
		ConfermaScontiBonusEVO.main(args);
		ConfiguraProdottoElettricoNonResidenziale.main(args);
		AssociazioneProdottiServizi.main(args);
        SplitPaymentSwaEVO.main(args);//nuovo
        CigCugpRicontrattualizzazione.main(args);//nuovo
	    ConfermaFatturazioneElettronicaSWAEVONonResidenziale.main(args);
		IndirizziFatturazione.main(args);
		ConsensiEContatti.main(args);
		ModalitaFirmaECanaleInvioRicontrattualizzazione.main(args);
		ConfermaOffertaAllaccioAttivazione.main(args);
		AnnullaOfferta.main(args);

		//attendere 5 minuti
		Thread.sleep(300000);*/
		RecuperaStatusOffer.main(args);
		VerificaOffertaAnnullata.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};
}
