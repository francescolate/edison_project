package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazionePodCessato;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_SimulatoreEsiti2iRG;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
//COMPLETO
public class PrimaAttivazioneEVO_Id_3 {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "MDGGVR80C59H511K");
//			prop.setProperty("QUERY", Costanti.query_pod_cessato_gas);
			prop.setProperty("POD", "01613945002489");
			prop.setProperty("TIPO_UTENZA", "PE");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("MERCATO", "Libero");
			prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
			prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
			prop.setProperty("CAP", "24060");
			prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "N");
			prop.setProperty("ESITO_OFFERTABILITA", "");
			prop.setProperty("REGIONE", "LAZIO");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("CITTA", "ROMA");
			prop.setProperty("INDIRIZZO", "VIA NIZZA");
			prop.setProperty("CIVICO", "4");
			prop.setProperty("MODIFICA_IMPIANTO", "NO");
			prop.setProperty("VERIFICA_ESITO_PRECHECK", "Y");
			prop.setProperty("DESCRIZIONE_ESITO_PRECHECK", "Lo stato del POD non è congruente con l’operazione di PRIMA ATTIVAZIONE");
			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
			prop.setProperty("CAP_FORZATURA", "00135");
			prop.setProperty("CITTA_FORZATURA", "ROMA");
			
			//Integrazione R2D GAS
			prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/home.do");
			prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d_swa);
			prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d_swa);
			
			prop.setProperty("RUN_LOCALLY","Y");
		
		};
		
		
		
		@Test
        public void eseguiTest() throws Exception{
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};
			
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
				
			//Inserimento pod nel simulatore
			LoginR2D_GAS.main(args);
			R2D_SimulatoreEsiti2iRG.main(args);
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID15.main(args);
			SelezioneMercatoPrimaAttivazioneEVO.main(args);
			InserimentoFornitureEVO.main(args);
			CompilaDatiFornituraElePrimaAttivazionePodCessato.main(args);
		
		};
		
		@After
	    public void fineTest() throws Exception{
			String args[] = {nomeScenario};
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	  };
		
		
		
		
		
	

}
