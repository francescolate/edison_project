package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.components.lightning.SelezioneUsoFornituraComponentEVO;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiDelibera40;
import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumentiMultiELE;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckListText;
import com.nttdata.qa.enel.testqantt.CheckModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CheckSezioneContattiConsensi;
import com.nttdata.qa.enel.testqantt.CheckSezioneDelegatoSuRichiesta;
import com.nttdata.qa.enel.testqantt.CheckTouchPoint;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CommodityEleResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CommodityGasNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CommodityMultiGasNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraGasPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneAppuntamentoPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.GestioneReferentePrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_APPEZZOTTA;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID16;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID18;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginPortalTP2;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ModificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PagamentoBonificoEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneMULTIEVOGas;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneMULTIEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoMULTIEVOResidenziale;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2PrimaAttivazione;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2Voltura;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaPodMultiNonEsistenti;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_Multi_Pod1;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_Multi_Pod2;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOffertaDaTab;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SalvaNumeroContratto;
import com.nttdata.qa.enel.testqantt.SalvaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Dual;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Multi_Pod1;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Multi_Pod2;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Multi_Pod1;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Multi_Pod2;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.ValidaDocumentiAttività;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOIOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInInviata;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Accertamento_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Istruttoria_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIntermedie_GAS;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_22A {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("TIPO_DELEGA","Nessuna delega");
		prop.setProperty("CODICE_FISCALE","04922840113");
		prop.setProperty("COMMODITY", "GAS4");
		prop.setProperty("TIPO_UTENZA", "PE	");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "20121");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("REGIONE", "LOMBARDIA");
		prop.setProperty("PROVINCIA", "MILANO");
		prop.setProperty("CITTA", "MILANO");
		
		prop.setProperty("INDIRIZZO", "ALBERICO ALBRICCI");
		prop.setProperty("CIVICO", "2");
		prop.setProperty("CAP_FORZATURA", "20121");
		prop.setProperty("CITTA_FORZATURA", "MILANO");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("CATEGORIA_CONSUMO", "Industria");
		prop.setProperty("CATEGORIA_USO", "Uso tecnologico (artigianale-industriale)");
		prop.setProperty("CATEGORIA_MARKETING", "ISOLANTI");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("ORDINE_GRANDEZZA", "INDUSTRIA");
		prop.setProperty("PROFILO_CONSUMO", "PRODUZIONE");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CLASSE_PRELIEVO", "7 giorni");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "Y");
		prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
		prop.setProperty("CODICE_UFFICIO", "0000000");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("PROCESSO", "Avvio Prima Attivazione EVO");
		prop.setProperty("PRODOTTO","New_Soluzione Gas Impresa Business");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("TELEFONO", "3394675542");
		prop.setProperty("CELLULARE", "3394675542");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("DOCUMENTI_DA_VALIDARE",
				"C.T.E.;Documento di riconoscimento;ML_MdaMultiBus_No_Vocal;ML_NAU_Nazionale;ML_NAU_Nazionale");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("TIPO_CLIENTE", "Non Residenziale");

			
		// Dati R2d
		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/217.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d);
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Attivazione non in del 40");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		prop.setProperty("MATRICOLA_CONTATORE","11");		
		prop.setProperty("CP_GESTORE", "12345");
		prop.setProperty("LETTURA_CONTATORE", "112233");
		prop.setProperty("ANNO_COSTRUZIONE_CONTATORE", "2019");
		prop.setProperty("ACCESSIBILITA_229", "1 (Accessibile)");
		prop.setProperty("COEFFICIENTE_C", "1");
		prop.setProperty("CLASSE_CONTATORE", "G0004");
         
		prop.setProperty("RUN_LOCALLY","Y");
      
	}
		

	@Test
	public void eseguiTest() throws Exception {
	
		prop.store(new FileOutputStream(nomeScenario), null);
		 
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		
		prop.load(in);

	RecuperaPodMultiNonEsistenti.main(args);
	LoginSalesForce.main(args);
	SbloccaTab.main(args);
	//CercaClientePerNuovaInterazioneEVO.main(args);  
	CercaClientePerNuovaInterazione.main(args);
	ProcessoPrimaAttivazioneNonResidenzialeEVO.main(args);
		
		
		
		
		
	Identificazione_Interlocutore_ID16.main(args); 
	SelezioneMercatoPrimaAttivazioneEVO.main(args);	  
	ProcessoPrimaAttivazioneMULTIEVOGas.main(args);  
	VerificaCreazioneOffertaPrimaAttivazione.main(args);
	VerificaRiepilogoOffertaPrimaAttivazione.main(args);
	GestioneReferentePrimaAttivazioneEVO.main(args);  
	SelezioneUsoFornitura.main(args);
	CommodityMultiGasNonResidenzialePrimaAttivazione.main(args);  
	ConfermaIndirizziPrimaAttivazione.main(args); 
	ConfermaScontiBonusSWAEVO.main(args); 
	GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);  
	PagamentoBollettinoPostaleEVO.main(args); 
	SplitPaymentSwaEVO.main(args);
	CigCugpSwaEVO.main(args);
	
	ConfiguraProdottoSWAEVOGasNonResidenziale.main(args); 
	GestioneCVPPrimaAttivazione.main(args);
	ConsensiEContattiPrimaAttivazione.main(args);
	ModalitaFirmaPrimaAttivazione.main(args); 
	ConfermaPrimaAttivazione.main(args); 
	SalvaNumeroContratto.main(args);

	CaricaVerificaValidaDocumentiMultiELE.main(args);
		//GestioneAppuntamentoPrimaAttivazione.main(args);
		
		//Verifica offerta in stato chiusa
		RecuperaStatusOffer.main(args);
		VerificaOffertaInChiusa.main(args); 
		SalvaIdOfferta.main(args);		

		// Verifiche POD_1
		SetPODtoRetreiveGAS_Multi_Pod1.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
		SalvaIdOrdine.main(args);
		SetOIRicercaDaOIOrdine.main(args);
		VerificaStatusR2DPresaInCarico.main(args); 

				
		 //INTEGRAZIONE CON R2D
        LoginR2D_GAS.main(args);
        R2D_VerifichePodIniziali_GAS.main(args); 
        R2D_InvioPSPortale_1OK_GAS.main(args);
        R2D_CaricamentoEsiti_3OK_GAS.main(args); 
        R2D_CaricamentoEsiti_5OK_GAS.main(args); 
        R2D_VerifichePodFinali_GAS.main(args);
        
       
		// Verifiche POD_2
		SetPODtoRetreiveGAS_Multi_Pod2.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
		SalvaIdOrdine.main(args);
		SetOIRicercaDaOIOrdine.main(args);
		VerificaStatusR2DPresaInCarico.main(args);
		
		
		 //INTEGRAZIONE CON R2D
        LoginR2D_GAS.main(args);
        R2D_VerifichePodIniziali_GAS.main(args);
        R2D_InvioPSPortale_1OK_GAS.main(args);
        R2D_CaricamentoEsiti_3OK_GAS.main(args);
        R2D_CaricamentoEsiti_5OK_GAS.main(args);
        R2D_VerifichePodFinali_GAS.main(args);

			
			
			//Verifiche post esiti R2D
			
			RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
			VerificaStatusR2DAttivazPod.main(args);
			VerificaStatusOrdineEspletato.main(args);
			
			RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
			VerificaStatusR2DAttivazPod.main(args);
			VerificaStatusOrdineEspletato.main(args);
			
			//Lanciare dopo 4 ore
			
			//Verifiche finali 
			SetPODtoRetreiveGAS_Multi_Pod1.main(args);
			RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
			VerificaStatusSAP_ISUAttivazPod.main(args);
			VerificaStatusSEMPREAttivazPod.main(args);
			VerificheAttivazioneRichiesta.main(args);
			
			Thread.currentThread().sleep(3000); 
				
			SetPODtoRetreiveGAS_Multi_Pod2.main(args);
			RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
			VerificaStatusSAP_ISUAttivazPod.main(args);
			VerificaStatusSEMPREAttivazPod.main(args);
			VerificheAttivazioneRichiesta.main(args);
			
	};

	@After
	public void fineTest() throws Exception {
	/*	
		String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
*/
	};

}
