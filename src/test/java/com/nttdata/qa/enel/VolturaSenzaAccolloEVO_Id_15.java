package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura2;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVolturaGas;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrelloGas;
import com.nttdata.qa.enel.testqantt.CaricaDocumentoSubentro;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckListText;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziVolture;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.CreaOffertaVoltura3_Finalizzazione;
import com.nttdata.qa.enel.testqantt.CreaOffertaVoltura_Salva_In_Bozza;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModificaOffertaEConferma;
import com.nttdata.qa.enel.testqantt.ModificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.NuovoMetodoDiPagamento;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVolturaGas;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SetCPGestoreDaOIRichiesta;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyStato_R2D_AW;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_CF_Uscente_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Stato_Ordine_Espletato;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID15;
import com.nttdata.qa.enel.testqantt.ValidaDocumentiSubentro;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaSenzaAccolloEVO_Id_15 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
		prop.setProperty("CODICE_FISCALE", "CNTNCL66M08H501J");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CONSUMO","3");
		prop.setProperty("SCEGLI_METODO_PAGAMENTO","Y");

		prop.setProperty("FLAG_RESIDENTE","NO");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("CAP", "00198");
		prop.setProperty("CITTA", "ROMA");
		//Carrello
		prop.setProperty("PRODOTTO","NEW_Energia Sicura Gas_15 percento");
		prop.setProperty("VAS_EX_GAS","SI");

		prop.setProperty("MODALITA_FIRMA","NO VOCAL");
		prop.setProperty("CANALE","EMAIL");
		prop.setProperty("EMAIL","d.calabresi@reply.it");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "GAS");

		// Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		//prop.setProperty("OI_RICERCA", prop.getProperty("OI_ORDINE"));
		prop.setProperty("TIPO_OPERAZIONE", "VOLTURA_SENZA_ACCOLLO");
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");

		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_GAS","Areti");
		//prop.setProperty("STATO_R2D", "AW"); 
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS", "Voltura senza accollo");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		prop.setProperty("CODICE_CAUSALE_5OK_GAS","1234");
		prop.setProperty("RIGA_DA_ESTRARRE","3");	


		prop.setProperty("QUERY", Costanti.recupera_CF_POD_per_VSA_15);

		prop.setProperty("RUN_LOCALLY","Y");

		return prop;
	};

	@Test
	public void eseguiTest() throws Exception{
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		SetQueryRecupera_CF_POD_VSA_ID15.main(args);
		RecuperaDatiWorkbench.main(args);


		SetPropertyVSA_CF_Uscente_POD.main(args);		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//	CercaClientePerNuovaInterazioneEVO.main(args);   
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args); 
		Identificazione_Interlocutore_ID15.main(args);
		CheckListText.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args); 
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);
		CheckSezioneSelezioneFornitura.main(args);
		RiepilogoOffertaVolturaGas.main(args);
		AzioniRiepilogoOffertaVolturaGas.main(args);
		ConfermaIndirizziVolture.main(args);
		AzioniRiepilogoOffertaVoltura2.main(args); 
		ConfiguraProdottoSWAEVOGasResidenziale.main(args);

		//fino a modalità firma e canale
		CreaOffertaVoltura_Salva_In_Bozza.main(args);
		//Modulo modifica offerta e conferma ha bisogno di controlli sulle sezione che non sempre sono abilitate
		ModificaOffertaEConferma.main(args);

		CheckRiepilogoOfferta.main(args); 
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaOfferta.main(args);
		CaricaEValidaDocumenti.main(args);

		VerificheRichiestaDaPod.main(args);
		VerificheStatoRichiesta.main(args); 
		RecuperaOrderIDDaPod.main(args); 

		SetOIRicercaDaOIOrdine.main(args);
		SetPropertyStato_R2D_AW.main(args);



		SetCPGestoreDaOIRichiesta.main(args);

		LoginR2D_GAS.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_GAS.main(args);
		//TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSAcquirenteUnico_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_GAS.main(args); 
		R2D_VerifichePodFinali_GAS.main(args); 


		//Da eseguire dopo qualche ora
		SetPropertyVSA_Stato_Ordine_Espletato.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaRichiesta.main(args); 
		VerificheRichiestaDaPod.main(args); 
	};

	@After
	public void fineTest() throws Exception{

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
