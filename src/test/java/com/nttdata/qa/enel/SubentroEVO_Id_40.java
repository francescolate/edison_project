package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class SubentroEVO_Id_40 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "00184");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "ALTRI SERVIZI");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("DISALIMENTABILITA", "SI");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("TIPO", "SUBENTRO");
        prop.setProperty("TIPO_OPERAZIONE", "SUBENTRO_EVO");
        prop.setProperty("USO", "Uso Diverso da Abitazione");
        prop.setProperty("SEZIONE_ISTAT", "Y");       
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("CLIENTE_BUSINESS", "Y");
        prop.setProperty("CODICE_UFFICIO", "0000000");
        prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
        prop.setProperty("SEZIONI_TOTALI", "16");
        prop.setProperty("CONTA", "0");
        
        prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
        prop.setProperty("SOTTO_STATO_RICHIESTA", "OFFER - ANNULLATA");
        prop.setProperty("STATO_RICHIESTA", "CHIUSO");
        prop.setProperty("STATO_R2D", "N.D.");
        prop.setProperty("STATO_SAP", "N.D.");
        prop.setProperty("STATO_SEMPRE", "N.D.");
        prop.setProperty("STATO_ELEMENTO_RICHIESTA", "ANNULLATO");      
        
        prop.setProperty("RIGA_DA_ESTRARRE", "7");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("COMMODITY", "ELE");
        
        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
        
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "NO");
        prop.setProperty("SELEZIONA_CANALE_INVIO", "Y");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO_FIRMA", "POSTA");
  

        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));

        RecuperaPodNonEsistente.main(args);    
        SetSubentroquery_subentro_id13.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyCF.main(args);  
        
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID14.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroSingolaEleGas.main(args);      
      
        SelezioneReferente.main(args);
        SelezioneUsoFornitura.main(args);
        CommodityEleNonResidenzialeSubentro.main(args);
        ConfermaIndirizziPrimaAttivazione.main(args);
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
        PagamentoBollettinoPostale.main(args);
        ConfermaScontiBonusEVO.main(args);
        SplitPaymentEVO.main(args);
        CigCupEVO.main(args);    
        ConfiguraProdottoElettricoNonResidenziale.main(args);
        GestioneCVPAllaccioAttivazione.main(args);     
        ConsensiEContattiSubentro.main(args);
        ModalitaFirmaSubentro.main(args);
       
        AnnullaOffertaGenerico.main(args);
        //Startare 1 minuti dopo l'emissione
        Thread.currentThread().sleep(60000);      
        VerificheRichiestaAnnullaPredisposizionePresa.main(args);
        
    }

    @After
    public void fineTest() throws Exception {
       prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}
