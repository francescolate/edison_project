package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura2;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiVoltureSenzaAccollo;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckListText;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.CreaOffertaVoltura3_Finalizzazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.NuovoMetodoDiPagamento;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleVolture;
import com.nttdata.qa.enel.testqantt.ProcessoAvvioVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaLinkCreazioneAccount;
import com.nttdata.qa.enel.testqantt.RecuperaLinkTP2;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.ScontiBonusCodiceAmicoErrato;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetQueryRecuperaCF_POD_per_Volture;
import com.nttdata.qa.enel.testqantt.SetUtenzaMailResidenziale;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.VerificheStatoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaSenzaAccolloEVO_Id_16 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
		prop.setProperty("CODICE_FISCALE", "CRSFNC89M49H769B");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CONSUMO","3");
		prop.setProperty("SCEGLI_METODO_PAGAMENTO","Y");

		prop.setProperty("FLAG_RESIDENTE","NO");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");

		prop.setProperty("PRODOTTO","Speciale Luce 60");
		prop.setProperty("MODALITA_FIRMA","DOCUMENTI VALIDI");
		prop.setProperty("CANALE","EMAIL");
		prop.setProperty("EMAIL","d.calabresi@reply.it");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");


		prop.setProperty("QUERY", Costanti.recupera_CF_POD_per_Volture);
		prop.setProperty("CHECK_LIST_TEXT","ATTENZIONE PRIMA DI PROCEDERE VERIFICARE:Che il cliente disponga dei dati del cliente uscente (CF, PI, Utenza) e dei dati del sito (POD/PDR)Se il cliente è in Salvaguardia è necessaria l’Istanza di SalvaguardiaINOLTRE VERIFICARE:PER USO ABITATIVO-    Se il cliente vuole attivare il RID/SDD è necessario l’IBANPER USI DIVERSI DALL’ABITAZIONE-    Se il cliente vuole attivare il RID/SDD è necessario l’IBAN-    Se il cliente è una Pubb. Amm. è necessario il Codice Ufficio-    Se il cliente è soggetto a Legge 136 è necessario almeno uno tra CIG e CUP-    Se il cliente è soggetto a Split Payment è necessario indicare Data Inizio e Data Fine validità-    In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)INFORMAZIONI UTILI:In caso di Dual ricordati che le forniture dovranno avere stessi: USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO In caso di Multi ricordati che le forniture dovranno avere stessi: COMMODITY – USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO – PRODOTTO - CODICE UFFICIO (se previsto)Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto.  Fibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento della voltura. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta    In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente: -   numero di cellulare  -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associataSCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("RUN_LOCALLY","Y");

		return prop;
	};

	@Test
	public void eseguiTest() throws Exception{
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		SetQueryRecuperaCF_POD_per_Volture.main(args);
		RecuperaDatiWorkbench.main(args);

		SetPropertyVSA_Data_Query.main(args);		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//	CercaClientePerNuovaInterazioneEVO.main(args); 
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID15.main(args);
		ConfermaCheckList.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);

		CheckSezioneSelezioneFornitura.main(args);
		PagamentoBollettinoPostaleVolture.main(args); 
		ScontiBonusCodiceAmicoErrato.main(args);


	};

	@After
	public void fineTest() throws Exception{

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};
}
