package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class SubentroEVO_Id_26 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "20121");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("USO", "Uso Diverso da Abitazione");
        prop.setProperty("ISTAT", "Y");
        prop.setProperty("POD", "ENERGIA");
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("CLIENTE_BUSINESS", "Y");
        prop.setProperty("POPOLA_FATTURAZIONE_ELETTRONICA", "NO");
        prop.setProperty("PRODOTTO", "GiustaXte Impresa");
        prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
        prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - INVIATA");
        prop.setProperty("STATO_RICHIESTA", "INVIATA");
        prop.setProperty("STATO_R2D", "N.D.");
        prop.setProperty("STATO_SAP", "N.D.");
        prop.setProperty("STATO_SEMPRE", "N.D.");
        prop.setProperty("CONSENSO_MARKETING_TELEFONICO", "");
        prop.setProperty("CONSENSO_MARKETING_CELLULARE", "");
        prop.setProperty("CONSENSO_TERZI_CELLULARE", "");
        prop.setProperty("CONSENSO_TERZI_TELEFONICO", "");
        prop.setProperty("CONSUMO_ANNUO", "1255");
        prop.setProperty("RIGA_DA_ESTRARRE", "5");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("CONTAINER_RIEPILOGO_OFFERTE", "Y");
        prop.setProperty("PRODOTTO", "GiustaXte Impresa");
        prop.setProperty("TIPO_FORNITURA", "ELETTRICO");
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

        //prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
/*
        SetSubentroquery_subentro_id26.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyPODCF.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID14.main(args);
        SezioneMercatoSubentroLiberoAggiungiFornitura.main(args);
        System.out.println("fine test");*/
    }

    @After
    public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}
