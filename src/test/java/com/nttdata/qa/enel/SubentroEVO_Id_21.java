package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import com.nttdata.qa.enel.testqantt.*;

import org.apache.poi.util.SystemOutLogger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;



public class SubentroEVO_Id_21 {

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK",Costanti.salesforceLink);
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("LOCALITA_ISTAT", "ROMA");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("CAP", "00184");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("PROCESSO", "Avvio Subentro EVO");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ALTRI SERVIZI"); 
		prop.setProperty("RESIDENTE","NO");  
        prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "1250");
		prop.setProperty("NUMERO_DOCUMENTO", "1231");
		prop.setProperty("RILASCIATO_DA", "ABCD");
		prop.setProperty("RILASCIATO_IL", "01/01/2020");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "16");
		prop.setProperty("POTENZA_FRANCHIGIA", "16");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("TIPO_OPERAZIONE", "SUBENTRO_EVO");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("SEZIONE_ISTAT", "N");
		prop.setProperty("LIST_TEXT","N");
		prop.setProperty("CLIENTE_BUSINESS","N");
		prop.setProperty("POPOLA_FATTURAZIONE_ELETTRONICA","");
		prop.setProperty("CODICE_UFFICIO","0000000");
		prop.setProperty("CANALE_INVIO","SDI");
		prop.setProperty("SEZIONI_TOTALI","16");
		prop.setProperty("CONTA","0");
		prop.setProperty("PRODOTTO", "GiustaXte");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - INVIATA");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("TIPO_CLIENTE", "Residenziale");
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("RUN_LOCALLY","Y");
		
		prop.setProperty("CODICE_FISCALE", "PCCCRL39M69L117R");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("PRECHECK2G", "N");
		
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("VERIFICA_POTENZA", "LIMITATA");
		prop.setProperty("CANALE_INVIO_FIRMA", "POSTA");
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{

    	//prop.store(new FileOutputStream(nomeScenario), null);
		String[] args = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));
/*
		RecuperaPodNonEsistente.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazionePerReferenteEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID17.main(args);
		SezioneMercatoSubentro.main(args);
		InserimentoFornitureSubentroSingolaEleGas.main(args);
	 
		SelezioneUsoFornitura.main(args);
		CommodityEleResidenzialeSubentro.main(args);
        ConfermaIndirizziPrimaAttivazione.main(args);
        GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostale.main(args);
		ConfermaScontiBonusEVO.main(args);
	    ConfiguraProdottoElettricoResidenziale.main(args); 
	    GestioneCVPAllaccioAttivazione.main(args);
		ConsensiEContattiSubentro.main(args);
		ModalitaFirmaSubentro.main(args);	
		ConfermaOffertaAllaccioAttivazione.main(args);

		LoginSalesForce.main(args);
		RicercaOfferta.main(args);
		CaricaEValidaDocumenti.main(args);
		
		// Verifica contenuto campo  "Tipo Potenza" che il campo sia popolato con il valore "Limitata"
		Thread.sleep(60000);
		Recupera_Tipo_Potenza.main(args);
		VerificaTipoPotenza.main(args);
		*/
	}
	
	@After
    public void fineTest() throws Exception{
		 
		 InputStream in = new FileInputStream(nomeScenario);
         prop.load(in);
         this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
         ReportUtility.reportToServer(this.prop);	
         
	}
}
