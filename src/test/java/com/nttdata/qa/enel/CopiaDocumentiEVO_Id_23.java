package com.nttdata.qa.enel;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniDettagliSpedizioneCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID21;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.RecuperaLinkItemCaseCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.RecuperaStatusModelloPlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.SalvaNumeroDocumento;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaSezioneTipologiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaCasePlicoCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaModelloCorpoEmailGenericoCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaModelloOggettoEmailGenericoCopiaDocumentiWorkbench;
import com.nttdata.qa.enel.testqantt.VerificaNumeroRichiestaCaseCopiaDocumento;
import com.nttdata.qa.enel.testqantt.VerificaSelezioneFornitureCopiaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaSezioneDimostratoPagamentoMultiploCopiaDocumenti;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class CopiaDocumentiEVO_Id_23 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "TSSGPP23E04F843Z");
		prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
		prop.setProperty("NOME_UTENTE", "630415047");
		prop.setProperty("TIPOLOGIA_DOC", "DIMOSTRATO_PAGAMENTO");
		prop.setProperty("TIPOLOGIA_DMS", "Tutte");
		prop.setProperty("ID_DOCUMENTO", "Doc-0332096772");	
		prop.setProperty("ID_DOCUMENTO_BIS",  "Doc-028375509");	
		prop.setProperty("CANALE_INVIO", "EMAIL");	
		prop.setProperty("INDIRIZZO_EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("RUN_LOCALLY", "Y");
	};

	@Test
	public void eseguiTest() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load( new FileInputStream(nomeScenario));
	/*	
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID21.main(args);
		VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
		VerificaSelezioneFornitureCopiaDocumenti.main(args);		
		SelezionaSezioneTipologiaDocumenti.main(args);	
		VerificaSezioneDimostratoPagamentoMultiploCopiaDocumenti.main(args);		
		AzioniDettagliSpedizioneCopiaDocumenti.main(args);
		SalvaNumeroDocumento.main(args);
		RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
		VerificaCasePlicoCopiaDocumenti.main(args);
		RecuperaLinkItemCaseCopiaDocumenti.main(args);	*/
		VerificaModelloOggettoEmailGenericoCopiaDocumentiWorkbench.main(args);
		VerificaModelloCorpoEmailGenericoCopiaDocumentiWorkbench.main(args);
	};

	@After
	public void fineTest() throws Exception {
		prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	};
}
