 package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class SubentroEVO_Id_31 {
    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);//password_salesforce_s2s_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "STS");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "20121");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");

        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("MERCATO", "Libero");
  
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("USO", "Uso Diverso da Abitazione");
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("LIST_TEXT", "N");
        
        prop.setProperty("RIGA_DA_ESTRARRE", "7");
        prop.setProperty("RUN_LOCALLY", "Y");
    
        prop.setProperty("CODICE_FISCALE", "CLZGRL86R14D883O");
        prop.setProperty("MODULE_ENABLED", "Y");
        
        prop.setProperty("SET_POD_AS", "3");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("VERIFICA_VALIDAZIONE", "Y");
        
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));

        RecuperaPodMultiNonEsistenti.main(args);     
        SetSubentroqueryPod_subentro_id31.main(args); 
        RecuperaDatiWorkbench.main(args); 
        SetPropertyPODas.main(args);
        LoginSalesForce.main(args); 
        SbloccaTab.main(args);  
        CercaClientePerNuovaInterazioneEVO.main(args);  
        AvvioProcesso.main(args); 
        Identificazione_Interlocutore_ID14.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroMultiEle.main(args);   
        System.out.println("Fine test");
        
    }

    @After
    public void fineTest() throws Exception {
         prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }
}
