package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.components.colla.SetUtenzaS2SAttivazioni;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CommodityGasResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraGasPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaIndirizzoFibra;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class PrimaAttivazioneEVO_Id_37 {

	/**

	 Avvio processo Prima Attivazione EVO . con Fibra  - indirizzo fibra non valido
	 Profilo PE
	 Cliente Residenziale (senza email in anagrafica)
	 Commodity Gas - Prodotto - Speciale Gas 60

	 */

	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {

		this.prop = new Properties();

		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");



		//CODICE FISCALE
		prop.setProperty("CODICE_FISCALE", "TMTVVQ85C44F839V");



		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("DIPENDENTE_ENEL","N");

		//Property creazione cliente
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "AUTOMATION");
		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO", "F");
		prop.setProperty("DATA_NASCITA", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA", "Napoli");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");	
		prop.setProperty("CAP", "27036");
		
		//configurazione prodotto
		prop.setProperty("PRODOTTO","NEW_E-Light Gas_WEB");
		prop.setProperty("OPZIONE_FIBRA","SI");
		//prop.setProperty("MOD_MAIL_CERTIFICATA","SI");
		//prop.setProperty("MAIL_CERTIFICATA","testing.crm.automation@gmail.com");

		//Configurazione Fibra
		prop.setProperty("INDIRIZZO_FIBRA_OK","NO");
		prop.setProperty("TELEFONO_FIBRA", "06221234567");
		prop.setProperty("CONSENSO_DATI_FIBRA_OK", "SI");
		prop.setProperty("REGIONE_FIBRA", "LAZIO");
		prop.setProperty("PROVINCIA_FIBRA", "ROMA");
		prop.setProperty("CITTA_FIBRA", "ROMA");
		prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
		prop.setProperty("CIVICO_FIBRA", "20");
		prop.setProperty("COMPILA_EMAIL_FIBRA", "Y");
		prop.setProperty("EMAIL_FIBRA", "cli.res111@gmail.com");
		prop.setProperty("SCELTA_MELITA","NO");
		prop.setProperty("CONSENSO_DATI_FIBRA_OK","SI");


		//Property prima  attivazione
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("TIPO_CLIENTE", "Casa");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		
		prop.setProperty("SELEZIONA_CANALE_INVIO", "");
		prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		
		
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");

		prop.setProperty("SKIP_CONTATTI_CONSENSI", "Y");
		
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("RUN_LOCALLY", "Y");

	}

	@Test
	public void eseguiTest() throws Exception {
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		//Creazione cliente
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CreazioneNuovoCliente.main(args);




		//Scenario PA
		RecuperaPodNonEsistente.main(args);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID15.main(args);




		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEVO.main(args);		
		CompilaDatiFornituraGasPrimaAttivazione.main(args);		
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		CommodityGasResidenzialePrimaAttivazione.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostaleEVO.main(args);		
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoSWAEVOGasResidenziale.main(args);

		GestioneCVPPrimaAttivazione.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);

		ModalitaFirmaPrimaAttivazione.main(args);
		CompilaIndirizzoFibra.main(args); //non si abilita il tasto conferma - chiedere afunzionale
	


	}

	@After
	public void fineTest() throws Exception{
		
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}
}
