package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AnnullaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.AzioniSezioneCarrello;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckListText;
import com.nttdata.qa.enel.testqantt.CheckMessaggioErrorePrecheck2G;
import com.nttdata.qa.enel.testqantt.CheckMessaggioOkPrecheck2G;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPodMessagErrorMercatoLiberoFui;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziVolture;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.InserimentoIndirizzoEsecuzioneLavoriEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.MetodoDiPagamentoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaECanaleDInvioVoltura;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleVolture;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RiepilogoOffertaVoltura;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_CF_Uscente_POD;
import com.nttdata.qa.enel.testqantt.SetPropertyVSA_Data_Query;
import com.nttdata.qa.enel.testqantt.SetPropertyVolturaConAccolloEVONOMECFPOD;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID31;
import com.nttdata.qa.enel.testqantt.SetQueryRecupera_CF_POD_VSA_ID33;
import com.nttdata.qa.enel.testqantt.SetVolturaConAccolloEVOrecupera_CF_POD_per_VoltureConAccolloid23;
import com.nttdata.qa.enel.testqantt.VerificaStatoOffertaAnnullata;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaSenzaAccolloEVO_Id_33 {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
		prop.setProperty("CODICE_FISCALE", "CNTNCL66M08H501J");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("PROCESSO", "Avvio Voltura senza accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA","Y");
		prop.setProperty("FLAG_RESIDENTE","NO");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE");
		prop.setProperty("SCEGLI_METODO_PAGAMENTO","Y");
		prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "ABITAZIONI PRIVATE");
		prop.setProperty("TITOLARITA","Proprietà o Usufrutto");
		prop.setProperty("CONSUMO","3");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("PRODOTTO","Scegli Tu Ore Free");
		prop.setProperty("ORE_FREE","00");

		prop.setProperty("CANALE","EMAIL");
		prop.setProperty("EMAIL","d.calabresi@reply.it");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_CLIENTE", "Residenziale");
		prop.setProperty("CAP", "00198");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");


		//i seguenti POD IT002E9144271A e IT001E60115876 l’esito KO per contatore 2G come fatto in precedenza 
		//		prop.setProperty("QUERY", Costanti.recupera_CF_POD_per_Volture_33);
		//		prop.setProperty("NOME", "PERCOPO BRUNO");
		//		prop.setProperty("CODICE_FISCALE_CL_USCENTE", "BLZRND51E16B077J");
		//		prop.setProperty("POD", "IT002E5519407A");
		prop.setProperty("RUN_LOCALLY","Y");

		return prop;
	};

	@Test
	public void eseguiTest() throws Exception{
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);


		SetQueryRecupera_CF_POD_VSA_ID33.main(args);
		RecuperaDatiWorkbench.main(args);
		SetPropertyVSA_CF_Uscente_POD.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args);
		CercaClientePerNuovaInterazione.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID15.main(args);
		ConfermaCheckList.main(args);
		SelezioneMercatoVolturaSenzaAccolloEVO2.main(args);
		CheckSezioneSelezioneClienteUscenteAndInsertPod.main(args);

		CheckSezioneSelezioneFornitura.main(args);


		RiepilogoOffertaVoltura.main(args);


		AzioniRiepilogoOffertaVoltura.main(args);


		ConfermaIndirizziVolture.main(args);


		ConfermaScontiBonusEVO.main(args);



		ConfiguraProdottoElettricoResidenziale.main(args);

		//da modificare per adeguamento tl (check precheck da mettere in ok)
		CheckMessaggioOkPrecheck2G.main(args);

		AnnullaOffertaAllaccioAttivazione.main(args);

		RecuperaStatusCase_and_Offer.main(args);

		VerificaStatoOffertaAnnullata.main(args);

	};

	@After
	public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

	};
}
