package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSAP;
import com.nttdata.qa.enel.testqantt.AssegnaAttivitaScartiSEMPRE;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaEValidaTuttiIDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCupEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneBSN;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaAllaccioAttivazioneNoRes;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoIndirizzoEsecuzioneLavoriEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForce_Forzatura;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PagamentoPreventivo;
import com.nttdata.qa.enel.testqantt.ProcessoAllaccioAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_senza_POD;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaAttivita_Forzatura;
import com.nttdata.qa.enel.testqantt.RicercaOffertaDaTab;
import com.nttdata.qa.enel.testqantt.RilavoraScarti;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SbloccaTab_Forzatura;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoAllaccioAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetOrderLineItemCommoditytoRetreive;
import com.nttdata.qa.enel.testqantt.SetUtenzaBo;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVO;
import com.nttdata.qa.enel.testqantt.VerificaCaseInLavorazione_senza_POD;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOIOdineStatoPreventivoInviato;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInLavorazione_senza_POD;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SAP;
import com.nttdata.qa.enel.testqantt.VerificaSeForzabile_KO_SEMPRE;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineRichiestaPreventivo;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DInviatoAlDistributore;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPreventivoDaCompletare;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoPreventivo_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIntermedie_ELE;
import com.nttdata.qa.enel.testqantt.r2d.SetAttributeR2DCodDTE01;
import com.nttdata.qa.enel.testqantt.r2d.SetAttributeR2DCodDTN02;
import com.nttdata.qa.enel.testqantt.r2d.SetAttributeR2DPreventivoSopraSoglia;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class AllaccioAttivazioneEVO_Id_35 {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "00499970036");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "S2S");
			prop.setProperty("TIPO_CLIENTE", "Business");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("TIPO_OPERAZIONE", "ALLACCIO_EVO");
			prop.setProperty("MERCATO", "Libero");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("CITTA", "ROMA");
			prop.setProperty("INDIRIZZO", "VIA NIZZA");
			prop.setProperty("CIVICO", "4");
			prop.setProperty("CAP", "00135");
			prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
			prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "ALTRI SERVIZI");
			prop.setProperty("RESIDENTE", "NO");
			prop.setProperty("USO_ENERGIA", "Ordinaria");
			prop.setProperty("TITOLARITA", "Uso/Abitazione");
			prop.setProperty("TENSIONE", "220");
			prop.setProperty("ASCENSORE", "NO");
			prop.setProperty("POTENZA", "4");
			prop.setProperty("DISALIMENTABILITA", "SI");
			prop.setProperty("TIPO_MISURATORE", "Non Orario");
			prop.setProperty("CONSUMO_ANNUO", "1231");
			prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
			prop.setProperty("SPLIT_PAYMENT", "No");
		    prop.setProperty("FLAG_136", "NO");
	        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
	        prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
	        prop.setProperty("ELIMINA_VAS", "Y");
			prop.setProperty("CANALE_INVIO", "POSTA");
			prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
			prop.setProperty("CLASSIFICAZIONE_PREVENTIVO", "NON PREDETERMINABILE");
			prop.setProperty("MODALITA_INVIO_PREVENTIVO", "Posta");
			prop.setProperty("MODALITA_FIRMA_PREVENTIVO", "No Vocal");
			prop.setProperty("TIPO_OI_ORDER", "Commodity");			
			prop.setProperty("RUN_LOCALLY","Y");
			//Integrazione R2D ELE
			prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
			prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d);
			prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d);
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			ProcessoAllaccioAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID14.main(args);
			SelezioneMercatoAllaccioAttivazioneEVO.main(args);
			InserimentoIndirizzoEsecuzioneLavoriEVO.main(args);
			VerificaCreazioneOffertaAllaccioAttivazione.main(args);
			VerificaRiepilogoOffertaAllaccioAttivazione.main(args);
			SelezioneUsoFornitura.main(args);
			CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneBSN.main(args);
			ConfermaIndirizziAllaccioAttivazione.main(args);
			ConfermaFatturazioneElettronicaAllaccioAttivazioneNoRes.main(args);
			PagamentoBollettinoPostaleEVO.main(args);
			SplitPaymentEVO.main(args);
			CigCupEVO.main(args);
			ConfermaScontiBonusEVO.main(args);
			ConfiguraProdottoElettricoNonResidenziale.main(args);
			GestioneCVPAllaccioAttivazione.main(args);
			ConsensiEContattiAllaccioAttivazione.main(args);
			ModalitaFirmaAllaccioAttivazione.main(args);
			ConfermaOffertaAllaccioAttivazione.main(args);
			ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile.main(args);
			
			
			//Controlli post emissione richiesta
			RecuperaStatusOffer.main(args);
			VerificaOffertaInLavorazione_senza_POD.main(args);
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaCaseInLavorazione_senza_POD.main(args);
			VerificaStatusOrdineRichiestaPreventivo.main(args);
			SalvaIdOrdine.main(args);
			
			//Integrazione con R2D
			SetAttributeR2DCodDTN02.main(args);
			LoginR2D_ELE.main(args);
			R2D_VerifichePodIniziali_ELE.main(args);
			R2D_InvioPSPortale_1OK_ELE.main(args);
			R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
			
			//Verifiche post esitazione R2D
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaStatusR2DInviatoAlDistributore.main(args);
			
			//		Creazione preventivo
			SetAttributeR2DPreventivoSopraSoglia.main(args);
			LoginR2D_ELE.main(args);
			R2D_CaricamentoPreventivo_ELE.main(args);
			R2D_VerifichePodIntermedie_ELE.main(args);
			
			//Verifiche post caricamento preventivo R2D
			RecuperaStatusCase_and_Order_senza_POD.main(args);
//			VerificaOIOdineStatoPreventivoRicevuto.main(args);
			VerificaStatusR2DPreventivoDaCompletare.main(args);
			
			//Verifiche stato OI in Preventivo Inviato-->Aspettare 10 min
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaOIOdineStatoPreventivoInviato.main(args);
			
			//Validazione preventivo su SFDC
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			RicercaOffertaDaTab.main(args);
			PagamentoPreventivo.main(args);
			CaricaEValidaTuttiIDocumenti.main(args);
			
			//Verifica offerta in stato chiusa
			RecuperaStatusOffer.main(args);
			VerificaOffertaInChiusa.main(args);
			
			RecuperaPodNonEsistente.main(args);
			
			//Integrazione con R2D-->aspettare 5 min
			SetAttributeR2DCodDTE01.main(args);
			LoginR2D_ELE.main(args);
			R2D_VerifichePodIniziali_ELE.main(args);
			R2D_InvioPSPortale_1OK_ELE.main(args);
			R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
			R2D_CaricamentoEsiti_5OK_ELE.main(args);
			R2D_VerifichePodFinali_ELE.main(args);
			
			//Lanciare dopo 4 ore
			
			//Order
			SetOrderLineItemCommoditytoRetreive.main(args);
			RecuperaStatusCase_and_Order.main(args);
			SetUtenzaBo.main(args);
			//Verifica se un eventuale KO SAP è riprocessabile
			VerificaSeForzabile_KO_SAP.main(args);
			//I seguenti moduli vengono eseguiti nel caso di un KO SAP riprocessabile
			AssegnaAttivitaScartiSAP.main(args);
			LoginSalesForce_Forzatura.main(args);
			SbloccaTab_Forzatura.main(args);
			RicercaAttivita_Forzatura.main(args);
			RilavoraScarti.main(args);
			
			RecuperaStatusCase_and_Order.main(args);
			VerificaStatusSAP_ISUAttivazPod.main(args);
			
			//Verifica se un eventuale KO SEMPRE è riprocessabile
			VerificaSeForzabile_KO_SEMPRE.main(args);
			//I seguenti moduli vengono eseguiti nel caso di un KO SEMPRE riprocessabile
			AssegnaAttivitaScartiSEMPRE.main(args);
			LoginSalesForce_Forzatura.main(args);
			SbloccaTab_Forzatura.main(args);
			RicercaAttivita_Forzatura.main(args);
			RilavoraScarti.main(args);
			
			RecuperaStatusCase_and_Order.main(args);
			VerificaStatusSEMPREAttivazPod.main(args);

			//Verifica finale su chiusura Richiesta
			
			SetOrderLineItemCommoditytoRetreive.main(args);
			RecuperaStatusCase_and_Order.main(args);
			VerificheAttivazioneRichiesta.main(args);



		};
		
		@After
	    public void fineTest() throws Exception{
            String args[] = {nomeScenario};
            InputStream in = new FileInputStream(nomeScenario);
            prop.load(in);
            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
            ReportUtility.reportToServer(this.prop);
      };
		
		
		
		
		
	

}
