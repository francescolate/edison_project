package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;





import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import com.nttdata.qa.enel.util.ReportUtility;

public class SubentroEVO_Id_32D {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "STS");
        prop.setProperty("RIGA_DA_ESTRARRE", "7");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "20121");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("TIPO_DOCUMENTO", "Patente");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("USO", "Uso Diverso da Abitazione");
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("CHECK_CODICE_AMICO", "Y");
        
       
        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {
        //prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
        /*
        RecuperaTrePodNonEsistenti.main(args);      
        SetSubentroquery_subentro_id26.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyCF.main(args);
        LoginSalesForcePE.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args); //inserimento CF
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID15.main(args);
        SezioneMercatoSubentro.main(args);
        InserimentoFornitureSubentroMultiEle.main(args);
        ConfermaScontiBonusSubentro.main(args);
        */
        System.out.println("fine test");
       
    }

    @After
    public void fineTest() throws Exception {
    	
    prop.load(new FileInputStream(nomeScenario));
    this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
    ReportUtility.reportToServer(this.prop);
    
    }
}
