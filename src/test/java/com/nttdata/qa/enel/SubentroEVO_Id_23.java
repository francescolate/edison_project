package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class SubentroEVO_Id_23 {

    Properties prop;
    final String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
        prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
        prop.setProperty("TIPO_UTENZA", "S2S");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        prop.setProperty("CAP", "00178");
        prop.setProperty("MERCATO", "Salvaguardia");   
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        prop.setProperty("INDIRIZZO", "VIA APPIA NUOVA");
        prop.setProperty("CIVICO", "1");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("USO", "Uso Diverso da Abitazione");
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("CLIENTE_BUSINESS", "Y");
        prop.setProperty("RIGA_DA_ESTRARRE", "7");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("COMMODITY", "GAS");
        prop.setProperty("VERIFICA_SALVAGUARDIA", "Non sei abilitato ad eseguire operazioni sul Mercato Salvaguardia. Invita il cliente a recarsi presso un Punto Enel.");
    
  

        return prop;
    }

    @Test
    public void eseguiTest() throws Exception {

        String args[] = {nomeScenario};
        //prop.store(new FileOutputStream(nomeScenario), null);
        prop.load(new FileInputStream(nomeScenario));
        /*
        RecuperaPodNonEsistente.main(args);     
        SetSubentroquery_subentro_id8.main(args);
        RecuperaDatiWorkbench.main(args);
        SetPropertyCF.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID14.main(args);
        SezioneMercatoSubentro.main(args);
        */
    }

    @After
    public void fineTest() throws Exception {
    	
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
        
    }

}
