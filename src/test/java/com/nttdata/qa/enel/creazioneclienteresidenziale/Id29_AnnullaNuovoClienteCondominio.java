package com.nttdata.qa.enel.creazioneclienteresidenziale;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.PartitaIva;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class Id29_AnnullaNuovoClienteCondominio {
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	SeleniumUtilities util;
	Properties prop;


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() throws Exception{
		Properties prop = new Properties();
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("TIPOLOGIA_CLIENTE", "CONDOMINIO");
		String piva = PartitaIva.getPartitaIVA("8");
		prop.setProperty("CODICE_FISCALE", piva);
		prop.setProperty("PARTITA_IVA", piva);
		prop.setProperty("RAGIONE_SOCIALE", "AUTOMATION TEAM");
		prop.setProperty("COGNOME_REFERENTE", "GALDIERO");
		prop.setProperty("NOME_REFERENTE", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO_REFERENTE", "M");
		prop.setProperty("DATA_NASCITA_REFERENTE", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA_REFERENTE", "Napoli");
		prop.setProperty("PROVINCIA", "NAPOLI");
		prop.setProperty("COMUNE", "NAPOLI");
		prop.setProperty("INDIRIZZO", "VIA CONTE DELLA CERRA");
		prop.setProperty("TELEFONO_CLIENTE", "0819987645");
		prop.setProperty("CIVICO", "12");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("ANNULLA_CREAZIONE","Y");
		//Solo per esecuzione locale, NO QANTT
		prop.setProperty("RUN_LOCALLY","Y");
		return prop;
	}


	@After
	public void tearDown() throws Exception{
	       InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);

	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
//
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CreazioneNuovoCliente.main(args);

	}




}

