package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CaricaDocumentoSubentro;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazionePerReferenteEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.CommodityGasResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.CompilaFibraSubentro;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoGasResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiSubentro;
import com.nttdata.qa.enel.testqantt.GestioneAppuntamento;
import com.nttdata.qa.enel.testqantt.GestioneAppuntamentoPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureSubentroSingolaEleGas;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaSubentro;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostale;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RicercaOffertaSubentro;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetPropertyCF;
import com.nttdata.qa.enel.testqantt.SetPropertyPODCF;
import com.nttdata.qa.enel.testqantt.SetSubentroProperty;
import com.nttdata.qa.enel.testqantt.SetSubentroquery_subentro_id14;
import com.nttdata.qa.enel.testqantt.SezioneMercatoSubentro;
import com.nttdata.qa.enel.testqantt.ValidaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraOfferta;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_4OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_GAS;
import com.nttdata.qa.enel.util.Costanti;


public class SubentroEVO_Fibra_ID48 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
	    prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
        prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
        prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "PE");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("MODULE_ENABLED", "Y");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("COMMODITY", "GAS3");
        prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
        //prop.setProperty("RIGA_DA_ESTRARRE", "24");
		
        prop.setProperty("TIPO_DOCUMENTO", "Patente");
		prop.setProperty("NUMERO_DOCUMENTO", "1231");
		prop.setProperty("RILASCIATO_DA", "ABCD");
		prop.setProperty("RILASCIATO_IL", "01/01/2020");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("CAP", "37121");
		prop.setProperty("SEZIONE_ISTAT", "N");
		prop.setProperty("LOCALITA_ISTAT", "ROMA");
		prop.setProperty("INDIRIZZO", "CORSO PORTA NUOVA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "VERONA");
		
		prop.setProperty("USO", "Uso Abitativo");

		prop.setProperty("CATEGORIA_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("CATEGORIA_USO", "Riscaldamento + uso cottura cibi e/o produzione di acqua calda sanitaria");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
		prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
		
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		
		prop.setProperty("PRODOTTO","NEW_E-Light Gas_WEB"); //usata perchè è l'unica che ha la fibra
	    prop.setProperty("OPZIONE_FIBRA","SI");
	    
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");

		prop.setProperty("CANALE_INVIO_FIRMA", "STAMPA LOCALE");
	    prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
	    
	    prop.setProperty("INSERISCI_FIBRA", "OK");
	    prop.setProperty("PROVINCIA_FIBRA", "ROMA");
	    prop.setProperty("INDIRIZZO_FIBRA", "VIA ALBERTO ASCARI");
	    prop.setProperty("CIVICO_FIBRA", "196");
	    prop.setProperty("CELLULARE_FIBRA", "3929926614");
	    prop.setProperty("BANNER_FIBRA", "Indirizzo coperto dalla fibra");
	    
	    prop.setProperty("GESTIONE_APPUNTAMENTO", "Y");
	    
	    prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
        prop.setProperty("STATO_R2D", "PRESO IN CARICO");
        prop.setProperty("STATO_SAP", "BOZZA");
        prop.setProperty("STATO_SEMPRE", "BOZZA");
        prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
        prop.setProperty("RIGA_DA_ESTRARRE", "33");
        
        //propR2D
		prop.setProperty("PROFILO_PRELIEVO_ANNUO", "001X1");
		
    	prop.setProperty("CIFRE_CONTATORE", "5");
		prop.setProperty("TIPO_PDR", "0");
		prop.setProperty("PRELIEVO_ANNUO", "1000");
		prop.setProperty("PRELIEVO_ANNUO_BUS", "3000");
		prop.setProperty("LETTURA_CONTATORE", "15000");
		prop.setProperty("TIPO_LETTURA", "Effettiva");
		prop.setProperty("COEFFICIENTE_C", "1,02");
	    
	    

		return prop;
	}
 
	@Test
	public void eseguiTest() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String[] args = { nomeScenario };
		prop.load(new FileInputStream(nomeScenario));
		
		RecuperaPodNonEsistente.main(args);
		
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args); // inserimento CF
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID15.main(args);		
		
		SezioneMercatoSubentro.main(args);
		InserimentoFornitureSubentroSingolaEleGas.main(args);
		
		SelezioneUsoFornitura.main(args);
		CommodityGasResidenzialeSubentro.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBollettinoPostale.main(args);
		ConfermaScontiBonusEVO.main(args);
		ConfiguraProdottoGasResidenzialeSubentro.main(args);
		ConsensiEContattiSubentro.main(args);
		ModalitaFirmaSubentro.main(args);
		CompilaFibraSubentro.main(args);
		
	    GestioneCVPAllaccioAttivazione.main(args);
	    ConfermaOffertaAllaccioAttivazione.main(args);	           
	    
	    RicercaOffertaSubentro.main(args);            
		CaricaEValidaDocumenti.main(args);

        RecuperaOrderIDDaPod.main(args);
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);
        
		LoginR2D_GAS.main(args);		
		TimeUnit.SECONDS.sleep(5);	 
		R2D_VerifichePodIniziali_GAS.main(args);
		TimeUnit.SECONDS.sleep(5);	
		R2D_Forzatura_Esiti_3OK_SWA_GAS.main(args); 
		TimeUnit.SECONDS.sleep(5);	
		R2D_CaricamentoEsiti_4OK_SWA_GAS.main(args); 
		TimeUnit.SECONDS.sleep(5);	
		R2D_CaricamentoEsiti_5OK_SWA_GAS.main(args); 
		
		

		
	}
	
	@After
	public void fineTest() throws Exception {
		/*
		 * prop.load(new FileInputStream(nomeScenario));
		 * this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 * ReportUtility.reportToServer(this.prop);
		 */
	}
}
