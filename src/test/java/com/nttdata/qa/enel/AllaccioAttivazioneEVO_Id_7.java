package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneRes;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazioneSalvaguardia;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoIndirizzoEsecuzioneLavoriEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoAllaccioAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoAllaccioAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class AllaccioAttivazioneEVO_Id_7 {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "PE");
			prop.setProperty("TIPO_CLIENTE", "Residenziale");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("TIPO_OPERAZIONE", "ALLACCIO_EVO");
			prop.setProperty("MERCATO", "Salvaguardia");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("CITTA", "ROMA");
			prop.setProperty("INDIRIZZO", "VIA NIZZA");
			prop.setProperty("CIVICO", "4");
			prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
			prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "ACQUA");
			prop.setProperty("USO_ENERGIA", "Ordinaria");
			prop.setProperty("TENSIONE", "220");
			prop.setProperty("ASCENSORE", "NO");
			prop.setProperty("POTENZA", "3");
			prop.setProperty("DISALIMENTABILITA", "SI");
			prop.setProperty("TIPO_MISURATORE", "Non Orario");
			prop.setProperty("CONSUMO_ANNUO", "1000");
			prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
			prop.setProperty("PRODOTTO", "NEW_MS__B");
			prop.setProperty("TIPO_OI_ORDER", "Commodity");
			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
			prop.setProperty("CAP", "00135");
			prop.setProperty("RUN_LOCALLY","Y");
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			ProcessoAllaccioAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID14.main(args);
			SelezioneMercatoAllaccioAttivazioneEVO.main(args);
			InserimentoIndirizzoEsecuzioneLavoriEVO.main(args);
			VerificaCreazioneOffertaAllaccioAttivazione.main(args);
			VerificaRiepilogoOffertaAllaccioAttivazione.main(args);
			SelezioneUsoFornitura.main(args);
			CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneRes.main(args);
			ConfermaIndirizziAllaccioAttivazione.main(args);
			ConfermaFatturazioneElettronicaAllaccioAttivazione.main(args);
			PagamentoBollettinoPostaleEVO.main(args);
			ConfermaScontiBonusEVO.main(args);
			ConfiguraProdottoElettricoNonResidenziale.main(args);
			GestioneCVPAllaccioAttivazioneSalvaguardia.main(args);

		};
		
		@After
	    public void fineTest() throws Exception{
	        String args[] = {nomeScenario};
	        InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	  };
		
		
		
		
		
	

}
