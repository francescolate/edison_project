package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.AzioniRiepilogoOffertaVoltura;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CheckListText;
import com.nttdata.qa.enel.testqantt.CheckRiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPod;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneClienteUscenteAndInsertPodMessagErrorMercatoLiberoFui;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornitura;
import com.nttdata.qa.enel.testqantt.CheckSezioneSelezioneFornituraRicontrattualizzazione;
import com.nttdata.qa.enel.testqantt.ConfermaCheckList;
import com.nttdata.qa.enel.testqantt.ConfermaNessunSitoPresente;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID22;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;

import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoVolturaSenzaAccolloEVO2;
import com.nttdata.qa.enel.testqantt.SetPropertyNAMECFPOD;
import com.nttdata.qa.enel.testqantt.SetQueryRecuperaDatiRicontrattualizzazione122;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class RicontrattualizzazioneEVO_Id_30_PE_KO_Fornitura_non_Attivabile {

	Properties prop;
	String nomeScenario = this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("LINK",Costanti.salesforceLink);
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe_manager);
		prop.setProperty("PROCESSO", "Avvio Cambio Prodotto EVO");	
		prop.setProperty("ATTIVABILITA","Non Attivabile");
		prop.setProperty("RUN_LOCALLY","Y");
		prop.setProperty("RIGA_DA_ESTRARRE","5");
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));
		
		SetQueryRecuperaDatiRicontrattualizzazione122.main(args);
		RecuperaDatiWorkbench.main(args);
		SetPropertyNAMECFPOD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID22.main(args);
		ConfermaCheckList.main(args);
		CheckSezioneSelezioneFornituraRicontrattualizzazione.main(args);
		
	};
	
	@After
    public void fineTest() throws Exception{
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
		
	};
}
