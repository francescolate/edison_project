package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaCheckListAttivazionSDD;
import com.nttdata.qa.enel.testqantt.CreaNuovoSdd;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID12;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ProcessoGestioneSDD;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.AnnullaCaseCreazioneSdd;
import com.nttdata.qa.enel.testqantt.AttivazioneSDD_SetPropertiesPerVerificaCase;
import com.nttdata.qa.enel.testqantt.AttivazioneSDD_VerificaStatoCase;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezionaFornituraAttivazioneSDD;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetCfForGestioneSdd;
import com.nttdata.qa.enel.testqantt.VerificaAlertRosso;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamento;
import com.nttdata.qa.enel.testqantt.VerificaMetodoPagamentoRIDAsset;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

/**
 * 'Profilo: PE Cliente: Residenziale Commodity: ELE
 * 
 * Descrizione scenario Attivazione SDD ELE da canale PE, cliente Residenziale,
 * IBAN estero, controlli formali obbligatorietà campi e validità IBAN estero
 * 
 * Risultato atteso Corretta verifica controlli formali.
 * Viene mostrato in alto il messaggio di errore: Attenzione, trattasi di Iban Italiano, eliminare il flag iban estero (Area Sepa)
 */
public class Gestione_Attivazione_SSD_EVO_id12 {

	// RICHIESTA 215659983

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";
	Logger LOGGER = Logger.getLogger("");
	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("CODICE_FISCALE", "");
		prop.setProperty("POD", "");

		prop.setProperty("ESITO_COMPATIBILITA", "Attivabile");

		// IBAN
		prop.setProperty("ATT_SDD_IBAN", "IT97L0300203280125118261526");
		prop.setProperty("ATT_SDD_IBAN_ESTERO", "Y");
		prop.setProperty("ATT_SDD_BANCA", "FINECOBANK SPA");
		prop.setProperty("ATT_SDD_AGENZIA", "SEDE DI ROMA");
		prop.setProperty("ATT_SDD_STATO", "ITALIA");

		//gestione flusso creazione nuovo rid
		prop.setProperty("ATT_SDD_CLICK_SALVA_NUOVO_RID", "Y"); //Valorizzo a Y in quanto il test verifica messaggio errore su validazione rid
		prop.setProperty("ATT_SDD_CLICK_CONFERMA_NUOVO_RID", "NO"); //Valorizzo a N in quanto il test verifica messaggio errore su validazione rid
		prop.setProperty("ATT_SDD_VERIFICA_CREAZIONE_NUOVO_RID", "NO"); //Valorizzo a N in quanto il test verifica messaggio errore su validazione rid
		prop.setProperty("RED_ALERT_MESSAGE","Attenzione, trattasi di Iban Italiano, eliminare il flag iban estero (Area Sepa)" );
		prop.setProperty("CHECK_RED_ALERT_MESSAGE","Y");




		



		prop.setProperty("RUN_LOCALLY", "Y");

	};

	@Test
	public void eseguiTest() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		SetCfForGestioneSdd.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoGestioneSDD.main(args);
		Identificazione_Interlocutore_ID12.main(args);
		ConfermaCheckListAttivazionSDD.main(args);
		SelezionaFornituraAttivazioneSDD.main(args);
		CreaNuovoSdd.main(args);
		VerificaAlertRosso.main(args);
		LOGGER.info(nomeScenario+" concluso con successo");
	};

	@After
	public void fineTest() throws Exception {
		 String args[] = {nomeScenario};
		 InputStream in = new FileInputStream(nomeScenario);
		 prop.load(in);
		 this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 ReportUtility.reportToServer(this.prop);
	};

}
