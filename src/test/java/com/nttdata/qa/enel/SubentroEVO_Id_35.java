package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;


public class SubentroEVO_Id_35 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("MODULE_ENABLED", "Y");
		prop.setProperty("CODICE_FISCALE", "TMTRDN88T10F839M");
	    prop.setProperty("PROCESSO", "Avvio Subentro EVO");
	    prop.setProperty("COMMODITY", "ELE");
	    prop.setProperty("RUN_LOCALLY", "Y");
	    
	    prop.setProperty("TIPO_DOCUMENTO", "Patente");
	    prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
		
        prop.setProperty("SEZIONE_ISTAT", "N");
        prop.setProperty("CAP", "00198");      
        prop.setProperty("INDIRIZZO", "VIA NIZZA");
        prop.setProperty("CIVICO", "4");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("LOCALITA_ISTAT", "ROMA");
        
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "380");
		prop.setProperty("POTENZA_CONTRATTUALE", "4");
		prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		
		prop.setProperty("USO", "Uso Abitativo");
		
		prop.setProperty("RESIDENTE", "SI");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("TITOLARITA", "Proprietà o Usufrutto");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
		prop.setProperty("CONSUMO_ANNUO", "1250");
		
		return prop;
	}

	@Test
	public void eseguiTest() throws Exception {
	    //prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		prop.load(new FileInputStream(nomeScenario));

		//RecuperaPodMultiNonEsistenti.main(args);
		LoginSalesForcePE.main(args);/*
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args); // inserimento CF
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID15.main(args);
		SezioneMercatoSubentro.main(args);
		InserimentoFornitureSubentroMultiEle.main(args);
		SelezioneUsoFornitura.main(args); 
        CommodityMultiEleSubentro.main(args);
        IndirizzoDiResidenzaSubentro.main(args);
		*/
	}
	
	@After
	public void fineTest() throws Exception {
		/*
		 * prop.load(new FileInputStream(nomeScenario));
		 * this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		 * ReportUtility.reportToServer(this.prop);
		 */
	}

}
