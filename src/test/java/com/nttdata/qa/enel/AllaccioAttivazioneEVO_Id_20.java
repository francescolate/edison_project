package com.nttdata.qa.enel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.AnnullaOIOrdineAllaccio;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCupEVO;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneRes;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaEVONonResidenzialePA;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoIndirizzoEsecuzioneLavoriEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBonificoEVO;
import com.nttdata.qa.enel.testqantt.ProcessoAllaccioAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_senza_POD;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOICommodity;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoAllaccioAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVO;
import com.nttdata.qa.enel.testqantt.VerificaCaseInLavorazione_senza_POD;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOffertaAnnullata;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInLavorazione_senza_POD;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaSottostatoCaseOrderInviato;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineAnnullato;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineRichiestaPreventivo;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificheAnullamentoRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodAnnullato_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.SetAttributeR2DCodDTN02;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class AllaccioAttivazioneEVO_Id_20 {
		Properties prop;
		final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
			prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_2);
			prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_2);
			prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
			prop.setProperty("CODICE_FISCALE", "86001710044");
			prop.setProperty("COMMODITY", "ELE");
			prop.setProperty("TIPO_UTENZA", "PE");
			prop.setProperty("TIPO_CLIENTE", "PA");
			prop.setProperty("TIPO_DELEGA", "Nessuna delega");
			prop.setProperty("TIPO_OPERAZIONE", "ALLACCIO_EVO");
			prop.setProperty("MERCATO", "Libero");
			prop.setProperty("PROVINCIA", "ROMA");
			prop.setProperty("CITTA", "ROMA");
			prop.setProperty("INDIRIZZO", "VIA NIZZA");
			prop.setProperty("CIVICO", "4");
			prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
			prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "ACQUA");
			prop.setProperty("USO_ENERGIA", "Ordinaria");
			prop.setProperty("TENSIONE", "220");
			prop.setProperty("ASCENSORE", "NO");
			prop.setProperty("POTENZA", "3");
			prop.setProperty("DISALIMENTABILITA", "SI");
			prop.setProperty("TIPO_MISURATORE", "Non Orario");
			prop.setProperty("CONSUMO_ANNUO", "1000");
	        prop.setProperty("CODICE_UFFICIO", "B87H10");
	        prop.setProperty("TIPOLOGIA_PA", "Locale");
	        prop.setProperty("SPLIT_PAYMENT", "No");
	        prop.setProperty("FLAG_136", "SI");
	        prop.setProperty("CIG", "1111122222");
	        prop.setProperty("CUP", "111112222233333");
			prop.setProperty("CANALE_INVIO", "POSTA");
			prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
			prop.setProperty("CLASSIFICAZIONE_PREVENTIVO", "NON PREDETERMINABILE");
			prop.setProperty("MODALITA_INVIO_PREVENTIVO", "Posta");
			prop.setProperty("MODALITA_FIRMA_PREVENTIVO", "No Vocal");
			prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "SI");
			prop.setProperty("PRODOTTO", "Open Energy Special 3");
			prop.setProperty("SCELTA_ABBONAMENTI", "BASE");
			prop.setProperty("TIPO_OI_ORDER", "Commodity");
			prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
			prop.setProperty("CAP", "00135");
			prop.setProperty("RUN_LOCALLY","Y");
			
			
			//Integrazione R2D ELE
			prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
			prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d);
			prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d);
			
			
		
		};
		
		@Test
        public void eseguiTest() throws Exception{

			
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			CercaClientePerNuovaInterazioneEVO.main(args);
			ProcessoAllaccioAttivazioneResidenzialeEVO.main(args);
			Identificazione_Interlocutore_ID14.main(args);
			SelezioneMercatoAllaccioAttivazioneEVO.main(args);
			InserimentoIndirizzoEsecuzioneLavoriEVO.main(args);
			VerificaCreazioneOffertaAllaccioAttivazione.main(args);
			VerificaRiepilogoOffertaAllaccioAttivazione.main(args);
			SelezioneUsoFornitura.main(args);
			CompilaDatiFornituraAllaccioAttivazioneUsoDiversoAbitazioneRes.main(args);
			ConfermaIndirizziAllaccioAttivazione.main(args);
			ConfermaFatturazioneElettronicaEVONonResidenzialePA.main(args);
			SplitPaymentEVO.main(args);
			CigCupEVO.main(args);
			PagamentoBonificoEVO.main(args);
			ConfermaScontiBonusEVO.main(args);
			ConfiguraProdottoElettricoNonResidenziale.main(args);
			GestioneCVPAllaccioAttivazione.main(args);
			ConsensiEContattiAllaccioAttivazione.main(args);
			ModalitaFirmaAllaccioAttivazione.main(args);
			ConfermaOffertaAllaccioAttivazione.main(args);
			ConfermaPreventivoAllaccioAttivazioneNonPredeterminabile.main(args);
			
			//Controlli post emissione richiesta
			RecuperaStatusOffer.main(args);
			VerificaOffertaInLavorazione_senza_POD.main(args);
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaCaseInLavorazione_senza_POD.main(args);
			VerificaStatusR2DPresaInCarico.main(args);
			VerificaStatusOrdineRichiestaPreventivo.main(args);
			SalvaIdOrdine.main(args);
			
			//Integrazione con R2D
			SetAttributeR2DCodDTN02.main(args);
			LoginR2D_ELE.main(args);
			R2D_VerifichePodIniziali_ELE.main(args);
			R2D_InvioPSPortale_1OK_ELE.main(args);
			R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
			
			//Accesso a SFDC per annullare la richiesta
			LoginSalesForce.main(args);
			SbloccaTab.main(args);
			RicercaOICommodity.main(args);
			AnnullaOIOrdineAllaccio.main(args);
			
			//Aspettare 5 minuti
			
			//Accedere a R2D per verificare che vada in stato annullata
			LoginR2D_ELE.main(args);
			R2D_VerifichePodAnnullato_ELE.main(args);
			
			//Verifica stato offerta SFDC
			RecuperaStatusOffer.main(args);
			VerificaOffertaInLavorazione_senza_POD.main(args);
			
			//Verifica stato OI SFDC
			RecuperaStatusCase_and_Order_senza_POD.main(args);
			VerificaCaseInLavorazione_senza_POD.main(args);
			VerificaSottostatoCaseOrderInviato.main(args);


		};
		
		@After
		public void fineTest() throws Exception {
			String args[] = {nomeScenario};
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
			ReportUtility.reportToServer(this.prop);
		};
		
		
		
		
		
	

}
