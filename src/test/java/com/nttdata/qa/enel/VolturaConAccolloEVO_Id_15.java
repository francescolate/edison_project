package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import com.nttdata.qa.enel.testqantt.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.Utility;
/**
Utente: s2s_manager_cfi_l@crmtenelfreemarket.com.uat
Cliente: Residenziale

Descrizione: Accedere alla piattaforma SFDC ed avviare un processo di Voltura Con Accollo EVO su fornitura GAS da canale S2S su cliente di tipo Residenziale
Fornitura "Uso Abitativo"
Verificare che il campo USO FORNITURA sia valorizzato con la descrizione "Uso Abitativo" e il campo "Categoria di Consumo" contenga nella lista le sole voci predefinite.
Risultato Atteso: Il Campo uso fornitura contiene la descrizione "Uso Abitativo" e la lista associata al campo "Categoria di Consumo" contiene le voci predefinite.

Posizionarsi sul campo Categoria di Consumo e verificare che sia presenti nella lista le seguenti diciture:

1. Riscald.to appartamento < 100 mq
2. Riscald.to appartamento > 100 mq
3. Riscald.to appartamento < 60 mq
4. Riscald.toO villa/casa indipend
5. Uso cucina
6. Uso cucina prod.ne acqua calda
 */
public class VolturaConAccolloEVO_Id_15 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";
	
	@Before
	public void setUp() throws Exception {
	      this.prop = conf();
		}
	
	public static Properties conf() {
		Properties prop = new Properties();
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LINK",Costanti.salesforceLink);
		prop.setProperty("CODICE_FISCALE", "RSSPLA61L28E522S");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("CAUSALE", "Mortis Causa");
		prop.setProperty("PROCESSO", "Avvio Voltura con accollo EVO");
		prop.setProperty("ATTIVABILITA","Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("USO", "Uso Abitativo");
		prop.setProperty("RESIDENTE", "SI");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CATEGORIA","ABITAZIONI PRIVATE");
		prop.setProperty("SCEGLI_METODO_PAGAMENTO","Y");
		prop.setProperty("CATEGORIA_MERCEOLOGICA_SAP", "Abitazioni private");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("FLAG_RESIDENTE","SI");
		prop.setProperty("INDIRIZZO", "VIA");
		prop.setProperty("CIVICO", "1");
		prop.setProperty("PROVINCIA_COMUNE", "ROMA");
		prop.setProperty("CATEGORIA_DI_CONSUMO", "Riscald.to appartamento <100mq");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CHECK_LIST_TEXT","ATTENZIONE PRIMA DI PROCEDERE VERIFICARE:Che il cliente disponga dei dati del cliente uscente (CF, PI, Utenza) e dei dati del sito (POD/PDR)Se il cliente è in Salvaguardia è necessaria l’Istanza di SalvaguardiaINOLTRE VERIFICARE:PER USO ABITATIVO-    Se il cliente vuole attivare il RID/SDD è necessario l’IBANPER USI DIVERSI DALL’ABITAZIONE-    Se il cliente vuole attivare il RID/SDD è necessario l’IBAN-    Se il cliente è una Pubb. Amm. è necessario il Codice Ufficio-    Se il cliente è soggetto a Legge 136 è necessario almeno uno tra CIG e CUP-    Se il cliente è soggetto a Split Payment è necessario indicare Data Inizio e Data Fine validità-    In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)INFORMAZIONI UTILI:In caso di Dual ricordati che le forniture dovranno avere stessi: USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO In caso di Multi ricordati che le forniture dovranno avere stessi: COMMODITY – USO – INDIRIZZO DI FATTURAZIONE – MODALITÀ DI PAGAMENTO – PRODOTTO - CODICE UFFICIO (se previsto)Nel caso in cui la richiesta sia retrodatata, non sarà possibile scegliere il prodotto Scegli Tu Ore Free in quanto non previsto.  Fibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento della voltura. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta    In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente: -   numero di cellulare  -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associataSCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"");
		prop.setProperty("RIGA_DA_ESTRARRE", "4");
		prop.setProperty("QUERY", Costanti.recupera_CF_POD_per_Volture_id15);
		prop.setProperty("RUN_LOCALLY","Y");
		return prop;
	};
	
	@Test
    public void eseguiTest() throws Exception{

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));

		//SetVolturaConAccolloEVOrecupera_CF_POD_per_Volture26.main(args); //la query viene settata direttamente nelle property
		RecuperaDatiWorkbench.main(args);
		SetPropertyVolturaConAccolloEVONOMECFPOD.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID22.main(args);
		SelezioneMercatoVolturaConAccollo.main(args);
		CheckSezioneClienteUscenteAndInsertPodVolturaConAccollo.main(args);
		CheckSezioneSelezioneFornitura.main(args);
		CreaOffertaVerificaCampiVolturaConAccolloRes.main(args);
		VerificaRiepilogoOffertaVolturaConAccollo.main(args);
		CheckDatiFornituraVolturaConAccolloUsoAbitativoResGas.main(args);
	};
	
	@After
    public void fineTest() throws Exception{
		prop.load(new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		Utility.takeSnapShotOnKo(prop, nomeScenario);
		ReportUtility.reportToServer(this.prop);
	};
}
