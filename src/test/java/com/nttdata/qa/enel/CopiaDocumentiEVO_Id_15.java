package com.nttdata.qa.enel;


import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class CopiaDocumentiEVO_Id_15 {

	Properties prop;
	final String nomeScenario =this.getClass().getSimpleName()+".properties";
	//0000003041920612
	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", Costanti.salesforceLink);
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "TSSGPP23E04F843Z");
		prop.setProperty("PROCESSO", "Avvio Copia Documenti EVO");
		prop.setProperty("NOME_UTENTE", "630415047");
		//prop.setProperty("NUMERO_FATTURA", "0000003041920612");
	    prop.setProperty("NUMERO_FATTURA", "0000001910883855");
		prop.setProperty("TIPOLOGIA_DOC", "FATTURE");
		prop.setProperty("TIPOLOGIA_DMS", "Tutte");
		prop.setProperty("ID_DOCUMENTO", "a3W1l000001CXNpEAO");	
		prop.setProperty("CANALE_INVIO", "EMAIL");
		prop.setProperty("INDIRIZZO_EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("MODALITA_DUPLICATO","Invio dettaglio fattura");
		prop.setProperty("RUN_LOCALLY", "Y");
	}

	@Test
	public void eseguiTest() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load( new FileInputStream(nomeScenario));
/*
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID21.main(args);
		VerificaNumeroRichiestaCaseCopiaDocumento.main(args);
		VerificaSelezioneFornitureCopiaDocumenti.main(args);
		SelezionaSezioneTipologiaDocumenti.main(args);
		VerificaSelezioneFattureCopiaDocumenti.main(args);
		AzioniDettagliSpedizioneCopiaDocumenti.main(args);
		SalvaNumeroDocumento.main(args);		
		RecuperaStatusModelloPlicoCopiaDocumenti.main(args);
		VerificaCasePlicoCopiaDocumenti.main(args);
		RecuperaLinkItemCaseCopiaDocumenti.main(args);

		//verifica presenza item con Modello = Oggetto Email Generic
		VerificaModelloOggettoEmailGenericoCopiaDocumentiWorkbench.main(args);
		//verifica presenza item con Modello = Corpo Email Generic
		VerificaModelloCorpoEmailGenericoCopiaDocumentiWorkbench.main(args);*/
	}

	@After
	public void fineTest() throws Exception {
        prop.load(new FileInputStream(nomeScenario));
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
	}
}
