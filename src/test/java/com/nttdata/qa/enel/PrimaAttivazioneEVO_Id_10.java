package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CaricaVerificaValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.GestioneReferentePrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID16;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoRIDEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SetPropertyPodEspletatoPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusUDBCreazioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificheOrdinePostQuery;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_10 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_pe_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_pe_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("CODICE_FISCALE", "02567670845");
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("TIPO_CLIENTE", "Impresa Individuale");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "00100");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
		prop.setProperty("CATEGORIA_MERCEOLOGICA", "ACQUA");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "Y");
		prop.setProperty("CANALE_INVIO_FATTURAZIONE", "SDI");
		prop.setProperty("CODICE_UFFICIO", "0000000");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("IBAN", "IT69W0306909400100000008461");
		prop.setProperty("SPLIT_PAYMENT", "No");
		prop.setProperty("FLAG_136", "NO");
		//carrello
        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "CORPORATE SUPER");
        prop.setProperty("ELIMINA_VAS", "Y");
		
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO", "N");
		prop.setProperty("SKIP_CONTATTI_CONSENSI", "Y");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CANALE_INVIO", "STAMPA LOCALE");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("DOCUMENTI_DA_VALIDARE",
				"Addebito diretto su conto corrente;C.T.E.;Documento di riconoscimento;ML_MdaSingleBus_No_Vocal_Istanza326_Nazionale");
		prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "ORDER - INVIATO");
		prop.setProperty("STATO_RICHIESTA", "IN ATTESA");
		prop.setProperty("STATO_R2D", "PRESO IN CARICO");
		prop.setProperty("STATO_SAP", "BOZZA");
		prop.setProperty("STATO_SEMPRE", "BOZZA");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("LINK_UDB", "http://dtcmmind-bw-01.risorse.enel:8887/");
		prop.setProperty("ESITO_UDB", "OK");
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("RECUPERA_OI_RICHIESTA","Y");
		prop.setProperty("RUN_LOCALLY", "Y");
		
		// Dati R2d
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		// Dati Verifiche pod iniziali
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("DISTRIBUTORE_R2D_ATTESO_ELE", "Areti");
		prop.setProperty("STATO_R2D", "AW");
		// Dati 1OK
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Prima Attivazione");
		prop.setProperty("TIPO_CONTATORE", "CE - Contatore Elettronico Non Orario");
		prop.setProperty("POTENZA_FRANCHIGIA", "220");
		// Dati 3OK
		prop.setProperty("INDICE_POD", "1");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Ammissibilità A01");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito A01");

	};

	@Test
	public void eseguiTest() throws Exception {
		
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		
		RecuperaPodNonEsistente.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		ProcessoPrimaAttivazioneNonResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID16.main(args);
		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEVO.main(args);
		CompilaDatiFornituraElePrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		GestioneReferentePrimaAttivazioneEVO.main(args);
		CommodityEleNonResidenzialePrimaAttivazione.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoRIDEVO.main(args);
		SplitPaymentSwaEVO.main(args);
		CigCugpSwaEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoElettricoNonResidenziale.main(args);
		GestioneCVPPrimaAttivazione.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);
		ModalitaFirmaPrimaAttivazione.main(args);
		ConfermaPrimaAttivazione.main(args);
		CaricaVerificaValidaDocumenti.main(args);
		VerificheRichiestaDaPod.main(args);
		RecuperaOrderIDDaPod.main(args);

		SetOIRicercaDaOIOrdine.main(args);

		LoginR2D_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_VerifichePodIniziali_ELE.main(args);
		TimeUnit.SECONDS.sleep(5);
		R2D_InvioPSPortale_1OK_ELE.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
		R2D_CaricamentoEsiti_5OK_ELE.main(args);
		R2D_VerifichePodFinali_ELE.main(args);

		// Da eseguire dopo qualche ora
		
		
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusUDBCreazioneOrdine.main(args);
		InvioEsitoUBD.main(args);
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusUDBAttivazPod.main(args);
		SetPropertyPodEspletatoPrimaAttivazione.main(args);
		VerificheOrdinePostQuery.main(args);

	};

	@After
    public void fineTest() throws Exception{
        String args[] = {nomeScenario};
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
  };

}
