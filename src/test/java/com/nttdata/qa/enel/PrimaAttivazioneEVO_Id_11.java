package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiDelibera40;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazione;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCugpSwaEVO;
import com.nttdata.qa.enel.testqantt.CommodityGasNonResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraGasPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOGasNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneAppuntamentoPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.GestioneReferentePrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID14;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBonificoEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneNonResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOffertaDaTab;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.SplitPaymentSwaEVO;
import com.nttdata.qa.enel.testqantt.VerificaCreazioneOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaOIOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInBozza;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Accertamento_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsitiDelibera40Istruttoria_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_GAS;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIntermedie_GAS;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class PrimaAttivazioneEVO_Id_11 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("COMMODITY", "GAS4");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("TIPO_CLIENTE", "Non Residenziale");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "20121");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("MATRICOLA_CONTATORE", "Y");
		prop.setProperty("USO_FORNITURA", "Uso Diverso da Abitazione");
		prop.setProperty("CATEGORIA_CONSUMO", "Alberghi");
		prop.setProperty("CATEGORIA_USO", "Uso condizionamento");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_SMS", "3394675542");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3394675542");
		prop.setProperty("ORDINE_FITTIZIO", "NO");
		prop.setProperty("CLASSE_PRELIEVO", "7 giorni");
		prop.setProperty("CATEGORIA_MARKETING", "ALBERGHI");
		prop.setProperty("ORDINE_GRANDEZZA", "STANZE > 100 NR.");
		prop.setProperty("PROFILO_CONSUMO", "PRODUZIONE");
		prop.setProperty("PUBBLICA_AMMINISTRAZIONE","Locale");
		prop.setProperty("CUP","111111111111111");
		prop.setProperty("POPOLA_DATE_SPLIT_PAYMENT","Y");
		prop.setProperty("SPLIT_PAYMENT","Si");
		prop.setProperty("PRODOTTO", "New_Soluzione Gas Impresa Business");
		prop.setProperty("OPZIONE_KAM_AGCOR", "CORPORATE SUPER");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("ESECUZIONE_ANTICIPATA","SKIP");
		prop.setProperty("CANALE_INVIO", "POSTA");
		prop.setProperty("MODALITA_FIRMA", "VOCAL ORDER");
		prop.setProperty("REGISTRAZIONE_VOCAL","N");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");
		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("CODICE_FISCALE", "00813700143");		
		prop.setProperty("TIPO_OI_ORDER", "Commodity");

		// Dati R2d
		prop.setProperty("RECUPERA_ELEMENTI_ORDINE","Y");
		prop.setProperty("LINK_R2D_GAS","http://r2d-coll.awselb.enelint.global/r2d/gas/217.do");
		prop.setProperty("USERNAME_R2D_GAS",Costanti.utenza_r2d);
		prop.setProperty("PASSWORD_R2D_GAS",Costanti.password_r2d);
		prop.setProperty("TIPO_LAVORAZIONE_CRM_GAS","Attivazione in delibera 40");
		prop.setProperty("EVENTO_3OK_GAS","Esito Ammissibilità");
		prop.setProperty("EVENTO_5OK_GAS","Esito Richiesta");
		prop.setProperty("CP_GESTORE", "12345");
		prop.setProperty("ESITO_DELIBERA40_ISTRUTTORIA", "Istruttoria Positiva");
		prop.setProperty("ESITO_DELIBERA40_ACCERTAMENTO", "Accertamento Positivo");
		prop.setProperty("LETTURA_CONTATORE", "112233");
		prop.setProperty("ANNO_COSTRUZIONE_CONTATORE", "2019");
		prop.setProperty("ACCESSIBILITA_229", "1 (Accessibile)");
		prop.setProperty("COEFFICIENTE_C", "1");
		prop.setProperty("CLASSE_CONTATORE", "G0004");

	};

	@Test
	public void eseguiTest() throws Exception {
		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

		RecuperaPodNonEsistente.main(args);
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		//CercaClientePerNuovaInterazioneEVO.main(args);
		CercaClientePerNuovaInterazione.main(args);
		ProcessoPrimaAttivazioneNonResidenzialeEVO.main(args);
		Identificazione_Interlocutore_ID14.main(args);
		SelezioneMercatoPrimaAttivazioneEVO.main(args);
		InserimentoFornitureEVO.main(args);
		CompilaDatiFornituraGasPrimaAttivazione.main(args);
		VerificaCreazioneOffertaPrimaAttivazione.main(args);
		VerificaRiepilogoOffertaPrimaAttivazione.main(args);
		GestioneReferentePrimaAttivazioneEVO.main(args);
		CommodityGasNonResidenzialePrimaAttivazione.main(args);
		ConfermaIndirizziPrimaAttivazione.main(args);
		GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
		PagamentoBonificoEVO.main(args);
		SplitPaymentSwaEVO.main(args);
		CigCugpSwaEVO.main(args);
		ConfermaScontiBonusSWAEVO.main(args);
		ConfiguraProdottoSWAEVOGasNonResidenziale.main(args);
		GestioneCVPPrimaAttivazione.main(args);
		ConsensiEContattiPrimaAttivazione.main(args);
		ModalitaFirmaPrimaAttivazione.main(args);
		ConfermaPrimaAttivazione.main(args);

		//Recupera Offerta
		RecuperaStatusOffer.main(args);
		VerificaOffertaInBozza.main(args);

		RecuperaStatusCase_and_Offer.main(args);
		VerificaOIOffertaInBozza.main(args);
		SalvaIdOfferta.main(args);	

		RecuperaStatusCase_and_Order.main(args);
		SalvaIdOrdine.main(args);

		//set chiave ricerca per R2D
		SetOIRicercaDaOIOrdine.main(args);

		//Esiti R2D delibera 40
		LoginR2D_GAS.main(args);
		R2D_VerifichePodIniziali_GAS.main(args);
		R2D_InvioPSPortale_1OK_GAS.main(args);
		R2D_CaricamentoEsiti_3OK_Standard_GAS.main(args);
		R2D_CaricamentoEsitiDelibera40Istruttoria_GAS.main(args);
		R2D_CaricamentoEsitiDelibera40Accertamento_GAS.main(args);
		R2D_VerifichePodIntermedie_GAS.main(args);


		//Accesso a SFDC per caricare file
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		RicercaOffertaDaTab.main(args);
		CaricaEValidaDocumentiDelibera40.main(args);
		GestioneAppuntamentoPrimaAttivazione.main(args);
	
		//Verifica offerta in stato chiusa
		RecuperaStatusOffer.main(args);
		VerificaOffertaInChiusa.main(args);
		
		//SECONDA INTEGRAZIONE CON R2D - ESITO 5OK
		LoginR2D_GAS.main(args);
		R2D_CaricamentoEsiti_5OK_GAS.main(args);
		R2D_VerifichePodFinali_GAS.main(args);

		//Verifica ricezione esiti
		RecuperaStatusCase_and_Order.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);

		//Verifica chiusura richiesta
		VerificheAttivazioneRichiesta.main(args);

	};

	@After
	public void fineTest() throws Exception {
		String args[] = {nomeScenario};
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);
	};

}
