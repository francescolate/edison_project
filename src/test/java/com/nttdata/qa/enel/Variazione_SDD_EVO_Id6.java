package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.*;
import com.nttdata.qa.enel.udb.InvioEsitoUBD;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.IBAN;
import com.nttdata.qa.enel.util.ReportUtility;

import org.eclipse.persistence.internal.libraries.asm.util.CheckFieldAdapter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Variazione_SDD_EVO_Id6 {

	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName() + ".properties";

	@Before
	public void inizioTest() throws Exception {
		this.prop = new Properties();
		prop.setProperty("LINK", "https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME", Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD", Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LANDING_PAGE_TITLE", "Lightning Experience");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("RIGA_DA_ESTRARRE", "1");

		prop.setProperty("COMMODITY", "GAS");
		prop.setProperty("CODICE_FISCALE", "");
		prop.setProperty("TIPOLOGIA_CLIENTE", "BUSINESS");
		prop.setProperty("TIPO_OPERAZIONE", "VARIAZIONE_SDD");
		prop.setProperty("POD", "");

		prop.setProperty("ESITO_COMPATIBILITA", "Attivabile");

		// IBAN
		prop.setProperty("ATT_SDD_IBAN", "FR9510096000404937285628E26");
		prop.setProperty("ATT_SDD_IBAN_ESTERO", "Y");
		prop.setProperty("ATT_SDD_BANCA", "BANCA ESTERA");
		prop.setProperty("ATT_SDD_AGENZIA", "BANCA ESTERA");
		prop.setProperty("ATT_SDD_STATO", "ITALIA");
		prop.setProperty("ATT_SDD_SALVA_NUOVO_RID", "Y");
		prop.setProperty("ATT_SDD_VERIFICA_DEPOSITO_CAUZUIONALE", "N");

		// MODALITA FIRMA
		prop.setProperty("ATT_SDD_MOD_FIRMA", "DOCUMENTI VALIDI");
		prop.setProperty("ATT_SDD_CANALE_FIRMA", "STAMPA LOCALE");

		prop.setProperty("ATT_SDD_STATO_ATTESO_CASE", "CHIUSO");
		prop.setProperty("ATT_SDD_SOTTOSTATO_ATTESO_CASE", "RICEVUTO");


		prop.setProperty("RUN_LOCALLY", "Y");
		prop.setProperty("MODULE_ENABLED", "Y");

	};

	@Test
	public void eseguiTest() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));
        
//		SetCfForGestioneSdd.main(args);
//
//		LoginSalesForce.main(args);
//		SbloccaTab.main(args);
//		CercaClientePerNuovaInterazioneEVO.main(args);
//		ProcessoVariazioneSDD.main(args);
//		Identificazione_Interlocutore_ID12.main(args);
//		ConfermaCheckListAttivazionSDD.main(args);
//		SelezionaFornituraVariazioneSDD.main(args);
//		CreaNuovoSdd.main(args);
		ModalitaFirmaGestioneSdd.main(args);
		VerificaAttivitaContoEstero.main(args);
		AttivazioneSDD_VerificaStatoCase_ContoEstero.main(args);

  
    }

 //   @After
//    public void fineTest() throws Exception {
//        prop.load(new FileInputStream(nomeScenario));
//        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
//        ReportUtility.reportToServer(this.prop);
//    }
}
