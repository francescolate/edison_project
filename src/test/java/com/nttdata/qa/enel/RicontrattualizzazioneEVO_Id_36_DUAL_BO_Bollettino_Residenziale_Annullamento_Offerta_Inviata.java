package com.nttdata.qa.enel;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class
RicontrattualizzazioneEVO_Id_36_DUAL_BO_Bollettino_Residenziale_Annullamento_Offerta_Inviata {

    Properties prop;
    String nomeScenario = this.getClass().getSimpleName() + ".properties";

    @Before
    public void setUp() throws Exception {
        this.prop = conf();
    }

    public static Properties conf() {
        Properties prop = new Properties();
        prop.setProperty("LINK", Costanti.salesforceLink);
    	prop.setProperty("USERNAME",Costanti.utenza_forzatura_salesforce);
		prop.setProperty("PASSWORD",Costanti.password_forzatura_salesforce);
        prop.setProperty("PROCESSO", "Avvio Cambio Prodotto EVO");
        prop.setProperty("ATTIVABILITA", "Attivabile");
        prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
        //prop.setProperty("PRODOTTO_ELE", "Valore Luce Plus");
        prop.setProperty("PRODOTTO_ELE", "GiustaXte");
        prop.setProperty("PRODOTTO_GAS", "SoloXTe Gas");
        prop.setProperty("CAP", "00184");
        prop.setProperty("CITTA", "ROMA");
        prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
        prop.setProperty("CANALE_INVIO", "EMAIL");
        prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
        prop.setProperty("COMMODITY", "DUAL");
        prop.setProperty("TIPO_OI_ORDER", "Commodity");
        prop.setProperty("ESECUZIONE_ANTICIPATA", "SKIP");
        prop.setProperty("RUN_LOCALLY", "Y");
        //prop.setProperty("TAB_SGEGLI_TU_ELE", "SI");
        //prop.setProperty("PIANO_TARIFFARIO_ELE", "Senza Orari");
        //prop.setProperty("PRODOTTO_TAB_ELE", "Scegli Tu");
        //prop.setProperty("VAS_EX_GAS", "SI");
        prop.setProperty("MOTIVO_ANNULLAMENTO", "QC Rinuncia Cliente");
        return prop;
    }


    @Test
    public void eseguiTest() throws Exception {

        prop.store(new FileOutputStream(nomeScenario), null);
        String args[] = {nomeScenario};
        prop.load(new FileInputStream(nomeScenario));

        SetQueryRecuperaDatiRicontrattualizzazione119_3.main(args);
        RecuperaDatiRicontrattualizzazioneDUAL.main(args);
        LoginSalesForce.main(args);
        SbloccaTab.main(args);
        CercaClientePerNuovaInterazioneEVO.main(args);
        AvvioProcesso.main(args);
        Identificazione_Interlocutore_ID22.main(args);
        ConfermaCheckList.main(args);
        CheckSezioneSelezioneFornituraRicontrattualizzazioneDUAL.main(args);
        ConfermaRiepilogoOfferta.main(args);
        PagamentoBollettinoPostaleVolture.main(args);
        ConfermaScontiBonusEVO.main(args);
        ConfiguraProdottoSWAEVODUALResidenziale.main(args);
        AssociazioneProdottiServizi.main(args);
        ConfermaFatturazioneElettronicaSWAEVO.main(args);
        IndirizziFatturazione.main(args);
        ConsensiEContatti.main(args);
        ModalitaFirmaECanaleInvioRicontrattualizzazione.main(args);/*
        //RegistrazioneVocaleDigital.main(args);      
        ConfermaOffertaAllaccioAttivazione.main(args);
        AnnullaOfferta.main(args);
        
        //attendere 5 minuti
        Thread.sleep(300000);
      	RecuperaStatusOffer.main(args);
      	VerificaOffertaAnnullata.main(args);*/
       
    }

    ;

    @After
    public void fineTest() throws Exception {
    	prop.load( new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);

    }

    ;
}
