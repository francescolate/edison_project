package com.nttdata.qa.enel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import com.nttdata.qa.enel.testqantt.*;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSAcquirenteUnico_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;

public class VolturaConAccolloEVO_Id_33 {

	Properties prop;
	final String nomeScenario= this.getClass().getSimpleName()+".properties";

	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}

	public static Properties conf() throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_s2s_manager);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_s2s_manager);
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("CODICE_FISCALE", "52490720977");
		prop.setProperty("CODICE_FISCALE_REFERENTE", "BSNTTN88T10F839T");
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("CAUSALE", "Trasformazione/Fusione Azienda");
		prop.setProperty("PROCESSO", "Avvio Voltura con accollo EVO");
		prop.setProperty("CL_USCENTE_DENOMINAZIONE","0");
		prop.setProperty("CAP","00198");
		prop.setProperty("ATTIVABILITA","Non Attivabile");
		prop.setProperty("CONFERMA_SELEZIONE_FORNITURA", "Y");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CATEGORIA","ALTRI SERVIZI");
		prop.setProperty("SCEGLI_METODO_PAGAMENTO","Y");
		prop.setProperty("CANALE_INVIO","SDI");
		prop.setProperty("POPOLA_SPLIT_PAYMENT","NO");
		prop.setProperty("MODALITA_FIRMA", "NO VOCAL");
		//    prop.setProperty("CANALE_INVIO", "EMAIL");
		prop.setProperty("EMAIL", "d.calabresi@reply.it");
		prop.setProperty("EFFETTUARE_CONFERMA", "Y");            

		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("COMMODITY", "ELE");		
		prop.setProperty("RIGA_DA_ESTRARRE", "5");
		prop.setProperty("RUN_LOCALLY","Y");

		// DATA PREP
		prop.setProperty("CODICE_FISCALE_CL_USCENTE","00086480118");
		prop.setProperty("POD","05261100220044");

		return prop;

	};

	@Test
	public void eseguiTest() throws Exception{

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};
		prop.load(new FileInputStream(nomeScenario));

		//		SetVolturaConAccolloEVOrecupera_CF_POD_per_VoltureConAccolloid33.main(args);
		//		RecuperaDatiWorkbench.main(args);
		//		SetPropertyVolturaConAccolloEVONOMECFPOD.main(args);

		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CercaClientePerNuovaInterazioneEVO.main(args);
		AvvioProcesso.main(args);
		Identificazione_Interlocutore_ID12.main(args);
		SelezioneMercatoVolturaConAccolloBSN.main(args);
		CheckSezioneClienteUscenteAndInsertPodVolturaConAccollo.main(args);

		CheckSezioneSelezioneFornituraNonResidenzialeVCAMessaggioErroreFornituraNonAttivabile.main(args);


	};

	@After
	public void fineTest() throws Exception{
		prop.load(new FileInputStream(nomeScenario));
		this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
		ReportUtility.reportToServer(this.prop);	
	};
}
