package com.nttdata.qa.datapreparation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.components.colla.SetUtenzaS2SAttivazioni;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleResidenzialePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CompilaDatiFornituraElePrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.GestioneCVPPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.GestioneFatturazioneElettronicaPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID17;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureEVO;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.ProcessoPrimaAttivazioneResidenzialeEVO;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneMercatoPrimaAttivazioneEVO;
import com.nttdata.qa.enel.testqantt.SetOIRicercaDaOIOrdine;
import com.nttdata.qa.enel.testqantt.VerificaOffertaInChiusa;
import com.nttdata.qa.enel.testqantt.VerificaRiepilogoOffertaPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DPresaInCarico;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.SeleniumUtilities;
import com.nttdata.qa.enel.util.WindowsProcessKiller;

public class DP_13_Launcher implements Runnable {
	//elencare le dp da eseguire -  MAX 3 run Paralleli

	//static String[] dp=new String[] {"PA_13_6","PA_13_7","PA_13_8"}; /TUTTI OK
	//static String[] dp=new String[] {"PA_13_10","PA_13_11","PA_13_12"}; //TUTTI KO SAP
	
	//static String[] dp=new String[] {"PA_13_16","PA_13_17"}; //16 OK 17 OK
	//static String[] dp=new String[] {"PA_13_18","PA_13_19"}; //18 OK - 19 OK
	static String[] dp=new String[] {"PA_13_13","PA_13_14","PA_13_15"}; // 15 - KO SAP - 13 e 14 attesa SEMPRE
	//static String[] dp=new String[] {"PA_13_20","PA_13_21"}; //20 in OK - "21  attesa SAP e SEMPRE
	
	boolean restart=false;
    
    static Map<String,String> results=new HashMap<String,String>();

	final String MODULO_DA_ESEGUIRE="INDICE_MODULO_DA_ESEGUIRE";

	//Variabili d'istanza
	private Properties prop;
	private String nomeScenario="";
	private boolean testOk=true;


	public static   synchronized void UpdateResults(String nomeScenario,String result)
	{
		results.put(nomeScenario,result);
	}


	public  static void  main(String[] args) throws Exception
	{
		WindowsProcessKiller.killChrome();
		Thread t=null;
		ExecutorService executor = Executors.newFixedThreadPool(dp.length);
		String nomeScenario="";

		for(int i=0; i<dp.length;i++)
		{
			nomeScenario=String.format(dp[i]+".properties");
			DP_13_Launcher pa=new DP_13_Launcher();
			pa.nomeScenario=nomeScenario;

			t=new Thread(pa,pa.nomeScenario);
			

			executor.submit(pa);
			System.out.println("Start thread"+i);
			Thread.sleep(60*1000);

		}

		executor.shutdown();
		while (!executor.isTerminated()) {}
		print("Tutti i thread completati");
		
		
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		
		 String fileName ="Dp13_Execution.properties";
		 Properties resultsProp=new Properties();
		 if(new File(fileName).exists())
			 resultsProp.load(new FileInputStream(fileName));
		
		
		for(Entry<String, String>  k :results.entrySet())
		{
			System.out.println(k.getKey()+": "+k.getValue());
			resultsProp.setProperty(k.getKey(), k.getValue());
		}
		resultsProp.store(new FileOutputStream(fileName), null);
		
		
			 
		 dateFormat.format(new Date());


	}

	public static  void print(String msg)
	{
		msg=String.format(">>>>> %s *** %s", Thread.currentThread().getName(),msg);
		System.err.println(msg);
	}

	@Override
	public void run() {
		try {
			inizioTest();
			eseguiTest();
			print("Test concluso");
		} catch (Exception e) {
			
		}

	}

	
	 

	public void inizioTest() throws Exception {
		this.prop = new Properties();
		//Property creazione cliente

		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "AUTOMATION");
		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO", "F");
		prop.setProperty("DATA_NASCITA", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA", "Napoli");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("DIPENDENTE_ENEL","N");

		//Property PA
		prop.setProperty("COMMODITY", "ELE");
		prop.setProperty("TIPO_UTENZA", "S2S");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("TIPO_CLIENTE", "Casa");
		prop.setProperty("MERCATO", "Libero");
		prop.setProperty("FLAG_CLIENTE_PRODUTTORE", "N");
		prop.setProperty("TIPOLOGIA_PRODUTTORE", "");
		prop.setProperty("CAP", "00100");
		prop.setProperty("CAP_FORZATURA", "00135");
		prop.setProperty("CITTA_FORZATURA", "ROMA");
		prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
		prop.setProperty("ESITO_OFFERTABILITA", "OK");
		prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
		prop.setProperty("TIPO_MISURATORE", "Non Orario");
		prop.setProperty("TENSIONE_CONSEGNA", "220");
		prop.setProperty("POTENZA_CONTRATTUALE", "2");
		prop.setProperty("USO_FORNITURA", "Uso Abitativo");
		prop.setProperty("RESIDENTE", "NO");
		prop.setProperty("TITOLARITA", "Uso/Abitazione");
		prop.setProperty("TELEFONO_DISTRIBUTORE", "3390984856");
		prop.setProperty("ASCENSORE", "NO");
		prop.setProperty("DISALIMENTABILITA", "SI");
		prop.setProperty("CONSUMO_ANNUO", "10");
		prop.setProperty("SELEZIONA_CANALE_INVIO", "N");
		prop.setProperty("TIPO_OPERAZIONE", "PRIMA_ATTIVAZIONE_EVO");

		//CONFIGURAZIONE PRODOTTO
		//prop.setProperty("PRODOTTO", "Valore Luce Plus");
		prop.setProperty("PRODOTTO", "SCEGLI OGGI LUCE");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
		prop.setProperty("ELIMINA_VAS", "SI");
		//
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("VERIFICA_INFORMATIVA_RIPENSAMENTO","N");
		prop.setProperty("SKIP_CONTATTI_CONSENSI","Y");
		prop.setProperty("EMAIL", "testing.crm.automation@gmail.com");
		prop.setProperty("CANALE_INVIO", "EMAIL");
		prop.setProperty("MODALITA_FIRMA", "VOCAL ORDER");
		prop.setProperty("REGISTRAZIONE_VOCAL", "N");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		prop.setProperty("RUN_LOCALLY","Y");

		// Dati R2d ELE
		prop.setProperty("USER_R2D", Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D", Costanti.password_r2d_swa);
		prop.setProperty("SKIP_POD", "N");
		prop.setProperty("STATO_R2D", "AW");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE", "Prima Attivazione");
		prop.setProperty("TIPO_CONTATORE", "CE - Contatore Elettronico Non Orario");
		prop.setProperty("POTENZA_FRANCHIGIA", "220");
		prop.setProperty("EVENTO_3OK_ELE", "Esito Ammissibilità - Ammissibilità A01");
		prop.setProperty("EVENTO_5OK_ELE", "Esito Richiesta - Esito A01");


	};


	public boolean execute(int moduleIndex) throws Exception
	{
		return execute(moduleIndex,"",0);
	}
	public boolean execute(int moduleIndex, String commenti) throws Exception
	{
		return execute(moduleIndex,commenti,0);
	}
	public int randomint(int min, int max) {
	    return (int) ((Math.random() * (max - min)) + min);
	}

	public boolean execute(int moduleIndex, String commenti,int wait) throws Exception
	{
		
		boolean executeModule=false;


		Properties props=new Properties();
		props.load(new FileInputStream(nomeScenario));
		
		String msg=String.format("Execute Check - module index: %s %s - ultimo modulo avviato: %s ",moduleIndex
				,commenti.contentEquals("")?"":" *** "+commenti+" *** ",
				props.getProperty(MODULO_DA_ESEGUIRE));
		
		if(moduleIndex>=Integer.parseInt(props.getProperty(MODULO_DA_ESEGUIRE)))
		{
			executeModule= true;
			props.setProperty(MODULO_DA_ESEGUIRE, ""+(moduleIndex));
			props.store(new FileOutputStream(nomeScenario), null);
		}

		print(msg+(executeModule?" esegui "+moduleIndex:"non eseguire "+moduleIndex));

		if(executeModule && wait>0)
		{
			print("wait for "+wait+" second ");
			Thread.sleep(wait*1000);
		}
		
		return executeModule;

	}





	public void eseguiTest() throws Exception {
		String error="";
		try {
			print("Nome Scenario: "+nomeScenario);
			String args[] = {nomeScenario};
			

			

			//SALVO LE PROPERTIES SOLO SE IL FILE NON ESISTE
			if(!new File(nomeScenario).exists()||restart)
			{
				prop.setProperty(MODULO_DA_ESEGUIRE, "1");
				prop.store(new FileOutputStream(nomeScenario), null);
			}

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);

			//Creazione cliente
			if(execute(1,"Login su SF"))
			{
				LoginSalesForce.main(args);
				SbloccaTab.main(args);
			}
			if(execute(2,"Creazione nuovo cliente"))
			{
				CreazioneNuovoCliente.main(args);
			}

			//Scenario PA
			if(execute(3,"Recupera Pod non esistente"))
			{
				RecuperaPodNonEsistente.main(args);
			}

			if(execute(4,"Set utenza S2S"))
			{
				SetUtenzaS2SAttivazioni.main(args);
			}

			if(execute(5,"Login SF per Attivazione"))
			{
				LoginSalesForce.main(args);
				SbloccaTab.main(args);
			}

			if(execute(6,"Cerca cliente per nuova interazione"))
			{
				CercaClientePerNuovaInterazioneEVO.main(args);
			}

			if(execute(7,"Seleziona processo"))
			{
				ProcessoPrimaAttivazioneResidenzialeEVO.main(args);
			}

			if(execute(8,"Indentificazione interlocutore"))
			{
				Identificazione_Interlocutore_ID17.main(args);
			}

			if(execute(9,"Seleziona mercato prima attivazione"))
			{
				SelezioneMercatoPrimaAttivazioneEVO.main(args);
			}
			if(execute(10,"Inserimento Forniture"))
			{
				InserimentoFornitureEVO.main(args);
			}
			if(execute(11,"Compilazione dati fornitura"))
			{
				CompilaDatiFornituraElePrimaAttivazione.main(args);
			}
			if(execute(12))
			{
				VerificaRiepilogoOffertaPrimaAttivazione.main(args);
			}
			if(execute(13))
			{
				CommodityEleResidenzialePrimaAttivazione.main(args);
			}
			if(execute(14))
			{
				ConfermaIndirizziPrimaAttivazione.main(args);
			}
			if(execute(15))
			{
				GestioneFatturazioneElettronicaPrimaAttivazioneEVO.main(args);
			}
			if(execute(16))
			{
				PagamentoBollettinoPostaleEVO.main(args);
			}
			if(execute(17))
			{
				ConfermaScontiBonusSWAEVO.main(args);
			}
			if(execute(18,"Selezione prodotto"))
			{
				ConfiguraProdottoElettricoResidenziale.main(args);
			}
			if(execute(19))
			{
				GestioneCVPPrimaAttivazione.main(args);
			}
			if(execute(20))
			{
				ConsensiEContattiPrimaAttivazione.main(args);
			}
			if(execute(21))
			{
				ModalitaFirmaPrimaAttivazione.main(args);
			}
			if(execute(22))
			{
				ConfermaPrimaAttivazione.main(args);
			}
			if(execute(23))
			{
				CaricaEValidaDocumenti.main(args);
			}
			if(execute(24))
			{
				//Verifica offerta in stato chiusa
				RecuperaStatusOffer.main(args);
				VerificaOffertaInChiusa.main(args);
			}
			if(execute(25,"Verifiche POD ELE",180))
			{
				//Verifiche POD ELE-->aspettare 3 min
				RecuperaStatusCase_and_Order.main(args);
				SalvaIdOrdine.main(args);
				VerificaStatusR2DPresaInCarico.main(args);
			}
			if(execute(26))
			{
				SetOIRicercaDaOIOrdine.main(args);
			}
			
			if(execute(27,"Esitazione R2D",randomint(30, 120)))
			{
				//Esiti R2D ELE
				LoginR2D_ELE.main(args);
				TimeUnit.SECONDS.sleep(5);
				R2D_VerifichePodIniziali_ELE.main(args);
				TimeUnit.SECONDS.sleep(5);
				print("1OK");
				R2D_InvioPSPortale_1OK_ELE.main(args);
				print("3OK");
				R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
				print("5OK");
				R2D_CaricamentoEsiti_5OK_ELE.main(args); //per DP1 
				R2D_VerifichePodFinali_ELE.main(args);
			}
			if(execute( 28,"Verifiche finali"))
			{
				//Lanciare dopo 4 ore
				RecuperaStatusCase_and_Order.main(args);
				VerificaStatusSAP_ISUAttivazPod.main(args);
				VerificaStatusSEMPREAttivazPod.main(args);

				//Verifica finale su chiusura Richiesta
				RecuperaStatusCase_and_Order.main(args);
				VerificheAttivazioneRichiesta.main(args);
				
			}
		}
		catch(Exception ex)
		{
			
			print("########### \n FAILED - !!!!!!!!!!!!!!!!!!!!!!!!!  -  "+ nomeScenario  +"  - Exception: "+ex.getMessage());
			testOk=false;
			error=ex.getMessage().substring(0, Math.min(200,ex.getMessage().length()));
		}
		finally
		{
			String args[] = {nomeScenario};
			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);

			this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
			if(testOk)
			{
				prop.store(new FileOutputStream(nomeScenario.replace(".properties", "_"+prop.getProperty("NUMERO_RICHIESTA","")+"_OK.properties")), null);
			}
			UpdateResults(nomeScenario.replace(".properties", ""),
					" Richiesta: "+prop.getProperty("NUMERO_RICHIESTA","N/A") 
					+" esito: "+(testOk?"OK":"KO")
					+" " +error);
		}
	};







}
