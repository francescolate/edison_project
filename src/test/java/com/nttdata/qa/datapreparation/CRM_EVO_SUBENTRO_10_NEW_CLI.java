package com.nttdata.qa.datapreparation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;

import com.nttdata.qa.enel.testqantt.AvvioProcesso;
import com.nttdata.qa.enel.testqantt.CaricaDocumentoSubentro;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumenti;
import com.nttdata.qa.enel.testqantt.CaricaEValidaDocumentiSubDP10;
import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.CigCupEVO;
import com.nttdata.qa.enel.testqantt.CommodityEleNonResidenzialeSubentro;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaEVONonResidenzialePA;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaEVONonResidenzialePASubentroDP10;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziPrimaAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSubentroDP10;
import com.nttdata.qa.enel.testqantt.ConfermaOffertaAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoElettricoNonResidenziale;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiSubDP10;
import com.nttdata.qa.enel.testqantt.ConsensiEContattiSubentro;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.GestioneCVPAllaccioAttivazione;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_ID15;
import com.nttdata.qa.enel.testqantt.Identificazione_Interlocutore_SubDP10;
import com.nttdata.qa.enel.testqantt.InserimentoFornitureSubentroSingolaEleGas;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.LoginSalesForcePE;
import com.nttdata.qa.enel.testqantt.ModalitaFirmaECanaleInvioSubentroNew;
import com.nttdata.qa.enel.testqantt.PagamentoBonificoEVO;
import com.nttdata.qa.enel.testqantt.RecuperaDatiWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDDaPod;
import com.nttdata.qa.enel.testqantt.RecuperaOrderIDWorkbench;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RicercaOfferta;
import com.nttdata.qa.enel.testqantt.RicercaOffertaSubentro;
import com.nttdata.qa.enel.testqantt.RicercaRichiesta;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SelezioneReferente;
import com.nttdata.qa.enel.testqantt.SelezioneUsoFornitura;
import com.nttdata.qa.enel.testqantt.SetPropertyCF;
import com.nttdata.qa.enel.testqantt.SetPropertyIDOrder;
import com.nttdata.qa.enel.testqantt.SetSubentro7R2D;
import com.nttdata.qa.enel.testqantt.SetSubentroBatch;
import com.nttdata.qa.enel.testqantt.SetSubentroProperty;
import com.nttdata.qa.enel.testqantt.SetSubentroqueryCF_subentro_id34;
import com.nttdata.qa.enel.testqantt.SezioneMercatoSubentro;
import com.nttdata.qa.enel.testqantt.SplitPaymentEVO;
import com.nttdata.qa.enel.testqantt.ValidaDocumenti;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraOfferta;
import com.nttdata.qa.enel.testqantt.VerificaDocumento;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheRichiestaDaPod;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_Standard_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_InvioPSPortale_1OK_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.PartitaIva;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;


public class CRM_EVO_SUBENTRO_10_NEW_CLI  {
	final String nomeScenario = this.getClass().getSimpleName()+".properties";
	SeleniumUtilities util;
	Properties prop;


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() throws Exception{
		Properties prop = new Properties();
		//Property Creazione cliente - id 36
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_pe);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_pe);
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("TIPOLOGIA_CLIENTE", "BUSINESS");
		String piva = PartitaIva.getPartitaIVA();
		prop.setProperty("CODICE_FISCALE", piva);
		prop.setProperty("PARTITA_IVA", piva);
		prop.setProperty("TIPO_FORMA_GIURIDICA", "Amministrazione Pubblica");
		prop.setProperty("RAGIONE_SOCIALE", "AUTOMATION TEAM");
		prop.setProperty("COGNOME_REFERENTE", "GALDIERO");
		prop.setProperty("NOME_REFERENTE", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO_REFERENTE", "M");
		prop.setProperty("DATA_NASCITA_REFERENTE", "04/03/1985");
		prop.setProperty("COMUNE_NASCITA_REFERENTE", "Napoli");
		prop.setProperty("RUOLO_REFERENTE", "Titolare");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("TELEFONO_CLIENTE", "0819987645");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("MODULE_ENABLED","Y");
		//Solo per esecuzione locale, NO QANTT
		prop.setProperty("RUN_LOCALLY","Y");
		
		//Property Subentro - subentro id 34
		prop.setProperty("STATUS_UBIEST", Costanti.statusUbiest);
        //prop.setProperty("LINK", Costanti.salesforceLink);
        prop.setProperty("TIPO_UTENZA", "STS");
        prop.setProperty("CAP", "00178");
        prop.setProperty("PROVINCIA_COMUNE", "ROMA");
        prop.setProperty("PROCESSO", "Avvio Subentro EVO");
        prop.setProperty("NUMERO_DOCUMENTO", "1231");
        prop.setProperty("RILASCIATO_DA", "ABCD");
        prop.setProperty("RILASCIATO_IL", "01/01/2020");
        prop.setProperty("MERCATO", "Libero");
        prop.setProperty("TIPO_MISURATORE", "Non Orario");       
        prop.setProperty("TENSIONE_CONSEGNA", "220");
        prop.setProperty("POTENZA_CONTRATTUALE", "4");
        prop.setProperty("POTENZA_FRANCHIGIA", "4,4");
        prop.setProperty("ESITO_OFFERTABILITA", "OK");
        prop.setProperty("VERIFICA_ESITO_OFFERTABILITA", "Y");
        
        prop.setProperty("SEZIONE_ISTAT", "Y");
        prop.setProperty("LIST_TEXT", "N");
        prop.setProperty("CHECK_LIST_TEXT", "ATTENZIONE PRIMA DI PROCEDERE VERIFICARE- Che il contatore sia cessato ed in passato abbia erogato Luce/gas- Che il cliente sia in possesso dei dati necessari per l’inserimento della richiesta:POD/PDR - Matr/EneltelINFORMAZIONI UTILI- In caso di Dual ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO- In caso di Multi ricordati che le forniture dovranno avere stesso:USO - INDIRIZZO DI FATTURAZIONE - MODALITÀ DI PAGAMENTO – PRODOTTO – STESSA COMMODITY- In caso di Cliente Pubblica Amministrazione sarà necessario il Codice Ufficio- In caso di Cliente Business potranno essere inserite le informazioni sulla fatturazione elettronica (CU o pec dedicata)- In caso di Duale/Multi ricordati che non è possibile eseguire la modifica potenza e/o tensione- Se il cliente attiva metodo di pagamento SDD e Bolletta Web riceverà uno sconto\\bonus in fatturaFibra di Melita:In caso di vendita della Fibra di Melita ricordati che il prodotto è dedicato ai: -   Clienti RESIDENZIALI -   Uso fornitura ABITATIVO -   Prevede Contrattualizzazione senza DELEGA -   Occorre il Codice di migrazione e il numero telefonico in caso di portabilità  In caso di vendita della Fibra di Melita ricordati di fare sempre prima la Verifica di Copertura stand alone, solo in caso di copertura, se il cliente possiede già un collegamento dati/voce con un altro Operatore, richiedigli il codice Migrazione e il numero di telefono per effettuare la verifica prima di procedere con l’inserimento del subentro. Informa il cliente che se il suo l’attuale servizio è attivo su rete FTTH (Fiber To The Home) e la quartultima e terzultima cifra del suo Codice di Migrazione sono «O» e «F», la disdetta del vecchio contratto sarò automatica. Al contrario, il cliente dovrà provvedere in autonomia a cessare il contratto con il vecchio operatore nelle modalità da quest’ultimo disciplinate.Inoltre è importante ricordare al cliente che: •   la Fibra di Melita non prevede il servizio voce ma solo connessione dati •   i dati inseriti verranno salvati sui nostri sistemi solamente dopo l’invio della richiesta  In caso di vendita da canale telefonico ricordati che è obbligatoria la modalità di firma Registrazione Vocale (sono previste due registrazioni vocali separate)In caso di vendita della Fibra di Melita ricordati che è obbligatorio avere nell’anagrafica cliente:-   numero di cellulare -   indirizzo email (diversa per ogni Fibra di Melita che il cliente potrà avere)in caso di mancanza/obsolescenza occorrerà procedere all’integrazione/aggiornamento dei dati di contatto attraverso la Modifica Anagrafica.Ricordati che la modalità di pagamento per la Fibra di Melita sarà sempre la medesima scelta per la commodity Elettrica o Gas cui è associata  SERVIZIO DI PRENOTAZIONEIl Servizio di Prenotazione consente al Cliente di concludere il contratto di fornitura oggi, scegliendo una data desiderata di attivazione del servizio. Il Cliente potrà modificare la data di attivazione successivamente, attraverso un link che riceverà nella propria casella email.ATTENZIONE: è necessario che il Cliente restituisca tutta la documentazione necessaria richiesta,  prima della data indicata, altrimenti non sarà possibile procedere con l’operazione.Quando è possibile richiedere il Servizio di Prenotazione:al momento il servizio è disponibile solo per operazioni di :-          riattivazione del contatore elettrico su fornitura singola (subentro single ele)-          operazioni a parità di condizioni tecniche  (senza modifica potenza e/o tensione)-          rete application to application  ( E- Distribuzione )-          comprese tra 20gg dalla data odierna e non oltre i 90gg (giorni solari) Cosa serve:E’ necessario fornire un:-          indirizzo EMAIL-          numero di telefono mobileSe i dati sono già presenti a sistema, chiedere conferma al cliente prima di procedere.  SCRIPT INFORMAZIONI VERIFICHE CREDITIZIE[da leggere sempre al cliente in fase di VBL e Credit chek]\"Sig./Sig.ra La informo che prima dell'attivazione del contratto di fornitura, Enel Energia effettuerà attività di controllo sulla sua affidabilità creditizia e sulla puntualità nei pagamenti, nonché verifiche volte a prevenire il rischio di frodi accedendo ai propri sistemi informativi, a fonti pubbliche e/o a ad altre banche dati di società autorizzate, come previsto dal Decreto Legislativo 124/2017. Qualora le verifiche restituissero un esito negativo, non sarà possibile procedere all’attivazione del servizio e Le sarà inviata comunicazione scritta. Per avere maggiori informazioni sull'esito della sua richiesta può inviare una comunicazione scritta all’indirizzo email privacy.enelenergia@enel.com. Per quanto non espressamente descritto in questa Informativa, troveranno applicazione le disposizioni contenute nelle Condizioni Generali di Fornitura (CGF) di Enel Energia.Enel Energia, Titolare del trattamento, tratterà i suoi dati personali nel rispetto della normativa vigente. Informativa privacy al sito www.enel.it\"");
        prop.setProperty("CLIENTE_BUSINESS", "Y");
        prop.setProperty("POPOLA_FATTURAZIONE_ELETTRONICA", "NO");
        prop.setProperty("STATO_GLOBALE_RICHIESTA", "IN LAVORAZIONE");
        prop.setProperty("SOTTOSTATO_GLOBALE_RICHIESTA", "OFFER - INVIATA");
        prop.setProperty("STATO_RICHIESTA", "INVIATA");
        prop.setProperty("STATO_R2D", "N.D.");
        prop.setProperty("STATO_SAP", "N.D.");
        prop.setProperty("STATO_SEMPRE", "N.D.");
        prop.setProperty("CONSENSO_MARKETING_TELEFONICO", "");
        prop.setProperty("CONSENSO_MARKETING_CELLULARE", "");
        prop.setProperty("CONSENSO_TERZI_CELLULARE", "");
        prop.setProperty("CONSENSO_TERZI_TELEFONICO", "");
   
        prop.setProperty("RIGA_DA_ESTRARRE", "7");
        prop.setProperty("RUN_LOCALLY", "Y");
        prop.setProperty("CONTAINER_RIEPILOGO_OFFERTE", "Y");
        prop.setProperty("TIPO_FORNITURA", "ELETTRICO");
        prop.setProperty("COMMODITY", "ELE");
        prop.setProperty("USO", "Illuminazione Pubblica");
        prop.setProperty("PRODOTTO", "New_Soluzione Energia Impresa Business");
        prop.setProperty("OPZIONE_KAM_AGCOR", "SUPER");
        prop.setProperty("ELIMINA_VAS", "Y");
      
        prop.setProperty("CLIENTE_FORNISCE_INDIRIZZO", "NO");
        
        prop.setProperty("DISALIMENTABILITA", "SI");   
        prop.setProperty("ASCENSORE", "NO");
        prop.setProperty("ORDINE_FITTIZIO", "NO");
        prop.setProperty("CATEGORIA_MERCEOLOGICA", "SERVIZI PER LA RETE STRADALE");
        prop.setProperty("TELEFONO_DISTRIBUTORE", "3885545572");
        prop.setProperty("CONSUMO_ANNUO", "1250");
        prop.setProperty("TITOLARITA", "");
        
        prop.setProperty("TIPOLOGIA_PA", "Centrale");
        prop.setProperty("SPLIT_PAYMENT", "No");
        prop.setProperty("FLAG_136", "SI");
        prop.setProperty("CANALE_INVIO_FIRMA", "STAMPA LOCALE");
        prop.setProperty("CIG", "1231232231");
        prop.setProperty("CUP", "143dfsd12424241");
        prop.setProperty("CODICE_UFFICIO_FATTURAZIONE", "UFTKWP");
        prop.setProperty("TIPO_OPERAZIONE", "SUBENTRO_EVO");
        
        prop.setProperty("CONSENSO_PROFILAZIONE", "ACQUISITO");
        prop.setProperty("CONSENSO_MARKETING_TELEFONICO", "ACQUISITO");
        prop.setProperty("CONSENSO_MARKETING_CELLULARE", "ACQUISITO");
        prop.setProperty("CONSENSO_MARKETING_EMAIL", "ACQUISITO");
        prop.setProperty("CONSENSO_TERZI_TELEFONICO", "ACQUISITO");
        prop.setProperty("CONSENSO_TERZI_CELLULARE", "ACQUISITO");
        prop.setProperty("CONSENSO_TERZI_EMAIL", "ACQUISITO");

		return prop;
	}



	@After
	public void tearDown() throws Exception{
		/*
	       InputStream in = new FileInputStream(nomeScenario);
	        prop.load(in);
	        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
	        ReportUtility.reportToServer(this.prop);
	        */
	}

	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = {nomeScenario};

		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);

        //Creazione Cliente
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CreazioneNuovoCliente.main(args);
		String filePath = "src/test/resources/nuoviclienti.txt";
		Files.write(Paths.get(filePath), ("BUSINESS;"+prop.getProperty("CODICE_FISCALE")+"\n").getBytes(), StandardOpenOption.APPEND);
		
		//Subentro 34
		RecuperaPodNonEsistente.main(args);             
	    LoginSalesForcePE.main(args);
	    SbloccaTab.main(args);
	    CercaClientePerNuovaInterazioneEVO.main(args); //inserimento CF
	    AvvioProcesso.main(args);
	    //Identificazione_Interlocutore_ID15.main(args);
	    Identificazione_Interlocutore_SubDP10.main(args);
	    SezioneMercatoSubentro.main(args);
	    InserimentoFornitureSubentroSingolaEleGas.main(args);     
	       
	    SelezioneReferente.main(args);
	    SelezioneUsoFornitura.main(args);
	    CommodityEleNonResidenzialeSubentro.main(args);
	    ConfermaIndirizziSubentroDP10.main(args);
	    ConfermaFatturazioneElettronicaEVONonResidenzialePASubentroDP10.main(args);
	    PagamentoBonificoEVO.main(args);   
	    ConfermaScontiBonusEVO.main(args);
	    SplitPaymentEVO.main(args);
	    CigCupEVO.main(args);       
	    ConfiguraProdottoElettricoNonResidenziale.main(args);   
	    ConsensiEContattiSubDP10.main(args);
	    ModalitaFirmaECanaleInvioSubentroNew.main(args);
	    GestioneCVPAllaccioAttivazione.main(args);
	    ConfermaOffertaAllaccioAttivazione.main(args);  
	     
		LoginSalesForce.main(args);
		RicercaOffertaSubentro.main(args); 
		CaricaEValidaDocumenti.main(args);
		
        SetSubentroProperty.main(args);
        LoginSalesForce.main(args);
        RicercaOffertaSubentro.main(args); 
        VerificheRichiestaDaPod.main(args);   
        RecuperaOrderIDDaPod.main(args);
        
        //Check status offerta
        RecuperaStatusOffer.main(args);
        VerificaChiusuraOfferta.main(args);

        //Dati R2D
        SetSubentro7R2D.main(args);
        LoginR2D_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_VerifichePodIniziali_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_InvioPSPortale_1OK_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_CaricamentoEsiti_3OK_Standard_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_CaricamentoEsiti_5OK_ELE.main(args);
        TimeUnit.SECONDS.sleep(10);
        R2D_VerifichePodFinali_ELE.main(args);
	       
	   /* 
	    //Da eseguire dopo qualche ora
	    prop.store(new FileOutputStream(nomeScenario), null);
	    SetSubentroBatch.main(args);
	    prop.load(new FileInputStream(nomeScenario));
	    LoginSalesForce.main(args);
	    SbloccaTab.main(args);
	    RicercaRichiesta.main(args);
	    VerificheRichiestaDaPod.main(args);*/
	}




}

