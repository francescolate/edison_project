package com.nttdata.qa.datapreparation;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.nttdata.qa.enel.testqantt.CercaClientePerNuovaInterazioneEVO;
import com.nttdata.qa.enel.testqantt.ConfermaFatturazioneElettronicaSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaIndirizziSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfermaScontiBonusSWAEVO;
import com.nttdata.qa.enel.testqantt.ConfiguraProdottoSWAEVOElettricoResidenziale;
import com.nttdata.qa.enel.testqantt.ConfermaModalitaFirmaDigital;
import com.nttdata.qa.enel.testqantt.ConsensiEContatti;
import com.nttdata.qa.enel.testqantt.CreazioneNuovoCliente;
import com.nttdata.qa.enel.testqantt.GestioneDocumentaleSWAEvo;
import com.nttdata.qa.enel.testqantt.LoginSalesForce;
import com.nttdata.qa.enel.testqantt.PagamentoBollettinoPostaleEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVO;
import com.nttdata.qa.enel.testqantt.PrecheckSwaEVOMulti;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoMULTIEVOResidenziale;
import com.nttdata.qa.enel.testqantt.ProcessoSwitchAttivoMULTIEVOResidenzialeSiNo;
import com.nttdata.qa.enel.testqantt.RecuperaPodMultiNonEsistenti;
import com.nttdata.qa.enel.testqantt.RecuperaPodNonEsistente;
import com.nttdata.qa.enel.testqantt.RecuperaStatusAsset;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Offer;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_Multi_Pod1;
import com.nttdata.qa.enel.testqantt.RecuperaStatusCase_and_Order_Multi_Pod2;
import com.nttdata.qa.enel.testqantt.RecuperaStatusOffer;
import com.nttdata.qa.enel.testqantt.RiepilogoOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdBPM;
import com.nttdata.qa.enel.testqantt.SalvaIdOfferta;
import com.nttdata.qa.enel.testqantt.SalvaIdOrdine;
import com.nttdata.qa.enel.testqantt.SbloccaTab;
import com.nttdata.qa.enel.testqantt.SetOrderLineItemCommoditytoRetreive;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Dual;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Multi_Pod1;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveELE_Multi_Pod2;
import com.nttdata.qa.enel.testqantt.SetPODtoRetreiveGAS_Dual;
import com.nttdata.qa.enel.testqantt.UpdateDataRipensamentoSWA;
import com.nttdata.qa.enel.testqantt.VerificaChiusuraOfferta;
import com.nttdata.qa.enel.testqantt.VerificaStatusOrdineEspletato;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusR2DRicezioneOrdine;
import com.nttdata.qa.enel.testqantt.VerificaStatusSAP_ISUAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificaStatusSEMPREAttivazPod;
import com.nttdata.qa.enel.testqantt.VerificheAttivazioneRichiesta;
import com.nttdata.qa.enel.testqantt.VerifichePostEmissione;
import com.nttdata.qa.enel.testqantt.VerificheRichiesta;
import com.nttdata.qa.enel.testqantt.VerificheScadenzaRipensamento_SWA;
import com.nttdata.qa.enel.testqantt.r2d.LoginR2D_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_CaricamentoEsiti_5OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_Forzatura_Esiti_3OK_SWA_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodFinali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.testqantt.r2d.R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE;
import com.nttdata.qa.enel.util.Costanti;
import com.nttdata.qa.enel.util.ReportUtility;
import com.nttdata.qa.enel.util.ScreenShotRule;
import com.nttdata.qa.enel.util.SeleniumUtilities;

public class CRM_EVO_SWA_20_NEW_CLI  {
	private RemoteWebDriver driver;  Logger LOGGER = Logger.getLogger("");
	SeleniumUtilities util;
	Properties prop;
	final String nomeScenario = this.getClass().getSimpleName()+".properties";


	@Before
	public void setUp() throws Exception {
		this.prop = conf();
	}


	public static Properties conf() throws Exception {
		Properties prop = new Properties();
		
		//Property creazione cliente
		prop.setProperty("LINK","https://enelcrmt--uat.cs88.my.salesforce.com/");
		prop.setProperty("USERNAME",Costanti.utenza_salesforce_operatore_pe_3);
		prop.setProperty("PASSWORD",Costanti.password_salesforce_operatore_pe_3);
		prop.setProperty("TIPO_UTENZA", "PE");
		prop.setProperty("LANDING_PAGE_TITLE","Lightning Experience");
		prop.setProperty("TIPOLOGIA_CLIENTE", "RESIDENZIALE");
		prop.setProperty("COGNOME", "AUTOMATION");
		prop.setProperty("NOME", SeleniumUtilities.randomAlphaNumeric(9));
		prop.setProperty("SESSO", "F");
		prop.setProperty("DATA_NASCITA", "04/03/1987");
		prop.setProperty("COMUNE_NASCITA", "Napoli");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("COMUNE", "ROMA");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CELLULARE", "3467656345");
		prop.setProperty("DESCRIZIONE_CELLULARE", "Privato");
		prop.setProperty("DIPENDENTE_ENEL","N");
		
		//Property SWA
		prop.setProperty("SWITCH_ATTIVO", "Y");
		prop.setProperty("COMMODITY", "ELE");
		//COdice Fiscale preso dal Property per Cliente Nuovo
//		prop.setProperty("CODICE_FISCALE", "DTZRST33L18G482D"); 
		prop.setProperty("TIPOCLIENTE", "RESIDENZIALE");
		prop.setProperty("TIPO_DOCUMENTO", "Passaporto");
		prop.setProperty("NUMERO_DOCUMENTO", "YNABHYD");
		prop.setProperty("RILASCIATO_DA", "MCTCNA");
		prop.setProperty("RILASCIATO_IL", "04/03/2015");
		prop.setProperty("TIPO_DELEGA", "Nessuna delega");
		prop.setProperty("USO","Uso Abitativo");
		prop.setProperty("MERCATO_DI_PROVENIENZA", "Vincolato");
		prop.setProperty("TITOLARITA","Uso/Abitazione");
		prop.setProperty("RESIDENTE_POD1","SI"); 
		prop.setProperty("RESIDENTE_POD2","NO"); 
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO NAZIONALE • Vincolato");
//		prop.setProperty("SOCIETA_DI_VENDITA", "SERVIZIO ELETTRICO ROMANO");
		prop.setProperty("SOCIETA_DI_VENDITA", "servizio elettrico romano");
		prop.setProperty("CAP", "00198");
		prop.setProperty("REGIONE", "LAZIO");
		prop.setProperty("PROVINCIA", "ROMA");
		prop.setProperty("CITTA", "ROMA");
		prop.setProperty("INDIRIZZO", "VIA NIZZA");
		prop.setProperty("CIVICO", "4");
		prop.setProperty("CANALE_INVIO_CONTRATTO", "FACE");
		prop.setProperty("MODALITA_FIRMA", "No vocal");
		prop.setProperty("TIPO_OPERAZIONE", "SWITCH_ATTIVO_EVO");
//		prop.setProperty("PRODOTTO", "Speciale Luce 60");
		prop.setProperty("PRODOTTO", Costanti.prodotto_residenziale_ele);
		prop.setProperty("PIANO_TARIFFARIO", "Senza Orari");
		prop.setProperty("TAB_SGEGLI_TU", "SI");
		prop.setProperty("PRODOTTO_TAB", "Scegli Tu");
        prop.setProperty("ELIMINA_VAS", "Y");
		prop.setProperty("ESECUZIONE_ANTICIPATA", "SI");
		prop.setProperty("CARICA_DOCUMENTO", "SI");
		prop.setProperty("PATH_DOCUMENTO", "C:\\Appo\\DocSWA.pdf");
		prop.setProperty("STATO_RICHIESTA", "INVIATA");
		prop.setProperty("STATO_R2D", "N.D.");
		prop.setProperty("STATO_SAP", "N.D.");
		prop.setProperty("STATO_SEMPRE", "N.D.");
		prop.setProperty("OPZIONE_VAS", "");
		
		//nuovi prop
		prop.setProperty("STATO_CASE_POST_EMISSIONE", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_POST_EMISSIONE", "Offer - Chiusa da confermare");
		prop.setProperty("STATO_OFFERTA_POST_EMISSIONE", "Chiusa da confermare");
		prop.setProperty("TIPO_OI_ORDER", "Commodity");
		
		prop.setProperty("STATO_CASE_SCAD_RIPENSAMENTO", "IN LAVORAZIONE");
		prop.setProperty("SOTTOSTATO_CASE_SCAD_RIPENSAMENTO", "Order - Inviato");
		prop.setProperty("STATO_OFFERTA_SCAD_RIPENSAMENTO", "Chiusa");
		
		prop.setProperty("ESITO_UDB","OK");

		//Integrazione R2D ELE
		prop.setProperty("LINK_R2D_ELE","http://r2d-coll.awselb.enelint.global/r2d/ele/home.do");
		prop.setProperty("USERNAME_R2D_ELE",Costanti.utenza_r2d_swa);
		prop.setProperty("PASSWORD_R2D_ELE",Costanti.password_r2d_swa);
		prop.setProperty("DISTRIBUTORE_RD2_ATTESO_ELE","Areti");
		prop.setProperty("TIPO_LAVORAZIONE_CRM_ELE","Switch Attivo EVO");
		prop.setProperty("EVENTO_3OK_ELE","EIN - Esito Ammissibilità SE1");
		prop.setProperty("EVENTO_5OK_ELE","Esito Richiesta - Esiti SWA");
		
		prop.setProperty("RUN_LOCALLY", "Y");

		return prop;
	}

    @After
 public void fineTest() throws Exception{
        InputStream in = new FileInputStream(nomeScenario);
        prop.load(in);
        this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
        ReportUtility.reportToServer(this.prop);
    }


	@Rule
	public TestWatcher screenshotOnFailure = new ScreenShotRule();

	@Test
	public void test() throws Exception {

//		prop.store(new FileOutputStream(nomeScenario), null);
		String args[] = { nomeScenario };
		InputStream in = new FileInputStream(nomeScenario);
		prop.load(in);
/*		
		//Creazione cliente
		LoginSalesForce.main(args);
		SbloccaTab.main(args);
		CreazioneNuovoCliente.main(args);

		//DP SWA
		RecuperaPodMultiNonEsistenti.main(args);

		LoginSalesForce.main(args);
		
		SbloccaTab.main(args);
		
		CercaClientePerNuovaInterazioneEVO.main(args);

		ProcessoSwitchAttivoMULTIEVOResidenzialeSiNo.main(args);
		
		RiepilogoOfferta.main(args);
		
		ConfermaIndirizziSWAEVO.main(args);
		
		ConfermaFatturazioneElettronicaSWAEVO.main(args);

		PagamentoBollettinoPostaleEVO.main(args);
		
		ConfermaScontiBonusSWAEVO.main(args);
		
		ConfiguraProdottoSWAEVOElettricoResidenziale.main(args);
		
		ConfermaModalitaFirmaDigital.main(args);
		
		ConsensiEContatti.main(args);
		
		GestioneDocumentaleSWAEvo.main(args);
		
		PrecheckSwaEVOMulti.main(args);
		
		//startare 3 minuti dopo l'emissione
		//Verifica status case+offer emissione contratto
		//Order
		Thread.currentThread().sleep(180000); 

		// Data Ripensamento POD1
		SetPODtoRetreiveELE_Multi_Pod1.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);
		//Effettuare update su data ripensamento
		UpdateDataRipensamentoSWA.main(args);
		
		// Data Ripensamento POD2
		SetPODtoRetreiveELE_Multi_Pod2.main(args);
		RecuperaStatusCase_and_Offer.main(args);
		VerifichePostEmissione.main(args);
		SalvaIdOfferta.main(args);
		//Effettuare update su data ripensamento
		UpdateDataRipensamentoSWA.main(args);
		
		//Startare dopo 2 ore
		RecuperaStatusCase_and_Offer.main(args);
		VerificheScadenzaRipensamento_SWA.main(args);
		RecuperaStatusCase_and_Order.main(args);
		SalvaIdBPM.main(args);
		VerificaStatusR2DRicezioneOrdine.main(args);
		SetOrderLineItemCommoditytoRetreive.main(args);
		
		//Check status offerta
		RecuperaStatusOffer.main(args);
		VerificaChiusuraOfferta.main(args);

		//Esitazione 5-OK POD_1 ELE
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
		SalvaIdOrdine.main(args);

		//Esitazione R2D 
		LoginR2D_ELE.main(args);
		Thread.currentThread().sleep(4000); 
		R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE.main(args);  // OK-Cla
		Thread.currentThread().sleep(4000); 
		R2D_CaricamentoEsiti_3OK_SWA_ELE.main(args);
		R2D_Forzatura_Esiti_3OK_SWA_ELE.main(args); // OK Cla
		Thread.currentThread().sleep(4000); 
		R2D_CaricamentoEsiti_5OK_SWA_ELE.main(args); //OK Cla
		Thread.currentThread().sleep(4000); 
		R2D_VerifichePodFinali_SW_E_DISDETTE_ELE.main(args);

		//Esitazione 5-OK POD_2 ELE
		SetOrderLineItemCommoditytoRetreive.main(args);
		RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
		SalvaIdOrdine.main(args);

		//Esitazione R2D 
		LoginR2D_ELE.main(args);
		Thread.currentThread().sleep(4000); 
		R2D_VerifichePodIniziali_SW_E_DISDETTE_ELE.main(args);  // OK-Cla
		Thread.currentThread().sleep(4000); 
		R2D_CaricamentoEsiti_3OK_SWA_ELE.main(args);
		R2D_Forzatura_Esiti_3OK_SWA_ELE.main(args); // OK Cla
		Thread.currentThread().sleep(4000); 
		R2D_CaricamentoEsiti_5OK_SWA_ELE.main(args); //OK Cla
		Thread.currentThread().sleep(4000); 
		R2D_VerifichePodFinali_SW_E_DISDETTE_ELE.main(args);

		//Verifiche post esiti R2D, aspettare 3 minuti
		Thread.currentThread().sleep(18000); 
		RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);

		Thread.currentThread().sleep(18000); 
		RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
		VerificaStatusR2DAttivazPod.main(args);
		VerificaStatusOrdineEspletato.main(args);

		//Lanciare dopo 4 ore
		RecuperaStatusCase_and_Order_Multi_Pod1.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		VerificheAttivazioneRichiesta.main(args);
		
		RecuperaStatusCase_and_Order_Multi_Pod2.main(args);
		VerificaStatusSAP_ISUAttivazPod.main(args);
		VerificaStatusSEMPREAttivazPod.main(args);
		VerificheAttivazioneRichiesta.main(args);
*/
	}

}



