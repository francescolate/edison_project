package com.reply.qa.edison;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nttdata.qa.enel.testqantt.colla.LoginEnel;
import com.nttdata.qa.enel.util.ReportUtility;
import com.reply.qa.edison.modules.Test_01_Module;

public class Test_01 {
	Properties prop;
	final String nomeScenario=this.getClass().getSimpleName()+".properties";
		
		@Before
		public void inizioTest() throws Exception{
			this.prop=new Properties();
			prop.setProperty("LINK", "https://www.edisonenergia.it/edison/casa");
			prop.setProperty("USERNAME", "francesco01");
			prop.setProperty("PASSWORD", "Password01");
			prop.setProperty("RUN_LOCALLY","Y");
		
		};
		
		@Test
        public void eseguiTest() throws Exception{
			prop.store(new FileOutputStream(nomeScenario), null);
			String args[] = {nomeScenario};

			InputStream in = new FileInputStream(nomeScenario);
			prop.load(in);
			Test_01_Module.main(args);

			//prova windows3
			//prova mac3
			//prova modifica 6 sett
		};
		
		/*
		@After
        public void fineTest() throws Exception{
			
		};*/
		
		@After
	    public void fineTest() throws Exception{
            String args[] = {nomeScenario};
            InputStream in = new FileInputStream(nomeScenario);
            prop.load(in);
            this.prop.setProperty("TEST_NAME", this.getClass().getSimpleName());
            ReportUtility.reportToServer(this.prop);
      };

		
		
		
		
	

}